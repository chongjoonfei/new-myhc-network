export const environment = {
  production: true,
  localhost: 'https://myhc.my',
  uploadPath: '../../upload_files/',
  apiUrl: 'https://myhc.my/myhc-api'
};
