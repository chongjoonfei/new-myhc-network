import { BrowserModule } from '@angular/platform-browser';
import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';


import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { MainComponent } from './layout/main/main.component';
import { BreadcrumbsComponent } from './layout/main/breadcrumbs/breadcrumbs.component';
import { TitleComponent } from './layout/main/title/title.component';
import { AuthComponent } from './layout/auth/auth.component';
import { SharedModule} from './shared/shared.module';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { DataService } from './service/data.service';
import {HttpClientModule, HttpClient, HTTP_INTERCEPTORS} from '@angular/common/http';
import {TranslateModule, TranslateLoader, TranslateService} from '@ngx-translate/core';
import { MultiTranslateHttpLoader } from 'ngx-translate-multi-http-loader';
import { NgApexchartsModule } from 'ng-apexcharts';
import { ToastrModule } from 'ngx-toastr';
import { CustomLinerChartService } from './service/CustomLinerChartService';
import { PatientModule } from './pages/patient/patient.module';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { TableModule } from 'primeng-lts/table';
 
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MaterialModule } from './shared/material-module';
import { AddOnPurchaseModule } from './pages/add-on/add-on.module';
import { CorporateModule } from './pages/corporate/corporate.module';
import { DependentModule } from './pages/dependent/dependent.module';
import { NgxScrollTopModule } from 'ngx-scrolltop';
import { MauPageModule } from './pages/mau-page/mau-page.module';
import { MarketplaceModule } from './pages/marketplace/marketplace.module';
import { MyDashboardComponent } from './pages/my-dashboard/my-dashboard.component';
import { MyBiodataComponent } from './pages/my-biodata/my-biodata.component';
import { MyQrCodeComponent } from './pages/my-qr-code/my-qr-code.component';
import { AuthInterceptor } from './service/auth/AuthInterceptor';
import { ErrorComponent } from './pages/error/error.component';
import { MyAccountComponent } from './pages/my-account/my-account.component';
import { MyMemberMgmtComponent } from './pages/my-member-mgmt/my-member-mgmt.component';
import { MyMemberMgmtAddComponent } from './pages/my-member-mgmt/my-member-mgmt-add/my-member-mgmt-add.component';
import { MyRecordsComponent } from './pages/records/my-records/my-records.component';
import { MyAssessmentsComponent } from './pages/assessments/my-assessments/my-assessments.component';
import { MyBookingComponent } from './pages/booking/my-booking/my-booking.component';
import { MyBookingAddComponent } from './pages/booking/my-booking-add/my-booking-add.component';



export function HttpLoaderFactory(http: HttpClient) {
  return new MultiTranslateHttpLoader(http, [
    {prefix: "./assets/i18n/admin/", suffix: ".json"},
    {prefix: "./assets/i18n/member-mgmt/", suffix: ".json"},
    {prefix: "./assets/i18n/screening-test/", suffix: ".json"},
    {prefix: "./assets/i18n/booking/", suffix: ".json"},
    {prefix: "./assets/i18n/app/", suffix: ".json"},
  ]);
}
 
@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    BreadcrumbsComponent,
    TitleComponent,
    AuthComponent ,
    MyDashboardComponent,
    MyBiodataComponent,
    MyQrCodeComponent,
    MyRecordsComponent,
    MyAssessmentsComponent,
    ErrorComponent,
    MyAccountComponent,
    MyMemberMgmtComponent,
    MyMemberMgmtAddComponent,
    MyBookingComponent,
    MyBookingAddComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SharedModule,
    PatientModule,
    NgApexchartsModule,
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-right'
    }),
    SlickCarouselModule,
    MaterialModule,
    AddOnPurchaseModule,
    CorporateModule,
    DependentModule,
    NgxScrollTopModule,
    MauPageModule,
    MarketplaceModule,
    HttpClientModule,
    TableModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
    })
  ],
  exports: [
    TranslateModule
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  providers: [
    DataService,CustomLinerChartService, 
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    TranslateService 
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
