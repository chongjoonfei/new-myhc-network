/**
 * Created by: smajina 14 May 2020
 */
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
// import { AuthService } from '../auth/auth.service';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private _http: HttpClient,
    // private auth: AuthService, 
    private httpService: HttpService) { }

 

  public getMessages(): Observable<any> {
    return this.httpService.getData("./assets/data/message-box.json");
  }

  public getTestPanel(): Observable<any> {
    return this.httpService.getData("./assets/data/test-panel.json");
  }

  public getAddOn(): Observable<any> {
    return this.httpService.getData("./assets/data/add-on.json");
  }

  public getAddOnPurchaseHistory(): Observable<any> {
    return this.httpService.getData("./assets/data/add-on-purchase-history.json");
  }

  /** TEST PANEL */
  public getAllTestPanel(): Observable<any> {
    return this.httpService.getData("/myhc-api/v1/test-panel/read-all.php");
  }

  public updateTestPanel(data): Observable<any> {
    return this.httpService.postData("/myhc-api/v1/test-panel/update.php", data);
  }

  public getTestPanelDetails(code): Observable<any> {
    var url = "/myhc-api/v1/test-panel/read-one.php" + code;
    return this.httpService.getData(url);
  }

  /** TEST GROUP */
  public getAllAddOnPackage(): Observable<any> {
    return this.httpService.getData("/myhc-api/v1/test-group/read-all.php");
  }

  public createAddOnPackage(data): Observable<any> {
    return this.httpService.postData("/myhc-api/v1/test-group/create.php", data);
  }

  public updateAddOnPackage(data): Observable<any> {
    return this.httpService.postData("/myhc-api/v1/test-group/update.php", data);
  }

  public getAddOnPackageOne(code): Observable<any> {
    var url = "/myhc-api/v1/test-group/read-one.php?c=" + code;
    return this.httpService.getData(url);
  }
  
  public deleteAddOnPackage(data): Observable<any> {
    return this.httpService.postData("/myhc-api/v1/test-group/delete.php", data);
  }


  // ADD TEST PANEL INTO TEST GROUP/ADD-ON PACKAGE
  public createTestPanelInAddOnPackage(data): Observable<any> {
    return this.httpService.postData("/myhc-api/v1/test-group-panel/create.php", data);
  }

  public deleteTestPanelInAddOnPackage(data): Observable<any> {
    return this.httpService.postData("/myhc-api/v1/test-group-panel/delete.php", data);
  }



  /** TEST MARKER*/

  public createTestMarker(data): Observable<any> {
    return this.httpService.postData("/myhc-api/v1/test-marker/create.php", data);
  }

  public updateTestMarker(data): Observable<any> {
    return this.httpService.postData("/myhc-api/v1/test-marker/update.php", data);
  }

  public getTestMarkerDetails(code): Observable<any> {
    var url = "/myhc-api/v1/test-marker/read-one.php" + code;
    return this.httpService.getData(url);
  }

  /** TEST MARKER - REFERENCE RANGE NEW/EDIT/DELETE FUNCTION*/
  public updateRefRange(data): Observable<any> {
    return this.httpService.postData("/myhc-api/v1/test-reference-range/update.php", data);
  }

  public getRefreshRefRange(code): Observable<any> {
    return this.httpService.getData("/myhc-api/v1/test-marker/read-one.php?c=" + code);
  }

  public getTestMarkerByPanelCode(code): Observable<any> {
    return this.httpService.getData("/myhc-api/v1/test-marker/read-by-panel-code.php?c=" + code);
  }

  public deleteRefRange(data): Observable<any> {
    return this.httpService.postData("/myhc-api/v1/test-reference-range/delete.php", data);
  }

  /** ADD ON - MANAGE EDIT/DELETE FUNCTION*/

  public getAllAddOn(): Observable<any> {
    return this.httpService.getData("/myhc-api/v1/add-on/read-all.php");
  }

  public getOneAddOn(code): Observable<any> {
    return this.httpService.getData("/myhc-api/v1/add-on/read-one.php?c=" + code);
  }

  public getSearchAddOn(keyword): Observable<any> {
    return this.httpService.getData("/myhc-api/v1/add-on/search.php?s=" + keyword);
  }

  public ceateAddOn(data): Observable<any> {
    return this.httpService.postData("/myhc-api/v1/add-on/create.php", data);
  }

  public updateAddOn(data): Observable<any> {
    return this.httpService.postData("/myhc-api/v1/add-on/update.php", data);
  }

  public deleteAddOn(data): Observable<any> {
    return this.httpService.postData("/myhc-api/v1/add-on/delete.php", data);
  }

  /** SCREENING PLAN */

  public getAllScreeningPlan(): Observable<any> {
    return this.httpService.getData("/myhc-api/v1/screening-plan/read-all.php");
  }

  public createScreenPlan(data): Observable<any> {
    return this.httpService.postData("/myhc-api/v1/screening-plan/create.php", data);
  }

  public updateScreenPlan(data): Observable<any> {
    return this.httpService.postData("/myhc-api/v1/screening-plan/update.php", data);
  }

  public deleteScreenPlan(data): Observable<any> {
    return this.httpService.postData("/myhc-api/v1/screening-plan/delete.php", data);
  }

  public getOneScreenPlan(code): Observable<any> {
    return this.httpService.getData("/myhc-api/v1/screening-plan/read-one.php?c=" + code);
  }

  //added by smajina 2021-08-23
  public getScreenPlanByCategory(code): Observable<any> {
    return this.httpService.getData("/myhc-api/v1/screening-plan/read-by-category-code.php?c=" + code);
  }

  public getScreenPlanByCommercialCategory(code,commercial): Observable<any> {
    return this.httpService.getData("/myhc-api/v1/screening-plan/read-all-by-commercial-category.php?c=" + code + '&s=' + commercial);
  }

  public getScreenPlanByCommercial(commercial): Observable<any> {
    return this.httpService.getData("/myhc-api/v1/screening-plan/read-all-by-commercial.php?s=" + commercial);
  }

  uploadPackagePicture(data): Observable<any> {
    var url = "/myhc-api/v1/screening-plan/upload-files.php";
    return this.httpService.postMultipartData(url,data);
  }
  
  /** SCREENING PLAN - TEST PANEL PACKAGES*/

  public getAllSPPackageTestPanel(): Observable<any> {
    return this.httpService.getData("/myhc-api/v1/package-test-panels/read-all.php");
  }

  public getSPTestPanelPack(pcode, code): Observable<any> {
    return this.httpService.getData("/myhc-api/v1/package-test-panels/read-one.php?c=" + pcode + "&p=" + code);
  }

  public deletePackageTestPanel(data): Observable<any> {
    return this.httpService.postData("/myhc-api/v1/package-test-panels/delete.php", data);
  }

  public updateSPPacTestPanel(data): Observable<any> {
    return this.httpService.postData("/myhc-api/v1/package-test-panels/update.php", data);
  }

  public createSPPacTestPanel(data): Observable<any> {
    return this.httpService.postData("/myhc-api/v1/package-test-panels/create.php", data);
  }

  /** SCREENING PLAN - PACKAGE PATIENT*/

  public getAllSPPatientPack(): Observable<any> {
    return this.httpService.getData("/myhc-api/v1/package-patient/read-all.php");
  }

  public getSPPatientPack(pcode, code): Observable<any> {
    return this.httpService.getData("/myhc-api/v1/package-patient/read-one.php?c=" + pcode + "&p=" + code);
  }

  public deletePackagePatient(data): Observable<any> {
    return this.httpService.postData("/myhc-api/v1/package-patient/delete.php", data);
  }

  public updateSPPackagePatient(data): Observable<any> {
    return this.httpService.postData("/myhc-api/v1/package-patient/update.php", data);
  }

  public createSPPackagePatient(data): Observable<any> {
    return this.httpService.postData("/myhc-api/v1/package-patient/create.php", data);
  }

  /** SCREENING PLAN - PACKAGE TEST GROUP => ADD-ON PACKAGES */
  public getAllSPAddOnPackage(): Observable<any> {
    return this.httpService.getData("/myhc-api/v1/package-test-groups/read-all.php");
  }

  public createSPAddOnPackage(data): Observable<any> {
    return this.httpService.postData("/myhc-api/v1/package-test-groups/create.php", data);
  }

  public updateSPAddOnPackage(data): Observable<any> {
    return this.httpService.postData("/myhc-api/v1/package-test-groups/update.php", data);
  }

  public getSPAddOnPackageOne(pcode,code): Observable<any> {
    var url = "/myhc-api/v1/package-test-groups/read-one.php?c=" + pcode + "&p=" + code;
    return this.httpService.getData(url);
  }

  public searchSPAddOnPackage(code): Observable<any> {
    var url = "/myhc-api/v1/package-test-groups/search.php?s=" +  code;
    return this.httpService.getData(url);
  }
  
  public deleteSPAddOnPackage(data): Observable<any> {
    return this.httpService.postData("/myhc-api/v1/package-test-groups/delete.php", data);
  }

  /** SCREENING PLAN - PACKAGE ADD ON SERVICES*/

  public getAllSPAddOnService(): Observable<any> {
    return this.httpService.getData("/myhc-api/v1/package-add-ons/read-all.php");
  }

  public getSPAddOnService(pcode, code): Observable<any> {
    return this.httpService.getData("/myhc-api/v1/package-add-ons/read-one.php?c=" + pcode + "&p=" + code);
  }

  public createSPAddOnService(data): Observable<any> {
    return this.httpService.postData("/myhc-api/v1/package-add-ons/create.php", data);
  }

  public updateSPAddOnService(data): Observable<any> {
    return this.httpService.postData("/myhc-api/v1/package-add-ons/update.php", data);
  }

  public deleteSPAddOnService(data): Observable<any> {
    return this.httpService.postData("/myhc-api/v1/package-add-ons/delete.php", data);
  }

  public searchSPAddOnService(code): Observable<any> {
    return this.httpService.getData("/myhc-api/v1/package-add-ons/search.php?s=" + code);
  }



  /** LOOKUP */

  public getLPatientType(): Observable<any> {
    return this.httpService.getData("/myhc-api/v1/patient-type/read-all.php");
  }

  public getLLocationType(): Observable<any> {
    return this.httpService.getData("/myhc-api/v1/test-location/read-all.php");
  }




  /** PACKAGE CATEGORY */
  getAllPackageCategory(): Observable<any> {
    var url = "/myhc-api/v1/package-category/read-all.php";
    return this.httpService.getData(url);
  }

  getAllPackageCategoryShowStatus(): Observable<any> {
    var url = "/myhc-api/v1/package-category/read-show.php";
    return this.httpService.getData(url);
  }


  /** SMS NOTIFICATION */
  getSmsContentByCode(code): Observable<any> {
    var url = "/myhc-api/v1/sms-notification/read-one.php?c=" + code;
    return this.httpService.getData(url);
  }

  get6DigitCode(mobile): Observable<any> {
    var url = "/myhc-api/v1/six-digit-code/get-one.php?m=" + mobile;
    return this.httpService.getData(url);
  }

  verify6DigitCode(data): Observable<any> {
    var url = "/myhc-api/v1/six-digit-code/verify.php";
    return this.httpService.postData(url, data);
  }

  sendSms(data): Observable<any> {
    var url = "/myhc-api/v1/sms-notification/send-message.php";
    return this.httpService.postData(url, data);
  }


  /** REGISTRATION */
  preRegister(data): Observable<any> {
    var url = "/myhc-api/v1/pre-registration/create.php";
    return this.httpService.postData(url, data);
  }

  getRegistrationInfo(regNo): Observable<any> {
    var url = "/myhc-api/v1/pre-registration/read-one.php?c=" + regNo;
    return this.httpService.getData(url);
  }

  updateRegField(data): Observable<any> {
    var url = "/myhc-api/v1/pre-registration/update-field.php";
    return this.httpService.postData(url,data);
  }

  deleteApplication(data): Observable<any> {
    var url = "/myhc-api/v1/pre-registration/delete.php";
    return this.httpService.postData(url,data);
  }

  getAllRegistration(): Observable<any> {
    var url = "/myhc-api/v1/pre-registration/read-all.php";
    return this.httpService.getData(url);
  }

  resetPassword(data): Observable<any> {
    var url = "/myhc-api/v1/user-account/reset-password.php";
    return this.httpService.postData(url,data);
  }

  
  /** PERSON REGISTRATION */
  saveNewPerson(person): Observable<any> {
    var url = "/myhc-api/v1/person/create.php";
    return this.httpService.postData(url,person);
  }

  saveUpdatePerson(person): Observable<any> {
    var url = "/myhc-api/v1/person/update.php";
    return this.httpService.postData(url,person);
  }

  registerPerson(person): Observable<any> {
    var url = "/myhc-api/v1/person/create-for-registration.php";
    return this.httpService.postData(url,person);
  }

  verifyIcNoDependent(icNo): Observable<any> {
    var url = "/myhc-api/v1/registration-person/verify-ic-no-dependent.php?c=" + icNo;
    return this.httpService.getData(url);
  }

  removePatient(dependent): Observable<any> {
    var url = "/myhc-api/v1/registration-person/delete.php";
    return this.httpService.postData(url,dependent);
  }

  searchPerson(keyword): Observable<any> {
    var url = "/myhc-api/v1/person/search-registration.php?s="+ keyword;
    return this.httpService.getData(url);
  }

  getPerson(keyword): Observable<any> {
    var url = "/myhc-api/v1/person/read-one.php?c="+ keyword;
    return this.httpService.getData(url);
  }

   /** COMPANY REGISTRATION */
 
  saveNewCompanyInfo(company): Observable<any> {
    var url = "/myhc-api/v1/company/create.php";
    return this.httpService.postData(url,company);
  }

  saveUpdateCompanyInfo(company): Observable<any> {
    var url = "/myhc-api/v1/company/update.php";
    return this.httpService.postData(url,company);
  }



  /** DOCUMENT UPLOAD */
  uploadSupportingDoc(data): Observable<any> {
    var url = "/myhc-api/v1/person-document/upload.php";
    return this.httpService.postMultipartData(url,data);
  }

  uploadSupportingDocV2(data): Observable<any> {
    var url = "/myhc-api/v1/person-document/upload-files.php";
    return this.httpService.postMultipartData(url,data);
  }

  getPersonDocument(ic): Observable<any> {
    var url = "/myhc-api/v1/person-document/read-by-ic-no.php?c=" + ic;
    return this.httpService.getData(url);
  }

  removePersonDocument(personDoc): Observable<any> {
    var url = "/myhc-api/v1/person-document/delete.php";
    return this.httpService.postData(url,personDoc);
  }

  uploadCompanySupportingDoc(data): Observable<any> {
    var url = "/myhc-api/v1/company-document/upload-files.php";
    return this.httpService.postMultipartData(url,data);
  }

  removeCompanyDocument(personDoc): Observable<any> {
    var url = "/myhc-api/v1/company-document/delete.php";
    return this.httpService.postData(url,personDoc);
  }

  getCompanyDocument(co_reg_no): Observable<any> {
    var url = "/myhc-api/v1/company-document/read-by-co-reg-no.php?c=" + co_reg_no;
    return this.httpService.getData(url);
  }


  /** MESSAGE BOX */
  getInboxMessages(uname): Observable<any> {
    var url = "/myhc-api/v1/message-box/read-by-receiver.php?c=" + uname;
    return this.httpService.getData(url);
  }

  getAllInboxMessages(uname): Observable<any> {
    var url = "/myhc-api/v1/message-box/read-all-messages.php?c=" + uname;
    return this.httpService.getData(url);
  }

  sendInboxMessage(msg): Observable<any> {
    var url = "/myhc-api/v1/message-box/create.php";
    return this.httpService.postData(url,msg);
  }

  sendInboxMessageWithAttachment(data): Observable<any> {
    var url = "/myhc-api/v1/message-box/create-with-attachment.php";
    return this.httpService.postMultipartData(url,data);
  }

   /** TEST RESULT */
   getAllPatients(): Observable<any> {
    var url = "/myhc-api/v1/registration-person/read-by-patient.php";
    return this.httpService.getData(url);
  }

  pushTestResult(data): Observable<any> {
    var url = "/myhc-api/v1/test-result-input/push-result.php";
    return this.httpService.postData(url,data);
  }

  deleteTestResult(data): Observable<any> {
    var url = "/myhc-api/v1/test-result-input/delete.php";
    return this.httpService.postData(url,data);
  }

  getAllTestResult(): Observable<any> {
    var url = "/myhc-api/v1/test-result-input/read-all-result.php";
    return this.httpService.getData(url);
  }

  searchTestResult(key): Observable<any> {
    var url = "/myhc-api/v1/test-result-input/search.php?s=" + key;
    return this.httpService.getData(url);
  }

  searchTestResultRef(key): Observable<any> {
    var url = "/myhc-api/v1/test-result-input/result-ref-by-ic-no.php?s=" + key;
    return this.httpService.getData(url);
  }

  searchDashboardResultRef(key): Observable<any> {
    var url = "/myhc-api/v1/test-result-input/result-dashboard-by-ic-no.php?s=" + key;
    return this.httpService.getData(url);
  }

  /** MAIN MENU */
  getMainMenu(owner): Observable<any> {
    var url = "/myhc-api/v1/menu-main/read-by-owner.php?o=" + owner;
    return this.httpService.getData(url);
  }

  getMainMenuLANG(owner,lang): Observable<any> {
    var url = "/myhc-api/v1/menu-main/read-by-owner.php?o=" + owner + "&lang="+ lang;
    return this.httpService.getData(url);
  }

  /** USER ACCOUNT */

  getUserDetails(u): Observable<any> {
    var url = "/myhc-api/v1/user-account/read-one.php?c=" + u;
    return this.httpService.getData(url);
  }

  getAllMauPerson(){
    var url = "/myhc-api/v1/user-account/read-all-mau.php";
    return this.httpService.getData(url);
  }

  getSearchUser(key){
    var url = "/myhc-api/v1/user-account/search.php?s=" + key;
    return this.httpService.getData(url);
  }

  
  changePassword(data){
    var url = "/myhc-api/v1/user-account/change-password.php";
    return this.httpService.postData(url,data);
  }

  /** LOOKUP TABLE */
  getStates(): Observable<any> {
    var url = "/myhc-api/v1/state/read-all.php";
    return this.httpService.getData(url);
  }

  getRelationships(): Observable<any> {
    var url = "/myhc-api/v1/relationship/read-all.php";
    return this.httpService.getData(url);
  }

  getPaymentMethods(): Observable<any> {
    var url = "/myhc-api/v1/payment-method/read-all.php";
    return this.httpService.getData(url);
  }
 
  getDocUploadTypes(code): Observable<any> {
    var url = "/myhc-api/v1/document-upload/read-by-package-category.php?c=" + code;
    return this.httpService.getData(url);
  }

  getHomeScreeningCharging(): Observable<any> {
    var url = "/myhc-api/v1/home-screening-charging/read-all.php";
    return this.httpService.getData(url);
  }

   
  /** CONTENT - UPDATABLE*/
  getContent(c): Observable<any> {
    var url = "/myhc-api/v1/content-mgmt/read-one.php?c=" + c; 
    return this.httpService.getData(url);
  }


  /** ADMIN - SETTINGS*/
  getAllAdmin(): Observable<any> {
    var url = "/myhc-api/v1/user-account/read-all-admin.php"; 
    return this.httpService.getData(url);
  }

  createAdminAccount(data): Observable<any> {
    var url = "/myhc-api/v1/person/create-for-admin.php"; 
    return this.httpService.postData(url,data);
  }
  
  updateAdminAccount(data): Observable<any> {
    var url = "/myhc-api/v1/person/update-admin.php"; 
    return this.httpService.postData(url,data);
  }

  getAllGroup(): Observable<any> {
    var url = "/myhc-api/v1/menu-main/read-all.php"; 
    return this.httpService.getData(url);
  }

  createGroup(data): Observable<any> {
    var url = "/myhc-api/v1/menu-main/create-group.php"; 

    
    return this.httpService.postData(url,data);
  }

  updateGroup(data): Observable<any> {
    console.log(data);
    var url = "/myhc-api/v1/menu-main/update-group.php"; 
    return this.httpService.postData(url,data);
  }

  getDistinctGroup(): Observable<any> {
    var url = "/myhc-api/v1/menu-main/read-all-uniqe-group.php"; 
    return this.httpService.getData(url);
  }

  getMainMenuAllItem(): Observable<any> {
    var url = "/myhc-api/v1/menu-main/read-all.php"; 
    return this.httpService.getData(url);
  }

  getMenuAllItem(): Observable<any> {
    var url = "/myhc-api/v1/menu-item/read-all.php"; 
    return this.httpService.getData(url);
  }

  addMenuItem(data): Observable<any> {
    var url = "/myhc-api/v1/menu-main-item/create.php"; 
    return this.httpService.postData(url,data);
  }

  deleteMenuItem(data): Observable<any> {
    var url = "/myhc-api/v1/menu-main-item/delete.php"; 
    return this.httpService.postData(url,data);
  }

  getMenuItemById(item_id, main_id){
    var url = "/myhc-api/v1/menu-main-item/read-one.php?item_id=" + item_id + "&main_id=" + main_id;
    return this.httpService.getData(url);
  }

  updateMenuItem(data): Observable<any> {
    var url = "/myhc-api/v1/menu-main-item/update.php"; 
    return this.httpService.postData(url,data);
  }

  /** SCREENING CENTER */

  getAllScreeningCenters(){
    var url = "/myhc-api/v1/screening-center/read-all.php";
    return this.httpService.getData(url);
  }

  getScreeningCenterById(id){
    var url = "/myhc-api/v1/screening-center/read-one.php?id=" + id;
    return this.httpService.getData(url);
  }

  createScreeningCenterById(data){
    var url = "/myhc-api/v1/screening-center/create.php";
    return this.httpService.postData(url,data);
  }

  updateScreeningCenterById(data){
    var url = "/myhc-api/v1/screening-center/update.php";
    return this.httpService.postData(url,data);
  }

  getScreeningCenterTimeslots(id){
    var url = "/myhc-api/v1/screening-center-timeslot/read-by-screening-center-id.php?c=" + id;
    return this.httpService.getData(url);
  }

  getAllTimeslots(){
    var url = "/myhc-api/v1/timeslot/read-all.php";
    return this.httpService.getData(url);
  }

  getAvailableScreeningDateRangeAndTimeslot(scid,sdate,edate){
    var url = "/myhc-api/v1/screening-center-timeslot/read-timeslot-availabilty-by-screening-center.php?scid="+scid
      +"&sdate="+sdate+"&edate="+edate;
    return this.httpService.getData(url);
  }

  getAvailableScreeningDateAndTimeslot(scid,sdate){
    var url = "/myhc-api/v1/screening-center-timeslot/read-timeslot-availabilty-by-date-and-center.php?scid="+scid
      +"&sdate="+sdate;
    return this.httpService.getData(url);
  }

  createScreeningCentre(data): Observable<any> {
    return this.httpService.postData("/myhc-api/v1/screening-center/create.php", data);
  }

  deleteScreeningCentre(data): Observable<any> {
    return this.httpService.postData("/myhc-api/v1/screening-center/delete.php", data);
  }

  uploadCenterPhoto(data): Observable<any> {
    var url = "/myhc-api/v1/screening-center/upload-photo.php";
    // var url = "/myhc-api/v1/screening-center/test-upload.php";
    return this.httpService.postMultipartData(url,data);
  }

  updateScreeningCentre(data): Observable<any>{
    return this.httpService.postData("/myhc-api/v1/screening-center/update.php", data);
  }

  addCentreTimeSlot(data): Observable<any> {
    return this.httpService.postData("/myhc-api/v1/screening-center-timeslot/create.php", data);
  }
  
  deleteCentreTimeSlot(data): Observable<any> {
    return this.httpService.postData("/myhc-api/v1/screening-center-timeslot/delete.php", data);
  }



  /** BOOKING */
  getAllBookings(){
    var url = "/myhc-api/v1/booking/read-all.php";
    return this.httpService.getData(url);
  }

  searchBookings(keyword,status){
    var url = "/myhc-api/v1/booking/search-by-status.php?k=" + keyword + "&s=" + status;
    return this.httpService.getData(url);
  }

  searchBookingByRegNo(regNo,keyword,keysearch){
    var url = "/myhc-api/v1/booking/search-by-status-and-reg-no.php?rn=" + regNo + "&kw=" + keyword + "&ks=" + keysearch;
    return this.httpService.getData(url);
  }

  getBooking(bNo){
    var url = "/myhc-api/v1/booking/read-one.php?c=" + bNo;
    return this.httpService.getData(url);
  }

  getBookingByRegNo(rNo){
    var url = "/myhc-api/v1/booking/read-by-reg-no.php?c=" + rNo;
    return this.httpService.getData(url);
  }

  getUpcomingBookingByRegNo(rNo){
    var url = "/myhc-api/v1/booking/upcoming-by-reg-no.php?c=" + rNo;
    return this.httpService.getData(url);
  }

  submitBooking(data){
    var url = "/myhc-api/v1/booking/create.php";
    return this.httpService.postData(url,data);
  }

  submitChangeBooking(data){
    var url = "/myhc-api/v1/booking/change-booking.php";
    return this.httpService.postData(url,data);
  }

  updateBooking(data,type){
    var url = "/myhc-api/v1/booking/update-booking-details.php?t=" + type;
    return this.httpService.postData(url,data);
  }

  submitIpay88(data){
    var url = "/myhc-api/v1/ipay88/request.php";
    return this.httpService.postData(url,data);
  }

  printReceipt(bNo){
    var url = "/myhc-api/pdf/print-receipt.php?bNo=" + bNo;
    return this.httpService.downloadPdf(url);
  }

  printReceiptByRefNo(bNo,rNo){ 
    var url = "/myhc-api/pdf/print-receipt-refno.php?bNo=" + bNo + "&rNo=" + rNo;
    return this.httpService.downloadPdf(url);
  }

  printTestForm(bNo){
    var url = "/myhc-api/pdf/print-test-form.php?bNo=" + bNo;
    return this.httpService.downloadPdf(url);
  }

  /** TRANSACTIONS */
  getAllTransactions(){
    var url = "/myhc-api/v1/transaction/read-all.php";
    return this.httpService.getData(url);
  }

  searchTransactions(keyword){
    var url = "/myhc-api/v1/transaction/search-keyword.php?k=" + keyword;
    return this.httpService.getData(url);
  }

  /** AUDITRAIL **/
  getAllAuditrail(){
    var url = "/myhc-api/v1/auditrail/read-all.php";
    return this.httpService.getData(url);
  }

  public createAuditrail(dataAut): Observable<any>{
    var url = "/myhc-api/v1/auditrail/create.php";
    return this.httpService.postData(url,dataAut);
  }



  /* TIME SLOT */

  public createTimeSlot(data): Observable<any>{
    return this.httpService.postData("/myhc-api/v1/timeslot/create.php", data);
  }

  public updateTimeSlot(data): Observable<any>{
    return this.httpService.postData("/myhc-api/v1/timeslot/update.php", data);
  }

  public deleteTimeSlot(data): Observable<any>{
    return this.httpService.postData("/myhc-api/v1/timeslot/delete.php", data);
  }

  /* MEMBER MANAGEMENT */
  getAllMember(){
    var url = "/myhc-api/v1/member-mgmt/read-all.php";
    return this.httpService.getData(url);
  }

  addNewMember(data){
    var url = "/myhc-api/v1/member-mgmt/create.php";
    return this.httpService.postData(url,data);
  }


  /** NOT RELATED */
  /** PATIENT */
  getPatientDetails(refno): Observable<any> {
    var url = "/api/patient/read_one.php?refno" + refno;
    return this.httpService.getData(url);
  }

  // getAllPatients(): Observable<any> {
  //   var url = "/api/patient/read_all.php";
  //   return this.httpService.getData(url);
  // }

  createPatientInfo(data) {
    var url = "/api/patient/create.php";
    return this.httpService.postData(url, data);
  }

  updatePatientInfo(data) {
    var url = "/api/patient/update.php";
    return this.httpService.postData(url, data);
  }

  searchPatientInfo(key) {
    var url = "/api/patient/search.php?s=" + key;
    return this.httpService.getData(url);
  }

  deletePatient(refno) {
    let data = {
      "refno": refno
    }
    var url = "/api/patient/delete.php";
    return this.httpService.postData(url, data);
  }

  /** HEALTH RESULT */

  getHealthResultByRefNoAndResultName(refno, name): Observable<any> {
    var url = "/api/health_result/readByRefNoAndResultName.php?refno=" + refno + "&result_name=" + name;
    return this.httpService.getData(url);
  }

  uploadResult(data): Observable<any> {
    var url = "/api/health_result/create.php";
    return this.httpService.postData(url, data);
  }


 
}
