import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

/**
 * Created by: smajina 18 May 2020
 */

@Injectable({
  providedIn: 'root'
})
export class ChartConfigService {
  private data: Observable<any>;

  constructor() { }



  public chartColors = ["#008FFB",
    "#00E396",
    "#FEB019",
    "#FF4560",
    "#775DD0",
    "#1B5E20",
    "#26a69a",
    "#D10CE8",
    "#4FC3F7",
    "#9E9D24",
    "#FFEB3B",
    "#FFAB91",
    "#B0BEC5",
    "#8D6E63"];



  public lineChartMarker = {
    series: [
      // {
      //   name: "High - 2013",
      //   data: [28, 29, 33, 36, 32, 32, 33]
      // },
      // {
      //   name: "Low - 2013",
      //   data: [12, 11, 14, 18, 17, 13, 13]
      // }
    ],
    chart: {
      height: 350,
      type: 'line',
      // dropShadow: {
      //   enabled: true,
      //   color: '#000',
      //   top: 18,
      //   left: 7,
      //   blur: 10,
      //   opacity: 0.2
      // },
      toolbar: {
        show: false
      }
    },
    colors: ['#1E88E5', '#FF5733','#673AB7'],
    dataLabels: {
      enabled: true,
    },
    stroke: {
      // curve: 'smooth'
    },
    title: {
      text: 'Test marker title',
      align: 'left'
    },
    grid: {
      borderColor: '#e7e7e7',
      row: {
        colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
        opacity: 0.5
      },
      yaxis: {
        lines: {
          show: true
        }
      },
  
    },
    markers: {
      // size: 0.5
    },
    xaxis: {
      categories: [],
      title: {
        text: 'xaxis title'
      }
    },
    yaxis: {
      title: {
        text: 'yaxis title'
      },
      min: 0,
      max: 40
    },
    legend: {
      position: 'top',
      horizontalAlign: 'right',
      floating: true,
      offsetY: -25,
      offsetX: -5
    },
    annotations: {
      yaxis: [
        // {
        //   y: 3.9,
        //   y2: 6,
        //   borderColor: '#000',
        //   fillColor: '#FEB019',
        //   label: {
        //     text: 'Normal'
        //   }
        // }
      ]
    }
  };

  public lineChart = {
    chart: {
      height: 400,
      width: '100%',
      type: 'line',
      zoom: {
        enabled: false
      }
    },
    dataLabels: {
      enabled: true,
      width: 2,
    },
    stroke: {
      curve: 'smooth',
    },
    colors: this.chartColors,
    series: [],
    title: {
      text: '',
      align: 'left'
    },
    grid: {
      row: {
        opacity: 0.5
      },
    },
    xaxis: {
      categories: [],
      title: {
        text: ''
      },
      labels: {
        rotate: -90,
        rotateAlways: false
      }
    },
    yaxis: {
      title: {
        text: ''
      }
    }
  };


  public horizontalBar = {
    chart: {
      type: 'bar',
      height: 400,
      width: '100%'
    },
    plotOptions: {
      bar: {
        horizontal: true,
        dataLabels: {
          position: 'top',
        },
      }
    },
    colors: this.chartColors,
    dataLabels: {
      enabled: true,
      style: {
        fontSize: '12px',
        colors: ['#000000']
      }
    },
    stroke: {
      show: true,
      width: 1,
      colors: ['#fff']
    },
    series: [],
    xaxis: {
      categories: [],
      labels: {
        rotate: -45
      }
    },
    title: {
      text: '',
      align: 'left'
    },
    yaxis: {
      title: {
        text: ''
      }
    },
    responsive: [
      {
        breakpoint: 400,
        options: {
          legend: {
            position: "bottom",
            offsetX: -10,
            offsetY: 0
          }
        }
      }
    ],
  };

  public lineChart2 = {
    series: [
      {
        name: "High - 2013",
        data: [28, 29, 33, 36, 32, 32, 33]
      },
      {
        name: "Low - 2013",
        data: [12, 11, 14, 18, 17, 13, 13]
      }
    ],
    chart: {
      height: 350,
      type: "line",
      dropShadow: {
        enabled: true,
        color: "#000",
        top: 18,
        left: 7,
        blur: 10,
        opacity: 0.2
      },
      toolbar: {
        show: false
      }
    },
    colors: ["#77B6EA", "#545454"],
    dataLabels: {
      enabled: true
    },
    stroke: {
      curve: "smooth"
    },
    title: {
      text: "Average High & Low Temperature",
      align: "left"
    },
    grid: {
      borderColor: "#e7e7e7",
      row: {
        colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
        opacity: 0.5
      }
    },
    markers: {
      size: 1
    },
    xaxis: {
      categories: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul"],
      title: {
        text: "Month"
      }
    },
    yaxis: {
      title: {
        text: "Temperature"
      },
      min: 5,
      max: 40
    },
    legend: {
      position: "top",
      horizontalAlign: "right",
      floating: true,
      offsetY: -25,
      offsetX: -5
    }
  }

  public areaChart = {
    chart: {
      height: 350,
      type: 'area',
    },
    title: {
      text: "",
      align: "left",
    },
    dataLabels: {
      enabled: false
    },
    stroke: {
      curve: 'smooth'
    },
    markers: {
      size: 2,
      colors: ["#2E93fA"],
      strokeColor: "#2E93fA",
      strokeWidth: 3
    },
    fill: {
      opacity: 0.5
    },
    colors: ['#2E93fA', '#28B463'],
    series: [],
    xaxis: {
      categories: [],
      labels: {
        rotate: -90,
        rotateAlways: false
      }
    },
    tooltip: {
      x: {
        format: ''
      },
    },
    yaxis: {
      title: {
        text: ''
      }
    }
  };

  public columnLineChart = {
    series: [
      // {
      //   name: "Website Blog",
      //   type: "column",
      //   data: [440, 505, 414, 671, 227, 413, 201, 352, 752, 320, 257, 160]
      // },
    ],
    colors: this.chartColors,
    chart: {
      width: '100%',
      height: 400,
      type: "line"
    },
    // stroke: {
    //   width: [0, 4]
    // },
    stroke: {
      show: true,
      curve: 'smooth',
      lineCap: 'butt',
      width: 3,
      dashArray: 0,
    },
    title: {
      text: ""
    },
    dataLabels: {
      enabled: true,
      // enabledOnSeries: [1]
    },
    labels: [],
    yaxis: [
      {
        title: {
          text: ""
        }
      },
      {
        opposite: true,
        title: {
          text: ""
        }
      }
    ]
  };

  public barChart = {
    series: [],
    chart: {
      type: 'bar',
      height: 400,
      width: '100%'
    },
    colors: this.chartColors,
    plotOptions: {
      bar: {
        horizontal: false,
      }
    },
    dataLabels: {
      enabled: false
    },
    xaxis: {
      title: {
        text: ''
      }
    },
    yaxis: {
      title: {
        text: ''
      }
    }
  };

  public barChartStack100Percent = {
    chart: {
      // height: 350,
      width: "100%",
      type: 'bar',
      stacked: true,
      stackType: "100%"
    },
    title: {
      text: '',
      align: 'left'
    },
    dataLabels: {
      enabled: true,
      style: {
        fontSize: '9px',
        colors: ['#000000']
      }
    },
    colors: this.chartColors,
    series: [],
    xaxis: {
      labels: {
        rotate: -90,
        rotateAlways: false
      },
      categories: []
    },
    yaxis: {
      title: {
        text: ''
      }
    },
    fill: {
      opacity: 1
    },
    annotations: {
      xaxis: []
    }
  };

  public pieChart = {
    chart: {
      width: '100%',
      type: 'pie',
    },
    labels: [],
    series: [],
    colors: ['#714cfe', '#b39afd'],
    legend: {
      show: true,
      position: 'right',
    },
    dataLabels: {
      enabled: true,
      dropShadow: {
        enabled: false,
      }
    },
    responsive: [{
      breakpoint: 480,
      options: {
        chart: {
          width: 200
        },
        legend: {
          position: 'bottom'
        }
      }
    }]
  };

  public heatMapChart = {
    series: [],
    chart: {
      width: '100%',
      type: "heatmap"
    },
    dataLabels: {
      enabled: true,
      style: {
        colors: ["#000000"]
      }
    },
    xaxis: {
      type: "category",
      categories: [],
      title: {
        text: "xaxis",
      }
    },
    yaxis: {
      show: true,
      title: {
        text: "yaxis"
      }
    },
    colors: ["#008FFB"],
    title: {
      text: ""
    },
    events: {
      click: function (event, chartContext, config) {
        console.log('click');
      },
      dataPointSelection: function (event, chartContext, config) {
        console.log('dataPointSelection');
      },
      selection: function (event, chartContext, config) {
        console.log('selection');
      }
    }
  };

}


