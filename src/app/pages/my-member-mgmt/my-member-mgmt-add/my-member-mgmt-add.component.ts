import { Component, OnInit } from '@angular/core';
import { TemplateRef } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { DataService } from '../../../service/data.service';
import { Router } from '@angular/router';
import { GENDER } from '../../../shared/constant/constant';

@Component({
  selector: 'app-my-member-mgmt-add',
  templateUrl: './my-member-mgmt-add.component.html',
  styleUrls: ['./my-member-mgmt-add.component.scss']
})
export class MyMemberMgmtAddComponent implements OnInit {

   modalRef: BsModalRef;
   loading = true;
  
  constructor(
    private modalService: BsModalService,
    public translate: TranslateService,
    private formBuilder: FormBuilder,
    private router: Router,
    private toastrService: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    private dataService: DataService,
  ) { }

  genderList = [];
  ngOnInit() {
    this.genderList = GENDER;
    this.newMember();
  }

  newMember() {
    this.isSubmitted = false;
    this.formDetails = this.formBuilder.group({
      name: [null, Validators.required],
      // password: null,
      nric: [null, Validators.required],
      dob: [null, Validators.required],
      age: [null, Validators.required],
      gender: [null, Validators.required],
      address: [null, Validators.required]
    });
  }

  formDetails: FormGroup;
  isSubmitted=false;
  addNewMember() {
    this.isSubmitted = true;
    if (this.formDetails.invalid){
      this.translate.get('member-mgmt').subscribe(data => {
        this.toastrService.warning(data.notification.title_incomplete, data.notification.title_incompleteTitle);
      })
      
      return;
    }

    this.translate.get('member-mgmt').subscribe(data => {
      this.coolDialogs.confirm(data.notification.title_confirmAdd)
      .subscribe(res => {
        if (res) {
          console.log(this.formDetails.value);
          this.dataService.addNewMember(this.formDetails.value).subscribe(result => {
            this.router.navigate(['/my-member-mgmt/main']);
            this.toastrService.success(data.notification.title_success, data.notification.title_successTitle);
          }, err => {
            this.toastrService.error(err.error.message, "Process failed");
          });
          this.isSubmitted = false;
        }
      });
    })
    
  }

}
