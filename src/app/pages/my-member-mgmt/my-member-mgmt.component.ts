import { Component, OnInit } from '@angular/core';
import { TemplateRef } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { DataService } from '../../service/data.service';

@Component({
  selector: 'app-my-member-mgmt',
  templateUrl: './my-member-mgmt.component.html',
  styleUrls: ['./my-member-mgmt.component.scss']
})
export class MyMemberMgmtComponent implements OnInit {

   modalRef: BsModalRef;
   loading = true;
  
  constructor(
    private modalService: BsModalService,
    public translate: TranslateService,
    private dataService: DataService,
  ) { }

  ngOnInit() {
    this.fetchMember();
  }

  dataMember: any;
  fetchMember() {
    this.loading = true;
    this.dataMember = [];
    this.dataService.getAllMember().subscribe(res => {
      this.dataMember = res.data;
      this.loading = false;
    }, error => {
      this.loading = false;
    })
  }

}
