import { AfterViewInit, ChangeDetectorRef, Input, TemplateRef, ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { DataService } from '../../service/data.service';
import { ToastrService } from 'ngx-toastr';
import * as _ from "lodash";
import { environment } from '../../../environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { MatStepper } from '@angular/material';
import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { DatePipe } from '@angular/common';

import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-receipt',
  templateUrl: './receipt.component.html',
  // styleUrls: [
  //   './receipt.scss'
  // ]
})
export class ReceiptComponent implements OnInit  {

  constructor(
    private modalService: BsModalService,
    private datePipe: DatePipe,
    private coolDialogs: NgxCoolDialogsService,
    private dataService: DataService,
    private toastrService: ToastrService,
    private route: ActivatedRoute,
    private cdr: ChangeDetectorRef,
    private router:Router,
    public translate: TranslateService
  ) {}

  @Input('bookingNo') bookingNo: any;
  @Input('transNo') transNo: any;

  user: any;
  dataRegistration:any;
  dataBookingPayment :any;
  auth: any;
  dataPatients: any;
  loading = true;
  registrationPersons: any = [];
  picturePath:any;
  receiptTotal=0.00;
  ngOnInit() {
    this.picturePath = environment.localhost + "upload/";
    // let bookingNo = this.route.snapshot.paramMap.get('bookingno');
    // let transid = this.route.snapshot.paramMap.get('transid');

    this.auth = JSON.parse(localStorage.getItem("auth"));
    this.dataRegistration = JSON.parse(localStorage.getItem("plan-registration"));

    this.auth = JSON.parse(localStorage.getItem("auth"));

    this.dataService.getBooking(this.bookingNo).subscribe(data=>{
      let paymentByTransId = _.filter(data.payments,{trans_no: this.transNo});

      if (paymentByTransId.length>0) this.receiptTotal = paymentByTransId[0].amount;
      this.dataBookingPayment = data;
      this.dataBookingPayment.payments = paymentByTransId;

      console.log(data);
    });

    this.dataService.getUserDetails(this.auth.username).subscribe(user => {
      this.user = user;
    }, error => {
      this.loading = false;
    });
  }

  mode: any;

  titleCaseWord(word: string) {
    if (!word) return word;
    return word[0].toUpperCase() + word.substr(1).toLowerCase();
  }

  backToMarketplaceListing(){
    this.router.navigate(["marketplace","mp-listing"]);
  }

}
