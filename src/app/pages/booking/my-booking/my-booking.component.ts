import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as _ from "lodash";

import { DataService } from '../../../service/data.service';
import {TranslateService} from '@ngx-translate/core';

import { environment } from '../../../../environments/environment';


@Component({
  selector: 'app-my-booking',
  templateUrl: './my-booking.component.html',
  styleUrls: [
    './my-booking.component.scss' 
  ]
})
export class MyBookingComponent implements OnInit {
  
  loading=true;
  user:any;
  dataPerson:any=[];
  dataRegistration:any;
  auth:any;

  constructor(
    private router: Router,
    private dataService: DataService,
    public translate: TranslateService
  ) {}

  mauAccount:any;
  mauPhoto:any;
  dataBookings:any;
  dataPatients: any;
  staffPatient: any;
  ngOnInit() {

    this.auth = JSON.parse(localStorage.getItem("auth"));
    
    if (localStorage.getItem("plan-registration")){
      this.dataRegistration = JSON.parse(localStorage.getItem("plan-registration"));

      this.dataService.getUserDetails(this.auth.username).subscribe(res=> {
        this.user = res;
        if (this.dataRegistration.screening_plan.category_code == 'CORPORATE') 
          this.staffPatient = 'staff'; else this.staffPatient = 'patient';
        if (this.user.acc_type_code == 'STAFF') {
          this.dataPatients = _.filter(this.dataRegistration.registration_persons, { 'ic_no': this.user.ic_no });
        } else {
          this.dataPatients = _.filter(this.dataRegistration.registration_persons, { 'person_type_code': 'PATIENT' });
      }
        this.loading=false;
      }, error=>{
        this.loading=false;
      });


      this.dataPerson = this.dataRegistration.registration_persons;
      let mau = _.filter(this.dataPerson,{admin_type:'MAU'});
      if (mau.length>0) {
        this.mauAccount = mau[0];
        this.mauPhoto = environment.uploadPath + this.mauAccount.person.photo_path;
      }
      
      this.getUpcomingBookings();

      
    }else{
      this.router.navigate(['/mau']);
    }

  }

  loadingBooking=true;
  getUpcomingBookings() {
    this.loadingBooking=true;
    this.dataService.getUpcomingBookingByRegNo(this.dataRegistration.reg_no).subscribe(res=> {
      this.dataBookings = res;
      this.loadingBooking=false;
    }, error=>{
      this.loadingBooking=false;
    });
  }

  keyword:any;
  keysearch="date e.g. YYYY-MM-DD";
  loadingPreviousBooking=false;
  dataPreviousBooking:any;
  searchBooking(){
   
    this.loadingPreviousBooking=true;
    let key = this.keysearch;
    if (this.keysearch.indexOf("date")!=-1) key = "date";  

    this.dataService.searchBookingByRegNo(this.dataRegistration.reg_no,this.keyword,key).subscribe(res=>{
      console.log(res);
      this.dataPreviousBooking = res;
      this.loadingPreviousBooking=false;
    },error=>{
      this.loadingPreviousBooking=false;
    })
  }

  receiptBookingNo: any;
  receiptTransNo: any;
  loadingReceipt=false;
  printReceipt(payment) {
    const arrRefNo = payment.ref_no.split("#");
    this.receiptBookingNo = payment.booking_no;
    this.receiptTransNo = payment.trans_no;
    this.loadingReceipt = true;
    this.dataService.printReceiptByRefNo(this.receiptBookingNo,arrRefNo[1]).subscribe(res => {
      var file = new Blob([res], { type: 'application/pdf' });
      var fileURL = URL.createObjectURL(file);
      window.open(fileURL);
      this.loadingReceipt = false;
    }, error => {
      console.log(error);
      this.loadingReceipt = false;
    })
  }

  viewBookingDetails(bNo){
    this.router.navigate(['/marketplace','mp-booking','update',bNo]);
  }
  
  book() {}

  
}

 