import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import * as _ from "lodash";

import { DataService } from '../../../service/data.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-my-booking-add',
  templateUrl: './my-booking-add.component.html',
  styleUrls: [
    './my-booking-add.component.scss' 
  ]
})
export class MyBookingAddComponent implements OnInit {
  
  registrationPersons: any = [];
  confirmation = false;

  dataBookingPayment = {
    booking_no: null, reg_no: null,
    ic_no: null,
    products: [],
    screening_location: null, screening_center_id: null, screening_date: null, screening_time: null,
    total_payment: 0.00,
    total_paid: 0.00,
    patients: [],
    payments: [],
    date_modified: null
  };

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private toastrService: ToastrService,
    private dataService: DataService,
    public translate: TranslateService
  ) {}

  @Input('data-registration') dataRegistration: any;

  auth: any;
  user: any;
  updateMode: any;
  step: any;
  planTestItems:any;
  planServices:any;
  ngOnInit() {
    this.auth = JSON.parse(localStorage.getItem("auth"));
    this.dataRegistration = JSON.parse(localStorage.getItem("plan-registration"));
    this.registrationPersons = _.filter(this.dataRegistration.registration_persons, { person_type_code: 'PATIENT' });
    this.updateMode = this.route.snapshot.paramMap.get('mode');

    this.dataService.getUserDetails(this.auth.username).subscribe(user=> {
      this.user = user;
    });
 
    this.planTestItems = this.dataRegistration.screening_plan.package_test_groups;
    this.planServices = this.dataRegistration.screening_plan.package_add_ons;

    if (this.updateMode && this.updateMode == 'update') {

    } else {
      this.step = 1;
    }

    
  }

  goToStepNo(no) {
    if (no == 2) {
      if (!this.verifyStep1()) return;
    }
    if (no == 3) {

      
    }
    if (no == 4) {
 
    }

    this.step = no;
   
  }
  

  dataScreeningCenters: any;
  verifyStep1() {
    if (this.dataBookingPayment.patients.length == 0) {
      this.toastrService.warning("Please select at least one user for booking");
      return false;
    } else {
      //this.dataBookingPayment.patients = _.filter(this.registrationPersons, { checked: true });
      console.log(this.dataBookingPayment);
      return true;

      
    }
  }
  
  
}

 