import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule, HttpClient} from '@angular/common/http';
 
import {ChartModule} from 'angular2-chartjs';
import {SharedModule} from '../../shared/shared.module';
import { ContactSupportComponent } from './contact-support.component';
 
import { QRCodeSVGModule } from 'ngx-qrcode-svg';
import { TooltipModule } from 'ngx-bootstrap';
 
import { MaterialModule } from '../../shared/material-module';
import { ContactSupportRoutingModule } from './contact-support-routing.module';
// import { InputBookingComponent } from './input/input-booking.component';

@NgModule({
  imports: [
    CommonModule,
    ContactSupportRoutingModule,
    SharedModule,
    ChartModule,
    QRCodeSVGModule,
    TooltipModule.forRoot(),
    MaterialModule,
    HttpClientModule,
  ],
  declarations: [
    // InputBookingComponent,
    ContactSupportComponent,
  ]
})
export class ContactSupportModule { }
