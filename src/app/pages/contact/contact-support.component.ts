import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../../src/app/service/data.service';

import {TranslateService} from '@ngx-translate/core';

declare const AmCharts: any;
declare const $: any;

@Component({
  selector: 'app-contact-support',
  templateUrl: './contact-support.component.html',
})
export class ContactSupportComponent implements OnInit {
 

  constructor(private dataService: DataService, public translate: TranslateService) {}

  content:any;
  ngOnInit() {

    this.dataService.getContent("contact-support").subscribe(
      res=>{
        this.content = res.content;
      }
    );
   
  }

   

}
 