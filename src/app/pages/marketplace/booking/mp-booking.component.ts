import { AfterViewInit, ChangeDetectorRef, ElementRef, Input, TemplateRef, ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { DataService } from '../../../service/data.service';
import { ToastrService } from 'ngx-toastr';
import * as _ from "lodash";
import { environment } from '../../../../environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { MatStepper } from '@angular/material';
import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { DatePipe } from '@angular/common';
import * as sha256 from 'js-sha256';
import { HttpClient } from '@angular/common/http';

import {TranslateService} from '@ngx-translate/core';


export class Product {
  public booking_id: number;
  public booking_no: string;
  public patient_type: string;
  public code: string;
  public name: string;
  public price: number;
  public quantity: number;
  public total_price: number;
  public remark: string;

  constructor(booking_id: number, booking_no: string, patient_type: string, code: string, name: string, price: number,
    quantity: number, total_price: number, remark: string) {
    this.booking_id = booking_id;
    this.booking_no = booking_no;
    this.patient_type = patient_type;
    this.code = code;
    this.name = name;
    this.price = price;
    this.quantity = quantity;
    this.total_price = total_price;
    this.remark = remark;



  }
}

@Component({
  selector: 'app-mp-booking',
  templateUrl: './mp-booking.component.html',
  styleUrls: [
    './mp-booking.component.scss'
  ]
})
export class MpBookingComponent implements OnInit {

  constructor(
    private modalService: BsModalService,
    private datePipe: DatePipe,
    private coolDialogs: NgxCoolDialogsService,
    private dataService: DataService,
    private toastrService: ToastrService,
    private route: ActivatedRoute,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private formBuilder: FormBuilder,
    private http: HttpClient,
    public translate: TranslateService
  ) {}


  modalRef: BsModalRef;
  @Input('data-registration') dataRegistration: any;
  @ViewChild('stepper', { static: false }) private stepper: MatStepper;

  user: any;
  mainProduct: Product;

  dataBookingPayment = {
    booking_no: null, reg_no: null,
    ic_no: null,
    products: [],
    screening_location: null, screening_center_id: null, screening_date: null, screening_time: null,
    total_payment: 0.00,
    total_paid: 0.00,
    patients: [],
    payments: [],
    date_modified: null
  };

  dataBookingUpdate = {
    booking_id: null, booking_no: null, screening_location: null, screening_center_id: null, screening_date: null,
    screening_time: null, products: [], total_payment: 0.0
  }

  auth: any;
  dataPatients: any;
  staffPatient: any;
  loading = true;
  registrationPersons: any = [];
  bsInlineValue: any;
  minDate: any;
  maxDate: any;
  disabledDates = [];
  updateMode: any;
  step: any;
  isDateMore72Hours:any=false;
  serverhost:any;
  qrValue: any;

  ngOnInit() {
    this.serverhost = environment.localhost;
    let productCode = this.route.snapshot.paramMap.get('prodcode');
    let productType = this.route.snapshot.paramMap.get('type');
    let packageCode = this.route.snapshot.paramMap.get('pkgcode');
    this.updateMode = this.route.snapshot.paramMap.get('mode');

    if (!this.updateMode) this.updateMode = "NEW";
    let bookingNo = this.route.snapshot.paramMap.get('bookno');

    this.auth = JSON.parse(localStorage.getItem("auth"));
    this.dataRegistration = JSON.parse(localStorage.getItem("plan-registration"));
    console.log(this.dataRegistration)

    if (this.updateMode && this.updateMode == 'update') {
      //update booking
      this.fetchBooking(bookingNo);
      this.step = 8;
      this.loading = true;

    } else {
      //new booking
      this.dataBookingPayment.ic_no = this.dataRegistration.ic_no;
      this.dataBookingPayment.reg_no = this.dataRegistration.reg_no;
      this.qrValue = this.dataBookingPayment.booking_no;
      this.step = 2;
      if (productType == 'AOT') {
        this.dataService.getAddOnPackageOne(productCode).subscribe(product => {
          this.mainProduct = new Product(0, null, product.patient_type, productCode, product.group_name, product.price, 0, 0, 'AOT');
          this.dataBookingPayment.products.push(this.mainProduct);
        });
      } else if (productType == 'AOS') {
        this.dataService.getOneAddOn(productCode).subscribe(product => {
          this.mainProduct = new Product(0, null, product.patient_type_code, productCode, product.name, product.price, 0, 0, 'AOS');
          this.dataBookingPayment.products.push(this.mainProduct);
        });
      }

    }


    this.minDate = new Date();
    this.maxDate = new Date();
    this.minDate.setDate(this.minDate.getDate() + 7);
    this.maxDate.setDate(this.maxDate.getDate() + 21);

    this.dataService.getAllScreeningCenters().subscribe(scenter => {
      this.dataScreeningCenters = scenter.data;
    });

    this.registrationPersons = _.filter(this.dataRegistration.registration_persons, { person_type_code: 'PATIENT' });

    this.auth = JSON.parse(localStorage.getItem("auth"));

    this.dataService.getUserDetails(this.auth.username).subscribe(user => {
      this.user = user;
      this.fetchRegistrationInfo();
    }, error => {
      this.loading = false;
    });
  }

  fetchBooking(bookingNo) {
    this.dataService.getBooking(bookingNo).subscribe(data => {
      this.dataBookingPayment = data;
      this.dataBookingUpdate.booking_id = data.booking_id;
      this.dataBookingUpdate.booking_no = data.booking_no;
      this.dataBookingUpdate.screening_center_id = this.dataBookingPayment.screening_center_id;
      this.dataBookingUpdate.screening_location = this.dataBookingPayment.screening_location;
      this.qrValue = this.dataBookingPayment.booking_no;

      if (this.dataBookingPayment.screening_location == 'HOME') this.isHomeSelected = true; else this.isHomeSelected = false;
      this.dataBookingUpdate.screening_date = this.dataBookingPayment.screening_date;
      this.dataBookingUpdate.screening_time = this.dataBookingPayment.screening_time;
      this.dataBookingPayment.total_payment = 0;
      this.dataBookingPayment.products.forEach(element => {
        this.dataBookingPayment.total_payment += parseFloat(element.total_price);
      });

      this.dataBookingPayment.total_paid = 0.00;
      data.payments.forEach(element => {
        this.dataBookingPayment.total_paid += parseFloat(element.amount);
      });

      this.dataBookingPayment.screening_location = this.dataBookingPayment.screening_location ? this.dataBookingPayment.screening_location : null;
      let daysLeft = this.calculateDiff(this.dataBookingPayment.screening_date);

      if (Math.floor(daysLeft)>3) this.isDateMore72Hours = true;
      // console.log("dayLeft",daysLeft,this.isDateMore72Hours,Math.floor(daysLeft)>Math.floor(3));
      
      this.loading = false;
    }, error => {
      this.toastrService.error("Could not fetch booking information");
      this.loading = false;
    });
  }

  goToStepNo(no) {
    if (no == 3) {
      if (!this.verifyStep2()) return;
    }
    if (no == 4) {
      if (!this.verifyStep3()) return;
      this.getAvailableDates();
    }
    if (no == 5) {
      if (!this.verifyStep4()) return;
      this.getAvailableTimeslot();
    }
    if (no == 6) if (!this.verifyStep5()) return;
    this.step = no;

  }

  dataScreeningCenters: any;
  verifyStep2() {
    let checkedPersons = _.filter(this.registrationPersons, { checked: true });
    if (checkedPersons.length == 0) {
      this.toastrService.warning("Please select at least one user for booking");
      return false;
    } else {

      this.dataBookingPayment.patients = _.filter(this.registrationPersons, { checked: true });
      this.dataBookingPayment.products[0].quantity = this.dataBookingPayment.patients.length;
      this.dataBookingPayment.products[0].total_price = this.dataBookingPayment.products[0].quantity * this.dataBookingPayment.products[0].price;
      this.dataBookingPayment.total_payment = 0;
      this.dataBookingPayment.products.forEach(element => {
        this.dataBookingPayment.total_payment += element.total_price;
      });
      return true;
    }
  }

  screeningDateTimeslot: any = [];
  verifyStep3() {
  
    let screening_center = this.dataBookingUpdate.screening_center_id;
    let home_visit = this.isHomeSelected;
     
    // if (this.dataBookingUpdate.screening_center_id == null || this.dataBookingUpdate.screening_center_id === "null") {
      // && (home_visit==null || home_visit==false)
    if ((screening_center == null || screening_center === "null")) {
      this.toastrService.warning("Please choose a screening center");
      return false;
    } else {
      this.dataBookingPayment.screening_center_id = this.dataBookingUpdate.screening_center_id;
      // this.dataBookingPayment.screening_location = this.dataBookingUpdate.screening_location;
      if (this.isHomeSelected == true) {
        this.dataBookingPayment.screening_location = "HOME";
        let homeCharge = _.filter(this.dataBookingPayment.products, { remark: 'HOME' });
        if (homeCharge.length == 0) {
          this.dataService.getHomeScreeningCharging().subscribe(charge => {
            charge.data.forEach(element => {
              let newCharge = new Product(0, null, "ALL", element.id, element.description, element.price, this.dataBookingPayment.patients.length, this.dataBookingPayment.patients.length * element.price, 'HOME');
              this.dataBookingPayment.products.push(newCharge);
            });
            this.dataBookingPayment.total_payment = 0;
            this.dataBookingPayment.products.forEach(element => {
              this.dataBookingPayment.total_payment += element.total_price;
            });
          });
        }
      } else {
        _.remove(this.dataBookingPayment.products, { remark: 'HOME' });
        this.dataBookingPayment.total_payment = 0;
        this.dataBookingPayment.products.forEach(element => {
          this.dataBookingPayment.total_payment += element.total_price;
        });
        this.dataBookingPayment.screening_location = null;
      }
      
      return true;
    }
  }

  getAvailableDates() {
    this.disabledDates = [];
    this.dataService.getAvailableScreeningDateRangeAndTimeslot(this.dataBookingUpdate.screening_center_id, this.datePipe.transform(this.minDate, 'yyyy-MM-dd'), this.datePipe.transform(this.maxDate, 'yyyy-MM-dd')).subscribe(res => {
      this.screeningDateTimeslot = res;
      res.forEach(element => {
        if (element.date_available_status == false) {
          this.disabledDates.push(new Date(element.screening_date));
        }
      });
    });
  }

  dataTimeslots: any = [];
  getAvailableTimeslot() {
    this.dataTimeslots = [];
    if (this.updateMode != "update") {
      let screeningDate = _.filter(this.screeningDateTimeslot, { screening_date: this.dataBookingUpdate.screening_date });
      if (screeningDate.length > 0) {
        this.dataTimeslots = screeningDate[0].timeslots;
      }
    } else {
      this.dataService.getAvailableScreeningDateAndTimeslot(this.dataBookingPayment.screening_center_id, this.dataBookingUpdate.screening_date).subscribe(res => {
        this.screeningDateTimeslot = res;
        let screeningDate = _.filter(this.screeningDateTimeslot, { screening_date: this.dataBookingUpdate.screening_date });
        if (screeningDate.length > 0) {
          this.dataTimeslots = screeningDate[0].timeslots;
        }
      });
    }
  }


  verifyStep4() {
    if (this.dataBookingUpdate.screening_date == null) {
      this.toastrService.warning("Please choose a screening date");
      return false;
    } else {
      if (this.updateMode == 'NEW') this.dataBookingPayment.screening_date = this.dataBookingUpdate.screening_date;
      return true;
    }
  }

  verifyStep5() {
    if (this.dataBookingUpdate.screening_time == null || this.dataBookingUpdate.screening_time == 'null') {
      this.toastrService.warning("Please choose a screening time");
      return false;
    } else {
      if (this.updateMode == 'NEW') this.dataBookingPayment.screening_time = this.dataBookingUpdate.screening_time;
      return true;
    }
  }

  findScreeningCenter(id) {
    if (id == null || id == "null") return;
    let center = _.filter(this.dataScreeningCenters, { sc_id: id })
    return center[0].sc_name + ", " + center[0].sc_address;
  }

  checkAllUsers(evt) {
    this.registrationPersons.forEach(person => {
      if (person.person_type_code == 'PATIENT') person.checked = evt.target.checked;
    });
  }

  paymentProcessing = false;
  dataBookingCompleted: any;
  dataPay88 = {
    'PaymentId': null,
    'RefNo': null,
    'Amount': '1.00',
    'Currency': 'MYR',
    'ProdDesc': null,
    'UserName': null,
    'UserEmail': null,
    'UserContact': null,
    'Remark': null
  };


  @ViewChild('iPay88Form', { static: false }) iPay88Form: ElementRef;
  nextNewPayment() {
    this.paymentProcessing = true;
    this.dataPay88.ProdDesc = this.mainProduct.name;
    this.dataPay88.UserContact = '+60' + this.user.person.mobile_no;
    this.dataPay88.UserName = this.user.person.name;
    this.dataPay88.UserEmail = this.user.person.email;

    this.dataService.submitBooking(this.dataBookingPayment).subscribe(response => {
      if (response.errorFound) {
        this.toastrService.error(response.message);
        this.paymentProcessing = false;
      } else {
        // this.toastrService.success(response.message);
        let resData = response.content;
        this.dataPay88.RefNo = resData.booking_no + "#1";

        this.paymentProcessing = false;
        this.iPay88Form.nativeElement.RefNo.value = resData.booking_no + "#1";
        this.iPay88Form.nativeElement.submit();
      }

    }, error => {
      this.toastrService.error("Unable to process payment");
      this.paymentProcessing = false;
    });
  }

  nextChargePayment() {
    this.paymentProcessing = true;
    this.dataPay88.ProdDesc = this.dataBookingUpdate.products[0].name;
    this.dataPay88.UserContact = '+60' + this.dataRegistration.mobile_no;
    this.dataPay88.UserName = this.dataRegistration.name;
    this.dataPay88.UserEmail = this.dataRegistration.email;

    this.dataService.submitChangeBooking(this.dataBookingUpdate).subscribe(response => {
      if (response.errorFound) {
        this.toastrService.error(response.message);
        this.paymentProcessing = false;
      } else {
        // this.toastrService.success(response.message);
        let resData = response.content;
        this.dataPay88.RefNo = resData.booking_no + "#2";

        this.paymentProcessing = false;
        this.iPay88Form.nativeElement.RefNo.value = resData.booking_no + "#2";
        this.iPay88Form.nativeElement.submit();
      }

    }, error => {
      this.toastrService.error("Unable to process payment");
      this.paymentProcessing = false;
    });
  }


  onDayChanged(e) {
    this.bsInlineValue = e;
    this.dataBookingUpdate.screening_date = this.datePipe.transform(e, 'yyyy-MM-dd');
  }


  addRegPersonNote: any;
  isSubmitted: any;
  fetchRegistrationInfo() {
    this.addRegPersonNote = null;
    this.loading = true;
    if (this.dataRegistration.screening_plan.category_code == 'CORPORATE') this.staffPatient = 'staff'; else this.staffPatient = 'patient';
    this.dataPatients = _.filter(this.dataRegistration.registration_persons, { 'person_type_code': 'PATIENT' });
    this.dataRegistration.screening_plan.package_patients.forEach(element => {
      if (this.addRegPersonNote != null)
        this.addRegPersonNote = this.addRegPersonNote + ' and ' + element.total_patient + " " + element.patient_type_code;
      else
        this.addRegPersonNote = element.total_patient + " " + element.patient_type_code;
    });
    this.isSubmitted = false;
    this.loading = false;
  }

  refreshRegistrationInfo() {
    this.dataService.getRegistrationInfo(this.user.registration.reg_no).subscribe(res => {
      this.dataRegistration = res;
      this.fetchRegistrationInfo();
    });
  }

  changeVenue() {
    this.dataBookingUpdate.screening_center_id = this.dataBookingPayment.screening_center_id;
    this.dataBookingUpdate.screening_location = this.dataBookingPayment.screening_location;
    this.step = 3;
  }

  changeDate() {
    this.step = 4;
    this.dataBookingUpdate.screening_date = this.dataBookingPayment.screening_date;
    this.getAvailableDates();
  }

  changeTime() {
    this.step = 5;
    this.dataBookingUpdate.screening_time = this.dataBookingPayment.screening_time;
    this.getAvailableTimeslot();
  }

  isVenueHomeChanged: any = false;
  isHomeSelected: any = false;
  confirmVenueChange() {

    if (!this.isHomeSelected &&
      (!this.dataBookingUpdate.screening_center_id || this.dataBookingUpdate.screening_center_id == 'null')) {
      this.toastrService.warning("Please make your venue selection before proceed. ");
      return;
    }

    this.coolDialogs.confirm("Are you sure to change new venue?").subscribe(confirm => {
      if (confirm) {
        if (this.isHomeSelected) {
          if (this.isHomeSelected) this.isVenueHomeChanged = true;
          this.dataBookingUpdate.screening_location = 'HOME';
          let homeCharge = _.filter(this.dataBookingUpdate.products, { remark: 'HOME' });
          if (homeCharge.length == 0) {
            this.dataService.getHomeScreeningCharging().subscribe(charge => {
              charge.data.forEach(element => {
                let newCharge = new Product(this.dataBookingUpdate.booking_id, this.dataBookingUpdate.booking_no, "ALL", element.id, element.description, element.price, this.dataBookingPayment.patients.length, this.dataBookingPayment.patients.length * element.price, 'HOME');
                this.dataBookingUpdate.products.push(newCharge);
              });
              this.dataBookingUpdate.total_payment = 0;
              this.dataBookingUpdate.products.forEach(element => {
                this.dataBookingUpdate.total_payment += element.total_price;
              });
            });
          }
          this.step = 7;
        } else {
          // console.log(this.dataBookingUpdate);
          this.dataService.submitChangeBooking(this.dataBookingUpdate).subscribe(response => {
            if (response.errorFound) {
              this.toastrService.error(response.message);
            } else {
              this.fetchBooking(this.dataBookingUpdate.booking_no);
              this.toastrService.success(response.message);
              this.goToStepNo(8);
            }
            this.paymentProcessing = false;
          });
        }
      }
    });
  }



  confirmDateChange() {
    if (this.verifyStep4()) {
      this.coolDialogs.confirm("Are you sure to change screening date from "
        + this.dataBookingPayment.screening_date + " to " + this.dataBookingUpdate.screening_date + "?").subscribe(confirm => {
          if (confirm) {
            let saveData = this.dataBookingPayment;
            saveData.screening_date = this.dataBookingUpdate.screening_date;
            this.dataService.updateBooking(saveData, "DATE").subscribe(response => {
              if (response.errorFound) {
                this.toastrService.error(response.message);
              } else {
                this.toastrService.success(response.message);
                this.dataBookingPayment.screening_date = this.dataBookingUpdate.screening_date;
                // this.router.navigate(["marketplace", "mp-booking-completed", response.content.booking_no]);
                this.goToStepNo(8);
              }
            }, error => {
              this.toastrService.error("Unable to update new screening date");
            });
          }
        });
    }
  }

  confirmTimeChange() {
    if (this.verifyStep5()) {
      this.coolDialogs.confirm("Are you sure to change screening time from "
        + this.dataBookingPayment.screening_time + " to " + this.dataBookingUpdate.screening_time + "?").subscribe(confirm => {
          if (confirm) {
            let saveData = this.dataBookingPayment;
            saveData.screening_time = this.dataBookingUpdate.screening_time;
            this.dataService.updateBooking(this.dataBookingPayment, "TIME").subscribe(response => {
              if (response.errorFound) {
                this.toastrService.error(response.message);
              } else {
                this.toastrService.success(response.message);
                this.dataBookingPayment.screening_time = this.dataBookingUpdate.screening_time;
                // this.router.navigate(["marketplace", "mp-booking-completed", response.content.booking_no]);
                this.goToStepNo(8);
              }
            }, error => {
              this.toastrService.error("Unable to update new screening time");
            });
          }
        });
    }
  }

  cancelBooking() {
    this.coolDialogs.confirm("Are you sure to cancel this booking?")
      .subscribe(confirm => {
        if (confirm) {
          let updateData = this.formBuilder.group(this.dataBookingPayment);
          updateData.value.status = 'PENDING-CANCELLATION';

          this.dataService.updateBooking(updateData.value, 'STATUS').subscribe(res => {
            if (res.errorFound) {
              this.toastrService.error("Failed to cancel this booking", "Process failed");
            } else {
              this.backToMyDashboard();
              this.toastrService.success("The booking has been submitted for cancellation.", "Success");
            }
          })
        }
      })
  }

  backToMarketPlaceListing() {
    this.router.navigate(["marketplace", "mp-listing"]);
  }

  backToMyDashboard() {
    this.step = 1;
    this.router.navigate(["my-dashboard", "main"]);
  }


  mode: any;
  titleCaseWord(word: string) {
    if (!word) return word;
    return word[0].toUpperCase() + word.substr(1).toLowerCase();
  }

  calculateDiff(bookingDate) {
    var date1: any = new Date(bookingDate);
    var date2: any = new Date();
    var diffDays: any = Math.floor((date1 - date2) / (1000 * 60 * 60 * 24));

    return diffDays;
  }
}
