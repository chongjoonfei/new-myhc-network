import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MarketplaceComponent } from './marketplace.component';
import { MpBookingComponent } from './booking/mp-booking.component';
import { MpListingComponent } from './listing/mp-listing.component';
import { MpBookingCompletedComponent } from './booking/mp-booking-completed.component';
// import { ReceiptComponent } from '../receipt/receipt.component';
 
 
const routes: Routes = [
 
      {
        path: '',
        component: MarketplaceComponent,pathMatch: 'full'
      },
      {
        path: 'mp-listing',
        component: MpListingComponent,pathMatch: 'full'
      },
      {
        path: 'mp-booking/:type/:pkgcode/:prodcode',
        component: MpBookingComponent,pathMatch: 'full'
      },
      {
        path: 'mp-booking/:mode/:bookno',
        component: MpBookingComponent,pathMatch: 'full'
      },
      {
        path: 'mp-booking-completed/:bookingno',
        component: MpBookingCompletedComponent,pathMatch: 'full'
      },
      // {
      //   path: 'receipt/:bookingno/:transid',
      //   component: ReceiptComponent,pathMatch: 'full'
      // },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BookingRoutingModule { }
