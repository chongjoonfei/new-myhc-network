import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS,HttpClient } from '@angular/common/http';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import {ChartModule} from 'angular2-chartjs';
import {SharedModule} from '../../shared/shared.module';

import { MarketplaceComponent } from './marketplace.component';
 
import { QRCodeSVGModule } from 'ngx-qrcode-svg';
import { TooltipModule } from 'ngx-bootstrap';
 
import { MaterialModule } from '../../shared/material-module';
import { BookingRoutingModule } from './marketplace-routing.module';
import { MpBookingComponent } from './booking/mp-booking.component';
import { MpListingComponent } from './listing/mp-listing.component';
import { MpBookingCompletedComponent } from './booking/mp-booking-completed.component';
// import { ReceiptComponent } from '../receipt/receipt.component';


@NgModule({
  imports: [
    CommonModule,
    BookingRoutingModule,
    SharedModule,
    ChartModule,
    QRCodeSVGModule,
    TooltipModule.forRoot(),
    MaterialModule,
    HttpClientModule,
    SlickCarouselModule
  ],
  declarations: [
    MpListingComponent,
    MpBookingComponent,
    MpBookingCompletedComponent,
    MarketplaceComponent,
    // ReceiptComponent
  ]
})
export class MarketplaceModule { }
