import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
 
// import { AuthGuard } from 'src/app/service/auth/auth.guard';
 
import { CorporateDashboardComponent } from './dashboard/corporate-dashboard.component';
 
 
const routes: Routes = [
 
      {
        path: '',
        component: CorporateDashboardComponent,pathMatch: 'full'
      },
      {
        path: 'dashboard',
        component: CorporateDashboardComponent,pathMatch: 'full'
      },
 
 
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CorporateRoutingModule { }
