import { TemplateRef } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApexNonAxisChartSeries, ApexPlotOptions, ApexChart } from "ng-apexcharts";
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { ToastrService } from 'ngx-toastr';
import { DataService } from '../../service/data.service'; 
import * as _ from "lodash";
import { environment } from '../../../environments/environment';
import {TranslateService} from '@ngx-translate/core';

export type ChartOptions = {
  series: ApexNonAxisChartSeries;
  chart: ApexChart;
  labels: string[];
  plotOptions: ApexPlotOptions;
};

@Component({
  selector: 'app-my-dashboard',
  templateUrl: './my-dashboard.component.html',
  styleUrls: [
    './my-dashboard.component.scss'
  ]
})
export class MyDashboardComponent implements OnInit {

   
  loading=true;
  user:any;
  dataPerson:any=[];
  dataRegistration:any;
  auth:any;
  public chartOptions: Partial<ChartOptions>;

  constructor(
    private router: Router,
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private toastrService: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    private dataService: DataService,
    public translate: TranslateService
  ) {}

  refRangeColor = [
    { code: 'Low', colorClass: 'test-element-light-blue', colorCode: '#B2EBF2' },
    { code: 'Normal', colorClass: 'test-element-green', colorCode: '#B9F6CA' },
    { code: 'Borderline', colorClass: 'test-element-yellow', colorCode: '#FFF59D' },
    { code: 'High', colorClass: 'test-element-orange', colorCode: '#FFE0B2' },
    { code: 'Very High', colorClass: 'test-element-red', colorCode: '#EF9A9A' }
  ];

  mauAccount:any;
  mauPhoto:any;
  dataBookings:any;
  dataPatients: any;
  staffPatient: any;
  photoPath:any=environment.uploadPath;
  ngOnInit() {
    this.auth = JSON.parse(localStorage.getItem("auth"));
    
    if (localStorage.getItem("plan-registration")){
      this.dataRegistration = JSON.parse(localStorage.getItem("plan-registration"));

      this.dataService.getUserDetails(this.auth.username).subscribe(res=> {
        this.user = res;
        if (this.dataRegistration.screening_plan.category_code == 'CORPORATE') 
          this.staffPatient = 'staff'; else this.staffPatient = 'patient';
        if (this.user.acc_type_code == 'STAFF') {
          this.dataPatients = _.filter(this.dataRegistration.registration_persons, { 'ic_no': this.user.ic_no });
        } else {
          this.dataPatients = _.filter(this.dataRegistration.registration_persons, { 'person_type_code': 'PATIENT' });
      }
        this.loading=false;
      }, error=>{
        this.loading=false;
      });


      this.dataPerson = this.dataRegistration.registration_persons;
      let mau = _.filter(this.dataPerson,{admin_type:'MAU'});
      if (mau.length>0) {
        this.mauAccount = mau[0];
        this.mauPhoto = environment.uploadPath + this.mauAccount.person.photo_path;
      }
      
      this.getUpcomingBookings();
      this.loadRadialBarChart();
      
    }else{
      this.router.navigate(['/mau']);
    }
      
  }

  loadRadialBarChart() {
    this.chartOptions = {
      series: [44, 55],
      chart: {
        height: 350,
        type: "radialBar"
      },
      plotOptions: {
        radialBar: {
          dataLabels: {
            name: {
              fontSize: "22px"
            },
            value: {
              fontSize: "16px"
            },
            total: {
              show: true,
              label: "Total",
              formatter: function(w) {
                return "249";
              }
            }
          }
        }
      },
      labels: ["Apples", "Oranges"]
    };
  }

  loadingBooking=true;
  getUpcomingBookings(){
    this.loadingBooking=true;
    this.dataService.getUpcomingBookingByRegNo(this.dataRegistration.reg_no).subscribe(res=> {
      console.log("dataBookings",res)
      this.dataBookings = res;
      this.loadingBooking=false;
    }, error=>{
      this.loadingBooking=false;
    });
  }

  setDefaultPic(event) {
    event.target.src = "assets/images/avatar.jpg";
  }
 
  dataPatient:any;
  tabClick(evt){
    this.dataPatient = this.dataPatients[evt.index];
    this.getLatestResult();

  }

  dataPatientResults: any = [];
  loadingResult=true;
  getLatestResult() {

    let markerList=['bmi','hbA1c','ldl-chol','hdl-chol','t-chol-hdl-ratio','triglycerides','albumin','creatinine','gfr'];

    this.loadingResult = true;
    this.dataPatientResults = [];

    let testPanels = [];
    this.dataService.searchDashboardResultRef(this.dataPatient.ic_no).subscribe(res => {

      this.dataPatientResults = [];

      Object.entries(res).forEach(([key, value]) => {
        Object.entries(value).forEach(([keyPanel, valuePanel]) => {
          if (keyPanel.indexOf('test_panel') != -1) {
            testPanels.push(valuePanel);
            Object.entries(valuePanel).forEach(([keyMarker, valueMarker]) => {
              // if (keyMarker=='test_marker_range') test_marker_range=valueMarker;

              if (keyMarker.indexOf('test_marker_code') != -1) {
                let latest_res = null;
                let totalElement = valueMarker['test_dates'].length;
                if (totalElement > 1) {
                  latest_res = valueMarker['test_dates'][totalElement - 1]; //take last element - latest record
                }else{
                  latest_res = valueMarker['test_dates'][0];
                }

                if (!isNaN(latest_res.test_value)){
                   if (markerList.includes(valueMarker['test_marker_code'])) {
                    let decimal_point = valueMarker['decimal_point'];
                    let avg_marker_value = _.meanBy(valueMarker['test_dates'], (m) => parseFloat(m.test_value));
                    let avg_value = 'N/A';
                    let latest_value = latest_res.test_value;
                    if (!isNaN(avg_marker_value)) {
                      avg_value = avg_marker_value.toFixed(decimal_point);
                      latest_value = parseFloat(latest_res.test_value).toFixed(decimal_point);
                    }
  
                    let rec_marker = { test_marker_code: valueMarker['test_marker_code'], test_marker_name: valueMarker['test_marker_name'], decimal_point:valueMarker['decimal_point'],
                      test_unit: latest_res.test_unit, latest_value:latest_value, latest_ref:latest_res.ref_value, avg_value:avg_value, avg_ref:this.getRefValue(valuePanel.test_marker_range,valueMarker['test_marker_code'],avg_value) };

                    this.dataPatientResults.push(rec_marker);
                  }
                }

              }
            });
          }
        });

      });
      this.loadingResult = false;
    }, error => {
      this.loadingResult = false;
    });
  }

  getRefValue(refRanges, code,value){
    let selectedRange = null;
    refRanges.forEach(element => {
      if (element.code==code){
        element.test_reference_range.forEach(range => {
          if (value >= range.min && value <= range.max){
            selectedRange = range.code;
          }
        });
      }
    });
    return selectedRange;
  }

  viewBookingDetails(bNo){
    this.router.navigate(['/marketplace','mp-booking','update',bNo]);
  }

  goToMyTestResult(){
    this.router.navigate(["/test-panel","result"]);
  }

  keyword:any;
  keysearch="date e.g. YYYY-MM-DD";
  loadingPreviousBooking=false;
  dataPreviousBooking:any;
  searchBooking(){
    this.loadingPreviousBooking=true;
    let key = this.keysearch;
    if (this.keysearch.indexOf("date")!=-1) key = "date";  
    this.dataService.searchBookingByRegNo(this.dataRegistration.reg_no,this.keyword,key).subscribe(res=>{
      this.dataPreviousBooking = res;
      this.loadingPreviousBooking=false;
    },error=>{
      this.loadingPreviousBooking=false;
    })
  }

  receiptBookingNo: any;
  receiptTransNo: any;
  loadingReceipt=false;
  printReceipt(payment) {
    const arrRefNo = payment.ref_no.split("#");
    this.receiptBookingNo = payment.booking_no;
    this.receiptTransNo = payment.trans_no;
    this.loadingReceipt = true;
    this.dataService.printReceiptByRefNo(this.receiptBookingNo,arrRefNo[1]).subscribe(res => {
      var file = new Blob([res], { type: 'application/pdf' });
      var fileURL = URL.createObjectURL(file);
      window.open(fileURL);
      this.loadingReceipt = false;
    }, error => {
      console.log(error);
      this.loadingReceipt = false;
    })
  }
 
  
}
