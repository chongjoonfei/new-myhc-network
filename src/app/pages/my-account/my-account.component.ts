import { TemplateRef } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { DataService } from '../../service/data.service';
import * as _ from "lodash";
import {TranslateService} from '@ngx-translate/core';
import { AuthenticationService } from '../../service/auth/Authentication.service';



declare const AmCharts: any;
declare const $: any;

@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: [
    './my-account.component.scss'
  ]
})
export class MyAccountComponent implements OnInit {

  
  constructor(
    private router: Router,
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private coolDialogs: NgxCoolDialogsService,
    private dataService: DataService,
    public translate: TranslateService,
    private toastrService: ToastrService,
    private authenticationService : AuthenticationService,
  ) {}

  loading=true;
  user:any;
  dataPatients:any=[];
  selectedPatientData: any = [];
  dataRegistration:any;
  auth:any;
  qrActive: any;

  slideConfig = {
    "slidesToShow": 4,
    "slidesToScroll": 1,
    "nextArrow": "<div class='nav-btn next-slide'></div>",
    "prevArrow": "<div class='nav-btn prev-slide'></div>",
    "dots": true,
    "infinite": false
  };

  ngOnInit() {
    this.auth = JSON.parse(localStorage.getItem("auth"));
    this.dataRegistration = JSON.parse(localStorage.getItem("plan-registration"));

    this.dataService.getUserDetails(this.auth.username).subscribe(user=> {
      this.user = user;
    });
    
    if (this.dataRegistration.screening_plan.category_code == 'CORPORATE' 
        || this.dataRegistration.screening_plan.category_code == 'INDIVIDUAL') {
      this.dataPatients = _.filter(this.dataRegistration.registration_persons, { 'ic_no': this.auth.account.ic_no });
    } else {
      this.dataPatients = this.dataRegistration.registration_persons;
    }

    this.qrActive = 0;
    this.selectedPatientData = this.dataPatients[0];
  }


  changeAdminUser() {}

  cpass:any;
  npass:any;
  rpass:any;
  changePassword(){
      if (this.npass!=this.rpass){
          this.toastrService.warning("Sorry, your new password and re-type password does not matched", "Password does not matched");
      }else{
          let data = { "username": this.user.username, "npass": this.npass };
          this.dataService.changePassword(data).subscribe(res => {
              this.toastrService.success("Your password has been updated", "Success");
              this.npass=null;
              this.rpass=null;
              this.authenticationService.logout()
            }, msg1 => {
              this.toastrService.error(msg1.error.message, "Failed");
            });
      }
  }

}
