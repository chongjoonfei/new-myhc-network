import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { ToastrService } from 'ngx-toastr';
import { DataService } from '../../../../service/data.service';
import * as _ from "lodash";
import { MatStepper } from '@angular/material';
import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { environment } from '../../../../../../src/environments/environment';
import { DatePipe } from '@angular/common';
import {TranslateService} from '@ngx-translate/core';
@Component({
  selector: 'app-basic-reg',
  templateUrl: './basic-reg.component.html',
  styleUrls: ['./basic-reg.component.scss']
})
export class BasicRegComponent implements OnInit {
  isLinear = false;
  formDetails: FormGroup;
  formNCD: FormGroup;

  constructor(
    private router: Router,
    private dataService: DataService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private toastrService: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    private datePipe: DatePipe,
    public translate: TranslateService
  ) {}

  screeningPlans: any = [];
  selectedPlan: any;
  isVerifiedDependent = false;
  sevenDays: any = [{ no: 0, text: "Don't know" },
  { no: 1, text: "1 Day" }, { no: 2, text: "2 Days" }, { no: 3, text: "3 Days" }, { no: 4, text: "4 Days" },
  { no: 5, text: "5 Days" }, { no: 6, text: "6 Days" }, { no: 7, text: "7 Days" }]

  packageCategories: any;
  selectedPackageCategory: any;
  localhost: any;
  groupPlanPackages: any;
  dataScreening: any;

  ngOnInit() {
    this.localhost = window.location.protocol + "//" + window.location.host;
    document.querySelector('body').setAttribute('themebg-pattern', 'theme1');


    this.initFormDetails();
    // this.dataService.getAllScreeningPlan().subscribe(data => {
    //   let orderedPlans = _.orderBy(data, ['category_code'], ['asc']);
    //   let plans = ['SP01','SP03','SP05','SP07'];
    //   plans.forEach(element => {
    //     console.log(element)
    //     let data = _.filter(orderedPlans, {package_code:element})
    //     if (data.length>0) this.screeningPlans.push(data[0]);
    //   });
    //   console.log(this.screeningPlans);
    // });

    // this.dataService.getAllPackageCategoryShowStatus().subscribe(result => {
    //   this.packageCategories = result.data;
    // });

    this.dataService.getScreenPlanByCommercial('YES').subscribe(result => {
      // this.planPackages = result.data;
      let groupPlanPackages = [];
      let plans = ['FREE1-ID','FREE2-DC','FREE3-DE','FREE4-FM'];
      let catPlans = {'P1INDIVIDUAL':[], 'P2DEPENDENT':[], 'P3FAMILY':[]};
      plans.forEach(plan => {
        let data = _.filter(result.data, {package_code:plan})
        if (data.length>0) {
          groupPlanPackages.push(data[0]);
          if (data[0].category_code=='INDIVIDUAL')
            catPlans.P1INDIVIDUAL.push(data[0]);
          else if (data[0].category_code=='FAMILY')
            catPlans.P3FAMILY.push(data[0]);
          else if (data[0].category_code=='DEPENDENT')
            catPlans.P2DEPENDENT.push(data[0]);
        }
      });
      this.groupPlanPackages = catPlans;
      // this.groupPlanPackages = _.groupBy(groupPlanPackages, 'category_code');
       
    });

    this.dataService.getAllScreeningCenters().subscribe(res => {
      this.dataScreening = res.data
    })

    this.initFormNCD();

  }

  selectedIndex: number = 0;

  setIndex(event) {
    this.selectedIndex = event.selectedIndex;
  }

  triggerClick(event) {
    console.log(`Selected tab index: ${this.selectedIndex}`);
  }

  initFormDetails() {
    this.formDetails = this.formBuilder.group({
      name: [null, Validators.required],
      // password: null,
      ic_no: [null, Validators.required],
      email: [null, Validators.required],
      mobile_no: [null, Validators.required],
      date_of_birth: [null, Validators.required],
      screening_location: [null, Validators.required],
      package_code: null,
      amount_paid: 0.00,
      payment_no: null,
      payment_method: null,
      payment_date: null,
      date_registered: null,
      date_expired: null,
      status: 'PRE-ACCOUNT',
      center_code: null,
      chk_tnc: [null]
    });
  }

  optPackageCategory: any = null;
  startOver() {
    this.stepper.reset();
    this.selectedPackageCategory = null;
    this.selectedPlan = null;
    this.sixDigitCode = null;
    this.optPackageCategory = null;
    this.initFormDetails();
  }

  previous() {
    this.stepper.selected.completed = true;
    this.stepper.previous();
  }

  // planPackages: any;
  onPackageCategoryOptChange(pCategory) {
    this.selectedPackageCategory = pCategory;
    // this.dataService.getScreenPlanByCommercialCategory(pCategory,'YES').subscribe(res=>{
    //   this.planPackages = res.data;
    // });

    // this.planPackages = _.filter(this.screeningPlans, { category_code: pCategory });
  }

  onPackagePlanOptChange(plan) {
    this.selectedPlan = plan;
  }


  verifyTypeRegistration() {
    if (this.selectedPlan) {
      this.stepper.selected.completed = true;
      this.stepper.next();
    } else {
      this.stepper.selected.completed = false;
      this.toastrService.warning("Please select type of account before proceed to the next step", "Warning");
    }
  }

  dependentIcNo: any;
  errorDependent: any;
  // verifyDependentIcNo(){
  //   this.errorDependent = null;
  //   this.isVerifiedDependent=false;
  //   this.dataService.verifyIcNoDependent(this.dependentIcNo).subscribe(res=>{
  //     // console.log(res);
  //     this.isVerifiedDependent = res.errorFound;
  //     if (res.errorFound==true){
  //       this.errorDependent = "invalid";
  //       this.stepper.selected.completed = false;
  //       this.isVerifiedDependent = false;
  //       this.toastrService.warning(res.message, "Warning");
  //     }else{
  //       this.errorDependent=null;
  //       this.isVerifiedDependent=true;
  //       this.toastrService.success(res.message, "Success");
  //     }

  //   });
  // }

  verifyDetails() {
    if (!this.formDetails.invalid) {
      this.stepper.selected.completed = true;
      this.stepper.next();
    } else {
      this.stepper.selected.completed = false;
      this.toastrService.warning("Please complete your details before proceed to the next step", "Warning");
    }
  }


  initFormNCD() {
    // console.log(this.formDetails.value);
    this.formNCD = this.formBuilder.group({
      ic_no: [this.formDetails.get("ic_no").value],
      tabacco_use: [null, [Validators.required]],
      alcohol_consumption: [null, [Validators.required]],
      diet_eat_fruit: [null, [Validators.required]],
      diet_eat_vege: [null, [Validators.required]],
      physical_activities: [null, [Validators.required]],
      history_hypertension: [null, [Validators.required]],
      history_diabetes: [null, [Validators.required]]
    });
  }

  verifyFormNCD() {
    if (!this.formNCD.invalid) {
      this.stepper.selected.completed = true;
      this.stepper.next();
    } else {
      this.stepper.selected.completed = false;
      this.toastrService.warning("Please complete the questionnaire before proceed to the next step", "Warning");
    }
  }

  isSendingSMS:any=false;
  checkTermCondition: any;
  sentSmsSixDigitCode() {

    if (this.checkTermCondition) {
      this.isSendingSMS = true;
      let mobile_no = "60" + this.formDetails.get("mobile_no").value;
      this.toastrService.success("Success");
      this.stepper.selected.completed = true;
      this.stepper.next();
     
      this.dataService.get6DigitCode(mobile_no).subscribe(sixDigit => {
        let input = { "code": "six-digit-code", "mobile": mobile_no, "paramValues": [{ "param": "?code", "value": sixDigit.code }] };
        this.dataService.sendSms(input).subscribe(msg => {
          if (!msg.errorFound) {
            this.toastrService.success(msg.message, "Success");
            this.stepper.selected.completed = true;
            this.stepper.next();
          } else {
            this.toastrService.error(msg.message, "Process failed");
          }
          this.isSendingSMS=false;
        }, err => {
          this.isSendingSMS=false;
          this.toastrService.error("Your mobile no +" + mobile_no + " is invalid", err.error.error);
        });
      }, error => {
        this.isSendingSMS=false;
        this.toastrService.error("Unable to process your request", "Process failed");
      });
    } else {
      this.isSendingSMS=false;
      this.stepper.selected.completed = false;
      this.toastrService.warning("Please confirm your agreement to our Terms & Conditions and Privacy Policy before proceed to the next step", "Warning");
    }

  }

  isRegistering: any = false;
  sixDigitCode: any;
  codeConfirmation = false;
  @ViewChild('stepper', { static: false }) private stepper: MatStepper;

  verifyCode()
  {
    let data = this.formDetails.value;
    console.log(data);
    /*this.dataService.preRegister(data).subscribe(result => {
      this.isRegistering = false;
      this.stepper.selected.completed = true;
      this.codeConfirmation = true;
      this.stepper.next();
    }, err => {
      this.isRegistering = false;
      this.toastrService.error(err.error.message, "Registration process failed");
    });*/
  }



  verifyCode_old() {
    this.isRegistering = true;

    let mobile_no = "60" + this.formDetails.get("mobile_no").value;
    let input = { "code": this.sixDigitCode, "mobile": mobile_no };
    this.dataService.verify6DigitCode(input).subscribe(verification => {
      this.toastrService.success(verification.message, "Success");
      let data = this.formDetails.value;
      data.package_code = this.selectedPlan;
      data.amount_paid = "0.00";
      data.date_registered = this.datePipe.transform(new Date, 'yyyy-MM-dd hh:mm a');
      data.chk_tnc = this.checkTermCondition;
      this.formNCD.get('ic_no').setValue(data.ic_no);
      data.ncd = this.formNCD.value;

      this.dataService.preRegister(data).subscribe(result => {
        this.isRegistering = false;
        this.stepper.selected.completed = true;
        this.codeConfirmation = true;
        this.stepper.next();
      }, err => {
        this.isRegistering = false;
        this.toastrService.error(err.error.message, "Registration process failed");
      });
    
    }, verification => {
      if (verification.error.errorFound) {
        if (verification.error.message.indexOf("expired") != -1) {
          this.toastrService.error(verification.error.message, verification.error.error);
        } else {
          this.toastrService.error("Please enter a valid 6 digit code that you received from your registered mobile phone", "Invalid code");
        }
      }
      this.stepper.selected.completed = false;
      this.codeConfirmation = false;
      this.isRegistering = false;
    });

  }


  goToLoginPage() {
    this.router.navigate(['/', 'login']);
  }

  


}
