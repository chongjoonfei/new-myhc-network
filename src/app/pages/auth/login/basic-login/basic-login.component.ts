import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as _ from "lodash";
import { DataService } from '../../../../service/data.service';
import sha256 from 'crypto-js/sha256';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../../../../service/auth/Authentication.service';

import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-basic-login',
  templateUrl: './basic-login.component.html',
  styleUrls: ['./basic-login.component.scss']
})
export class BasicLoginComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private dataService: DataService,
    private modalService: BsModalService,
    private authenticationService : AuthenticationService,
    public translate: TranslateService
     ) {}

  imgSrc = 'assets/images/buttonclick_off.jpg';

  footer:any;
  ngOnInit() {
    //console.log("basic-login ngOnInit");
    // localStorage.clear();
    // localStorage.clear();
    document.querySelector('body').setAttribute('themebg-pattern', 'theme1');
    this.formLogin = this.formBuilder.group({ "email": [null, Validators.required], "password": [null, Validators.required] });
    this.dataService.getContent("footer-login").subscribe(
      res=>{
        this.footer = res.content;
      }
    );
  }


  modalRef: BsModalRef;
  error: any;
  username: any;
  formLogin: FormGroup;
  login() {
    if (!this.formLogin.invalid) {
      let data = this.formLogin.value;
      this.authenticationService.login(data)
      .pipe(first())
      .subscribe(
          res => {
            let auth = JSON.parse(localStorage.getItem("auth"));

           
            let dataAut = {
              intProject :1,
              strModuleNamePathTblName :"Login >> /login >> user_account",
              intRecordId :0,
              intAction :1, // 1=Login,2=Logout,3=Create,4=Read,5=Update,6=Delete
              strAddedByUsername :auth.account.username,
              strAddedByEmail :auth.account.email     
            };
            this.dataService.createAuditrail(dataAut).subscribe(resp=>{
              console.log(resp);
            },error =>{console.log(error);});

            if (auth.account.acc_type_code=='ADMIN'){
              this.router.navigate(['/administrator']);
            }else{
              this.router.navigate(['/mau']);
            }
          },
          error => {
              this.error = error.error.message;
          });
  
    } else {
      this.error = "Sorry, invalid email or password.";
    }
  }

  resetErrMsg(){
    this.error = null;
  }


  forgotPassword(template: TemplateRef<any>){
    console.log('sdfs');
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-md' });
  }

  reset() {};
}
