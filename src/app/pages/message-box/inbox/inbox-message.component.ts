import { TemplateRef } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { ToastrService } from 'ngx-toastr';
import { DataService } from '../../../service/data.service';
import * as _ from "lodash";
import { environment } from '../../../../environments/environment';

import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-inbox-message',
  templateUrl: './inbox-message.component.html',
  styleUrls: [
    './inbox-message.component.scss'
  ]
})
export class ListMessageComponent implements OnInit {


  loading = true;
  user: any;
  modalRef: BsModalRef;
  dataInboxMessages: any;
  auth: any;
  constructor(
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private toastrService: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    private dataService: DataService,
    public translate: TranslateService
  ) {}

  dataRegistration: any;
  sentToList: any = [];
  isAddMessage = false;
  dataPatients: any;
  ngOnInit() {
    this.auth = JSON.parse(localStorage.getItem("auth"));
    //if category=CORPORATE, acc=MAU, list receiver = ADMIN, staffs
    //if category=INDIVIDUAL , INDIVIDUAL & DEPENDENT, acc=MAU, list receiver = ADMIN
    this.dataService.getUserDetails(this.auth.username).subscribe(user => {
      this.user = user;
      if (user.acc_type_code != 'ADMIN') {
        this.dataRegistration = JSON.parse(localStorage.getItem("plan-registration"));
          if (this.dataRegistration.screening_plan.category_code=='CORPORATE'){
            if (user.acc_type_code == 'MAU') {
              let userlist = _.filter(this.dataRegistration.registration_persons, { 'person_type_code': 'PATIENT' });
              userlist.forEach(element => {
                if (element.user_account!=null){
                  this.sentToList.push({ username:element.user_account.username, name: element.person.name , acc:element.user_account.acc_type_code})
                }
              });
              this.sentToList.push({ username:'admin', name:'Administrator',acc:'ADMIN'});
            } else{
              let userlist = _.filter(this.dataRegistration.registration_persons, { 'admin_type': 'MAU' });
              userlist.forEach(element => {
                if (element.user_account!=null){
                  this.sentToList.push({ username:element.user_account.username, name: element.person.name , acc:element.user_account.acc_type_code})
                }
              });
            }
          }else {
            this.sentToList.push({ username:'admin', name:'Administrator',acc:'ADMIN'});
          }
      } else{
        // this.dataService.getAllMauPerson().subscribe(res => {
        //   res.data.forEach(user => {
        //     this.sentToList.push({ username:user.username, name:user.name,acc:user.acc_type_code});
        //   });
        // });
      }
      this.fetchMessages();
    },error=>{
      this.loading=false;
    });
  }

  results: string[];
  receiver:any;
  search(event) {
    if (this.user.acc_type_code=='ADMIN'){
      this.dataService.getSearchUser(event.query).subscribe(res => {
          this.results = res.data;
      });
    }

  }

  fetchMessages() {
    this.dataService.getAllInboxMessages(this.user.username).subscribe(res => {
      this.dataInboxMessages = res;
      this.loading = false;
    }, error => {
      this.loading = false;
    });
  }

  isSubmitted=false;
  selectedMessage: any;
  isReplyMode = false;
  messageForm: FormGroup;
  newMessage(template: TemplateRef<any>) {
    this.isSubmitted=false;
    this.isReplyMode = false;
    this.messageForm = this.formBuilder.group({
      attachment: null,
      content: [null,Validators.required],
      date_sent: null,
      headers: '',
      message_id: 0,
      message_root_id: 0,
      message_type_code: 'INBOX',
      receiver: [null,Validators.required],
      receiver_name: null,
      sender: this.user.username,
      sender_name: this.user.person.name,
      status: "NEW",
      subject: [null,Validators.required],
    });
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-l' });
  }

  openMessage(template: TemplateRef<any>, data) {
    this.isReplyMode = false;
    this.selectedMessage = data;
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-l' });
  }

  contentMessage: any;
  attachment:any;
  reply() {
    this.attachment = null;
    this.contentMessage = null;
    this.isReplyMode = true;
    this.files = [];
    // this.messageForm = this.formBuilder.group({
    //   attachment: null,
    //   content: [null,Validators.required],
    //   date_sent: null,
    //   headers: '',
    //   message_id: 0,
    //   message_root_id: this.selectedMessage.message_id,
    //   message_type_code: 'INBOX',
    //   receiver: [null,Validators.required],
    //   receiver_name: null,
    //   sender: this.user.username,
    //   sender_name: this.user.person.name,
    //   status: "RE:",
    //   subject: [null,Validators.required],
    // });
  }

  cancelReply() {
    this.attachment = null;
    this.contentMessage = null;
    this.isReplyMode = false;
  }

  sendMessage() {
    this.isSubmitted=true;
    // console.log(this.messageForm.value);
    // this.messageForm.get('receiver')

    // if (this.isReplyMode) {
    //   if (this.selectedMessage.sender != this.user.username) {
    //     this.messageForm.get('receiver').setValue(this.selectedMessage.sender);
    //   } else {
    //     this.messageForm.get('receiver').setValue(this.selectedMessage.receiver);
    //   }
    // }else{
    //   if (this.receiver) this.messageForm.get('receiver').setValue(this.receiver.username);
    // }

    // if (this.messageForm.invalid){
    //   this.toastrService.warning("Please complete the form before submitting");
    //   return;
    // }

    this.coolDialogs.confirm("Are you sure to send this message")
      .subscribe(confirm => {
        if (confirm) {
          let message = null;
          if (this.isReplyMode) {
            message = this.selectedMessage;
            if (this.selectedMessage.sender != this.user.username) {
              message.receiver = this.selectedMessage.sender;
            } else {
              message.receiver = this.selectedMessage.receiver;
            }
            message.sender = this.user.username;
            message.sender_name = this.user.person.name;
            message.content = this.contentMessage;
            if (this.selectedMessage.message_root_id == 0)
              message.message_root_id = this.selectedMessage.message_id;
            else
              message.message_root_id = this.selectedMessage.message_root_id;
            message.headers = 'RE:';
          } else {
            // console.log("receiver",this.receiver,this.receiver.username);
            if (this.messageForm.invalid){
              this.toastrService.warning("Please complete the form before submitting");
              return;
            }
            message = this.messageForm.value;
            if (this.receiver) message.receiver = this.receiver.username;
          }
          
          var formData = new FormData();

          formData.append('content', message.content);
          formData.append('date_sent', message.date_sent);
          formData.append('headers', message.headers);
          formData.append('message_id', message.message_id);
          formData.append('message_root_id', message.message_root_id);
          formData.append('message_type_code', message.message_type_code);
          formData.append('receiver', message.receiver);
          formData.append('receiver_name', message.receiver_name);
          formData.append('sender', message.sender);
          formData.append('sender_name', message.sender_name);
          formData.append('status', message.status);
          formData.append('subject', message.subject);

          Array.from(this.files).forEach(f => formData.append('attachment', f));

          this.dataService.sendInboxMessageWithAttachment(formData).subscribe(res => {

            if (res.errorFound) {
              this.toastrService.error("Unable to send your message", "Failed");
            } else {
              this.toastrService.success("Your message has been sent", "Success");
              this.fetchMessages();
              this.modalRef.hide();
            }
          }, error=>{
            this.toastrService.error(error.error.message, "Failed");
          });
        }
      });
  }

  files:string  []  =  [];
  onFileChange(event)  {
    for  (var i =  0; i <  event.target.files.length; i++)  {  
        this.files.push(event.target.files[i]);
    }
  }

  docUrl: any;
  printDoc(url) {
    this.docUrl = environment.uploadPath + 'message/' + url;
    var proxyIframe = document.createElement('iframe');
    var body = document.getElementsByTagName('body')[0];
    body.appendChild(proxyIframe);
    proxyIframe.style.width = '100%';
    proxyIframe.style.height = '100%';
    proxyIframe.style.display = 'none';

    var contentWindow = proxyIframe.contentWindow;
    contentWindow.document.open();
    contentWindow.document.write('<iframe src="' + this.docUrl + '" onload="print();" width="1000" height="1800" frameborder="0" marginheight="0" marginwidth="0">');
    contentWindow.document.close();
  }

}
