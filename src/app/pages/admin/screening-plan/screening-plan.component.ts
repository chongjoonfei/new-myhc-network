import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { DataService } from '../../../service/data.service';
import { ToastrService } from 'ngx-toastr';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import * as _ from "lodash";
import { environment } from '../../../../environments/environment';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-screening-plan',
  templateUrl: './screening-plan.component.html',
  styleUrls: ['./screening-plan.component.scss']
})
export class ScreeningPlanComponent implements OnInit {

  constructor(
    private dataService: DataService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private toastrService: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    public translate: TranslateService
  ) {}


  mode: any;
  modalRef: BsModalRef;
  isLoading = true;
  isSubmitted = false;

  categoryCodeList = [
    { category_code: 'FAMILY' },
    { category_code: 'CORPORATE' },
    { category_code: 'INDIVIDUAL' },
    { category_code: 'DEPENDENT' }
  ]

  photoPath :any;
  ngOnInit() {
    this.photoPath = environment.uploadPath + "package/";
    this.fetchData();
    this.fetchPatientType();
    this.fetchAddOn();
    this.fetchTestPanel();
    this.fetchLocation();
    this.fetchTestGroup();
  }

  fetchData() {
    this.dataService.getAllScreeningPlan().subscribe(result => {
      if (result.data) this.dataSPlan = result.data;
    }, error => {
      this.isLoading = false;
    });


  }
  selectedPackagePlan: any;
  // SCREENING PLAN
  dataSPlan: any;
  dataFormSPlan: FormGroup;

  newScreeningPlan(template: TemplateRef<any>) {
    this.isSubmitted = false;
    this.mode = "NEW";
    this.dataFormSPlan = this.formBuilder.group({
      "package_code": [null, Validators.required],
      "single_package": [null, Validators.required],
      "category_code": [null, Validators.required],
      "description": [null, Validators.required],
      "picture_path": [null],
      "price": [null, Validators.required],
      "license_validity_year": [null],
      "test_included": [null, Validators.required],
      "note": [null],
      "commercial": ["YES", Validators.required]
    });
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-lg' });
  }

  saveScreenPlan() {
    // console.log(this.dataFormSPlan.value);
    this.isSubmitted = true;
    if (this.dataFormSPlan.invalid) {
     
      this.toastrService.warning("Please complete the form before submitting", "Warning");
      return;
    }

    // console.log(this.mode);
    this.coolDialogs.confirm("Are you sure to save this info?")
      .subscribe(res => {
        if (res) {
          if (this.mode == "NEW") {
            this.dataService.createScreenPlan(this.dataFormSPlan.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message, "Warning"); else {
                this.toastrService.success(data.message, "Success");
                
                this.fetchData();
                // this.modalRef.hide();
                this.mode = "EDIT";
              }
            }, error => {
              if (error.status == 409)
                this.toastrService.error("Unable to create new screening plan - data already exist", "Warning");
              else
                this.toastrService.error("Unable to create new screening plan - service unavailabe", "Warning");
            });
          } else {
            this.dataService.updateScreenPlan(this.dataFormSPlan.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message, "Warning"); else {
                this.toastrService.success(data.message, "Success");
                this.isSubmitted = false;
                this.fetchData();
                // this.modalRef.hide();
              }
            }, error => {
              this.toastrService.error("Unable to update screening plan info", "Warning");
            });
          }
        }
      });
  }

  editScreenPlan(template: TemplateRef<any>, data) {
    this.isSubmitted = false;
    this.selectedPackagePlan = data;
    this.mode = "EDIT";
    this.dataFormSPlan = this.formBuilder.group({
      "package_code": [data.package_code, Validators.required],
      "single_package": [data.single_package, Validators.required],
      "category_code": [data.category_code, Validators.required],
      "description": [data.description, Validators.required],
      "picture_path": [data.picture_path, Validators.required],
      "price": [data.price, Validators.required],
      "license_validity_year": [data.license_validity_year, Validators.required],
      "test_included": [data.test_included, Validators.required],
      "note": [data.note, Validators.required],
      "commercial": [data.commercial, Validators.required]
    });
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-lg' });
  }

  deleteScreenPlan(code) {
    this.coolDialogs.confirm("Are you sure to delete selected record?")
      .subscribe(res => {
        if (res) {
          this.dataService.deleteScreenPlan(code).subscribe(data => {
            if (data.errorFound) this.toastrService.error(data.message, "Warning"); else {
              this.toastrService.success(data.message, "Success");
              this.fetchData();
            }
          });
        }
      }, error => {
        this.toastrService.error("Unable to delete screening plan record", "Warning");
      });
  }

  uploading = false;
  photoFile: any[] = [];
  tmpPhotoFile: any = [];
  uploadPhotoNow(event, pPhoto) {
    // console.log("uploadPhotoNow", event, pPhoto);
    this.photoFile = [];
    for (let file of event.files) {
      this.photoFile.push(file);
    }
    let packagePicture = null;
    // console.log("upload", this.uploadedFiles);
    this.coolDialogs.confirm("Are you sure to upload " + this.photoFile[0].name + "?")
      .subscribe(res => {
        if (res) {
          this.uploading = true;
          var formData = new FormData();
          formData.append('package_code', this.selectedPackagePlan.package_code);
          for (var i = 0; i < this.photoFile.length; i++) {
            formData.append("fileToUpload[]", this.photoFile[i]);
            packagePicture = this.photoFile[i];
          }
          this.dataService.uploadPackagePicture(formData).subscribe(res => {
            if (res.errorFound) {
              this.tmpPhotoFile = [];
              this.photoFile = [];
              this.toastrService.error(res.message, "Failed");
            } else {
              this.tmpPhotoFile = [];
              this.photoFile = [];
              this.dataFormSPlan.get('picture_path').setValue(this.selectedPackagePlan.package_code+"_"+packagePicture.name);
              this.selectedPackagePlan.picture_path = this.selectedPackagePlan.package_code+"_"+packagePicture.name;
              // console.log(packagePicture);
              this.toastrService.success("Photo has been successfully uploaded", "Success");
              // this.fetchPerson();
            }
            this.uploading = false;
          }, error => {
            this.toastrService.error(error.error.message, "Failed");
            this.uploading = false;
          });
        }
      });
  }


  // PACKAGE PATIENT
  dataFormPP: FormGroup;
  dataPP: any;
  modePatient: any;

  editPackagePatient(patient) {
    console.log(patient)
    this.modePatient = "EDIT";
    this.dataFormPP = this.formBuilder.group(patient);
  }

  newPackagePatient(package_code) {
    this.modePatient = "NEW";
    this.dataFormPP = this.formBuilder.group({
      "package_code": [package_code, Validators.required],
      "patient_type_code": [null, Validators.required],
      "total_patient": [null, Validators.required],
      "doc_required": [null]
    });
  }

  deletePackagePatient(code) {
    this.coolDialogs.confirm("Are you sure to delete selected record?")
      .subscribe(res => {
        if (res) {
          this.dataService.deletePackagePatient(code).subscribe(data => {
            if (data.errorFound) this.toastrService.error(data.message, "Warning"); else {
              this.toastrService.success(data.message, "Success");
              this.dataService.getOneScreenPlan(this.selectedPackagePlan.package_code).subscribe(data => {
                this.selectedPackagePlan = data;
              });
              this.modePatient = null;
            }
          });
        }
      }, error => {
        this.toastrService.error("Unable to delete selected record", "Warning");
      });
  }

  savePackagePatient() {
    this.isSubmitted = true;
    if (this.dataFormPP.invalid) {
      this.toastrService.warning("Please complete the form before submitting", "Warning");
      return;
    }

    // console.log(this.mode);
    this.coolDialogs.confirm("Are you sure to save this info?")
      .subscribe(res => {
        if (res) {
          if (this.modePatient == "NEW") {
            this.dataService.createSPPackagePatient(this.dataFormPP.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message, "Warning"); else {
                this.toastrService.success(data.message, "Success");
                this.isSubmitted = false;
                // this.fetchData();
                this.dataService.getOneScreenPlan(this.selectedPackagePlan.package_code).subscribe(data => {
                  this.selectedPackagePlan = data;
                });
                this.modePatient = null;
              }
            }, error => {
              if (error.status == 409)
                this.toastrService.error("Unable to create new patient package - data already exist", "Warning");
              else
                this.toastrService.error("Unable to create new patient package - service unavailabe", "Warning");
            });
          } else {
            this.dataService.updateSPPackagePatient(this.dataFormPP.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message, "Warning"); else {
                this.toastrService.success(data.message, "Success");
                this.isSubmitted = false;
                this.dataService.getOneScreenPlan(this.selectedPackagePlan.package_code).subscribe(data => {
                  this.selectedPackagePlan = data;
                });
                this.modePatient = null;
              }
            }, error => {
              this.toastrService.error("Unable to update patient package info", "Warning");
            });
          }
        }
      });
  }

  // PACKAGE TEST PANEL

  dataFormPTP: FormGroup;
  dataPTP: any;

  newPackageTestPanel(template: TemplateRef<any>, package_code) {

    this.mode = "NEW";
    this.dataFormPTP = this.formBuilder.group({
      "package_code": [package_code, Validators.required],
      "test_panel_code": [null, Validators.required],
      "test_location": [null, Validators.required],
      // "total_test_conducted": [null, Validators.required],
      "total_test_conducted":1,
      "remark": [null, Validators.required]

    });
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-lg' });
  }

  savePackageTestPanel() {
    this.isSubmitted = true;
    if (this.dataFormPTP.invalid) {
      this.toastrService.warning("Please complete the form before submitting", "Warning");
      return;
    }

    // console.log(this.mode);
    this.coolDialogs.confirm("Are you sure to save this info?")
      .subscribe(res => {
        if (res) {
          if (this.mode == "NEW") {
            this.dataService.createSPPacTestPanel(this.dataFormPTP.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message, "Warning"); else {
                this.toastrService.success(data.message, "Success");
                this.isSubmitted = false;
                this.fetchData();
                this.modalRef.hide();
                // this.mode = "EDIT";
              }
            }, error => {
              if (error.status == 409)
                this.toastrService.error("Unable to create new patient package - data already exist", "Warning");
              else
                this.toastrService.error("Unable to create new patient package - service unavailabe", "Warning");
            });
          } else {
            this.dataService.updateSPPacTestPanel(this.dataFormPTP.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message, "Warning"); else {
                this.toastrService.success(data.message, "Success");
                this.isSubmitted = false;
                this.fetchData();
                this.modalRef.hide();
              }
            }, error => {
              this.toastrService.error("Unable to update patient package info", "Warning");
            });
          }
        }
      });
  }

  deleteSPPackageTestPanel(code) {
    this.coolDialogs.confirm("Are you sure to delete selected record?")
      .subscribe(res => {
        if (res) {
          this.dataService.deletePackageTestPanel(code).subscribe(data => {
            if (data.errorFound) this.toastrService.error(data.message, "Warning"); else {
              this.toastrService.success(data.message, "Success");
              this.fetchData();
            }
          });
        }
      }, error => {
        this.toastrService.error("Unable to delete selected record", "Warning");
      });
  }

 // PACKAGE ADD ON / PACKAGES
  dataFormAOPackage: FormGroup;
  dataAOPackage: any;
  newAddOnPackage(template: TemplateRef<any>, plan) {
    this.selectedPackagePlan = plan;
    this.mode = "NEW";
    this.dataFormAOPackage = this.formBuilder.group({
      "package_code": [plan.package_code, Validators.required],
      "test_group_code": [null, Validators.required],
      "test_location": [null, Validators.required],
      // "total_test_conducted": [null, Validators.required],
      "total_test_conducted":1,
      "remark": [null, Validators.required]
    });
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-lg' });
  }

  saveAddOnPackage() {
    this.isSubmitted = true;
    if (this.dataFormAOPackage.invalid) {
      this.toastrService.warning("Please complete the form before submitting", "Warning");
      return;
    }

    // console.log(this.mode);
    this.coolDialogs.confirm("Are you sure to save this info?")
      .subscribe(res => {
        if (res) {
          if (this.mode == "NEW") {
            this.dataService.createSPAddOnPackage(this.dataFormAOPackage.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message, "Warning"); else {
                this.toastrService.success(data.message, "Success");
                this.isSubmitted = false;
                this.fetchData();
                this.modalRef.hide();
                // this.mode = "EDIT";
              }
            }, error => {
              if (error.status == 409)
                this.toastrService.error("Unable to create new plan package - data already exist", "Warning");
              else
                this.toastrService.error("Unable to create new plan package - service unavailabe", "Warning");
            });
          } else {
            this.dataService.updateSPAddOnPackage(this.dataFormAOPackage.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message, "Warning"); else {
                this.toastrService.success(data.message, "Success");
                this.isSubmitted = false;
                this.fetchData();
                this.modalRef.hide();
              }
            }, error => {
              this.toastrService.error("Unable to update patient package info", "Warning");
            });
          }
        }
      });
  }

  deleteAddOnPackage(code) {
    this.coolDialogs.confirm("Are you sure to delete selected record?")
      .subscribe(res => {
        if (res) {
          this.dataService.deleteSPAddOnPackage(code).subscribe(data => {
            if (data.errorFound) this.toastrService.error(data.message, "Warning"); else {
              this.toastrService.success(data.message, "Success");
              this.fetchData();
              // this.dataService.getOneScreenPlan(this.selectedPackagePlan.package_code).subscribe(data => {
              //   this.selectedPackagePlan = data;
              // });
            }
          });
        }
      }, error => {
        this.toastrService.error("Unable to delete selected plan package record", "Warning");
      });
  }

  editAddOnPackage(addOnP) {
    this.modePatient = "EDIT";
    this.dataFormPP = this.formBuilder.group(addOnP);
  }


  // PACKAGE ADD ON / SERVICES
  dataFormAOService: FormGroup;
  dataAOService: any;

  newAddOnService(template: TemplateRef<any>, plan) {
    console.log(plan);
    this.selectedPackagePlan = plan;
    this.mode = "NEW";
    this.dataFormAOService = this.formBuilder.group({
      "package_code": [plan.package_code, Validators.required],
      "add_on_code": [null, Validators.required],
      "add_on_name": [null],
      "test_location_code": [null, Validators.required],
      "test_location_name": [null],
      // "total_test_conducted": [null, Validators.required],
      "total_test_conducted":1,
      "patient_type_code": [null, Validators.required]

    });
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-lg' });
  }

  saveAddOnService() {
    this.isSubmitted = true;
    if (this.dataFormAOService.invalid) {
      this.toastrService.warning("Please complete the form before submitting", "Warning");
      return;
    }

    // console.log(this.mode);
    this.coolDialogs.confirm("Are you sure to save this info?")
      .subscribe(res => {
        if (res) {
          if (this.mode == "NEW") {
            this.dataService.createSPAddOnService(this.dataFormAOService.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message, "Warning"); else {
                this.toastrService.success(data.message, "Success");
                this.isSubmitted = false;
                this.fetchData();
                this.modalRef.hide();
                // this.mode = "EDIT";
              }
            }, error => {
              if (error.status == 409)
                this.toastrService.error("Unable to create new plan package - data already exist", "Warning");
              else
                this.toastrService.error("Unable to create new plan package - service unavailabe", "Warning");
            });
          } else {
            this.dataService.updateSPAddOnService(this.dataFormAOService.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message, "Warning"); else {
                this.toastrService.success(data.message, "Success");
                this.isSubmitted = false;
                this.fetchData();
                this.modalRef.hide();
              }
            }, error => {
              this.toastrService.error("Unable to update patient package info", "Warning");
            });
          }
        }
      });
  }

  deleteAddOnService(code) {
    this.coolDialogs.confirm("Are you sure to delete selected record?")
      .subscribe(res => {
        if (res) {
          this.dataService.deleteSPAddOnService(code).subscribe(data => {
            if (data.errorFound) this.toastrService.error(data.message, "Warning"); else {
              this.toastrService.success(data.message, "Success");
              this.fetchData();
              // this.dataService.getOneScreenPlan(this.selectedPackagePlan.package_code).subscribe(data => {
              //   this.selectedPackagePlan = data;
              // });
            }
          });
        }
      }, error => {
        this.toastrService.error("Unable to delete screening plan record", "Warning");
      });
  }

  editAddOnService(addOnS) {
    this.modePatient = "EDIT";
    this.dataFormPP = this.formBuilder.group(addOnS);
  }



  // LOOKUP LIST
  patientTypes: any = [];
  fetchPatientType() {
    this.dataService.getLPatientType().subscribe(result => {
      if (result.data) this.patientTypes = result.data;
    }, error => {
      this.isLoading = false;
    })
  }

  addOnTypes: any = [];
  fetchAddOn() {
    this.dataService.getAllAddOn().subscribe(result => {
      if (result.data) this.addOnTypes = result.data;
    }, error => {
      this.isLoading = false;
    })
  }

  testPanelTypes: any = [];
  fetchTestPanel() {
    this.dataService.getAllTestPanel().subscribe(result => {
      if (result.data) this.testPanelTypes = result.data;
    }, error => {
      this.isLoading = false;
    })
  }


  locationTypes: any = [];
  fetchLocation() {
    this.dataService.getLLocationType().subscribe(result => {
      if (result.data) this.locationTypes = result.data;
    }, error => {
      this.isLoading = false;
    })
  }

  testGroupTypes: any = [];
  fetchTestGroup() {
    this.dataService.getAllAddOnPackage().subscribe(result => {
      if (result.data) this.testGroupTypes = result.data;
    }, error => {
      this.isLoading = false;
    })
  }

  closeScreeningPlan() {
    this.modePatient = null;
    this.modalRef.hide();
    this.fetchData();
  }

  cancelPackagePatient() {
    this.modePatient = null;
    this.fetchData();
    
  }

  setDefaultPic(event) {
    event.target.src = "assets/images/no_picture.png";
  }

}
