import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from '../../../../service/data.service';
import { ToastrService } from 'ngx-toastr';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { environment } from '../../../../../environments/environment';
import * as _ from "lodash";


@Component({
  selector: 'app-admin-registration-add',
  templateUrl: './registration-add.component.html',
  styleUrls: ['./registration-add.component.scss']
})
export class RegistrationAddComponent implements OnInit {

  constructor(private dataService: DataService,
    private formBuilder: FormBuilder,
    private router: Router,
    private toastrService: ToastrService,
    private coolDialogs: NgxCoolDialogsService
  ) {

  }

  loading = true;

  ngOnInit() {
    this.newRegistration();
    this.fetchLookupInfo();
  }

  optChange(evt){
    this.formDetails.get("package_code").setValue(evt.target.value);
  }

  objectKeys = Object.keys;
  screeningPlans: any = [];
  newRegistration() {
    this.isSubmitted = false;
    this.formDetails = this.formBuilder.group({
      name: [null, Validators.required],
      // password: null,
      ic_no: [null, Validators.required],
      email: [null, Validators.required],
      mobile_no: [null, Validators.required],
      package_code: [null, Validators.required],
      amount_paid: [null, Validators.required],
      payment_no: [null, Validators.required],
      payment_date: [null, Validators.required],
      payment_method: 'CASH',
      date_registered: null,
      date_expired: null,
      status: 'PRE-ACCOUNT',
      center_code: 'MONT KIARA'
    });
    this.screeningPlans = [];
    this.dataService.getAllScreeningPlan().subscribe(res => {
      let screeningPlans = _.groupBy(res.data, 'category_code');
      for (var key in screeningPlans) {
        this.screeningPlans.push({ 'category': key, 'plans': screeningPlans[key] })
      }
    });
  }

  formDetails: FormGroup;
  isSubmitted=false;
  optPackageCode:any;
  registerNewUser() {
    this.isSubmitted = true;
    if (this.formDetails.invalid){
      this.toastrService.warning("Please complete the form details before submitting", "Incomplete information");
      return;
    }
    this.coolDialogs.confirm("Are you sure to register this user?")
      .subscribe(res => {
        if (res) {
          console.log(this.formDetails.value);
          this.dataService.preRegister(this.formDetails.value).subscribe(result => {
            this.router.navigate(['/administrator/user-registration']);
            this.toastrService.success("The user PRE-ACCOUNT has been successfully created.", "Success");
          }, err => {
            this.toastrService.error(err.error.message, "Process failed");
          });
          this.isSubmitted = false;
        }
      });
  }

  states: any;
  paymentMethods: any;
  fetchLookupInfo() {
    this.dataService.getStates().subscribe(res => {
      this.states = res.data;
    });
    this.dataService.getPaymentMethods().subscribe(res => {
      this.paymentMethods = res.data;
    });
 
  }
}
