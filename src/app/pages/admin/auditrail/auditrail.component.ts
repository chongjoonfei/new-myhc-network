import { TemplateRef } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { ToastrService } from 'ngx-toastr';
import { DataService } from '../../../service/data.service';
import {TranslateService} from '@ngx-translate/core';


declare const AmCharts: any;
declare const $: any;

@Component({
  selector: 'app-auditrail',
  templateUrl: './auditrail.component.html',
  styleUrls: [
    './auditrail.component.scss'
  ]
})
export class AuditrailComponent implements OnInit {

  constructor(
    private dataService: DataService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private toastrService: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    public translate: TranslateService
  ) { 

  }

  AccDetails : any;
  // dataRegistration: any;
  modalRef: BsModalRef;
  loading = true;


  ngOnInit() {
    // this.fetchRegistration();
    this.fetchAuditrail();
   
  }

  dataAuditrail: any;
  
  fetchAuditrail() {
    this.loading = true;
    this.dataAuditrail = [];   

    // this.AccDetails = JSON.parse(localStorage.getItem("auth"));
    // let dataAut = {
    //   intProject :1,
    //   strModuleNamePathTblName :"Auditrail >> /administrator/auditrail >> auditrail_staff",
    //   intRecordId :0,
    //   intAction :4, // 1=Login,2=Logout,3=Create,4=Read,5=Update,6=Delete
    //   strAddedByUsername :this.AccDetails.account.username,
    //   strAddedByEmail :this.AccDetails.account.email     
    // };
    // this.dataService.createAuditrail(dataAut).subscribe(resp=>{
    //   console.log(resp);
    // },error =>{console.log(error);});
    
    this.dataService.getAllAuditrail().subscribe(res => {
      
      this.dataAuditrail = res.data;
      this.loading = false;
    }, error => {
      this.loading = false;
    })
  }

}
