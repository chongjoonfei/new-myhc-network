import { TemplateRef } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { ToastrService } from 'ngx-toastr';
import { DataService } from '../../../service/data.service';

import {TranslateService} from '@ngx-translate/core';


declare const AmCharts: any;
declare const $: any;

@Component({
  selector: 'app-booking-mgmt',
  templateUrl: './booking-mgmt.component.html',
  styleUrls: [
    './booking-mgmt.component.scss'
  ]
})
export class BookingMgmtComponent implements OnInit {

  constructor(
    private dataService: DataService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private toastrService: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    public translate: TranslateService
  ) {}


  // dataRegistration: any;
  modalRef: BsModalRef;
  loading = true;


  ngOnInit() {
    // this.fetchRegistration();
    this.fetchState();
    this.fetchBooking();
  }

  completeBooking(booking) {
    let confirmMsg = "Are you sure to complete this booking?";
    if (booking.status == 'CANCELLED') confirmMsg = "Are you sure to complete refund this booking?";
    this.coolDialogs.confirm(confirmMsg)
      .subscribe(confirm => {
        if (confirm) {
          let updateData = this.formBuilder.group(booking);
          if (booking.status == 'PENDING') {
            updateData.value.status = 'COMPLETED';
            this.dataService.updateBooking(updateData.value, 'STATUS').subscribe(res => {
              if (res.errorFound) {
                this.toastrService.error("Failed to complete this booking", "Process failed");
              } else {
                this.toastrService.success("The booking has been completed.", "Success");
                this.fetchBooking();
              }
            })
          }
          if (booking.status == 'CANCELLED') {
            updateData.value.remark = 'REFUND DONE';
            this.dataService.updateBooking(updateData.value, 'REMARK').subscribe(res => {
              if (res.errorFound) {
                this.toastrService.error("Failed to complete refund this booking", "Process failed");
              } else {
                this.toastrService.success("The booking has been completely refunded.", "Success");
                this.fetchBooking();
              }
            })
          }
        }
      })
  }

  cancelBooking(booking) {
    this.coolDialogs.confirm("Are you sure to cancel this booking?")
      .subscribe(confirm => {
        if (confirm) {
          let updateData = this.formBuilder.group(booking);
          updateData.value.status = 'CANCELLED';
          if (booking.payments.length > 0) updateData.value.remark = "REFUND IS ON-GOING";

          this.dataService.updateBooking(updateData.value, 'STATUS').subscribe(res => {
            if (res.errorFound) {
              this.toastrService.error("Failed to cancel this booking", "Process failed");
            } else {
              this.toastrService.success("The booking has been cancelled.", "Success");
              this.fetchBooking();
            }
          },
          error=>{
            this.toastrService.error("Failed to cancel this booking", "Process failed");
          })
        }
      })
  }


  dataBooking: any;
  fetchBooking() {
    this.loading = true;
    this.dataBooking = [];
    this.dataService.searchBookings("", "PENDING,PENDING-CANCELLATION,CANCELLED").subscribe(res => {
      this.dataBooking = res.data;
      this.loading = false;
      this.dataSearchBooking = null;
      this.keyword = null;
    }, error => {
      this.loading = false;
    })
  }

  dataScreening: any;
  fetchScreeningCentre() {
    this.dataService.getAllScreeningCenters().subscribe(res => {
      this.dataScreening = res.data
    })
  }

  dataTimeSlots: any;
  fetchTimeSlots() {
    this.loadingTimeslot = true;
    this.dataService.getAllTimeslots().subscribe(res => {
      this.dataTimeSlots = res.data;
      this.loadingTimeslot = false;
    }, error => {
      this.loadingTimeslot = false;
    })
  }

  dataState: any;
  fetchState() {
    this.dataService.getStates().subscribe(res => {
      this.dataState = res.data
    })
  }

  mode: any;
  formScreeningCentre: FormGroup;
  formTimeSlot: FormGroup;
  openTimeSlots(template: TemplateRef<any>) {
    this.formTimeSlot = null;
    this.fetchTimeSlots();
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-md' });
  }

  editTimeSlot(data) {
    this.mode = "EDIT"
    this.formTimeSlot = this.formBuilder.group({
      ts_code: [data.ts_code],
      ts_description: [data.ts_description]
    })
  }

  addTimeSlot() {
    this.mode = "ADD"
    this.formTimeSlot = this.formBuilder.group({
      ts_code: [null],
      ts_description: [null]
    })
  }

  deleteTimeSlot(data) {
    this.coolDialogs.confirm("Are you sure to delete this time slot?")
      .subscribe(confirm => {
        if (confirm) {
          this.dataService.deleteTimeSlot(data).subscribe(res => {
            if (res.errorFound) {
              this.toastrService.error("Failed to delete this time slot", "Process failed");
            } else {
              this.toastrService.success("This time slot has been successfully deleted.", "Success");
              this.formTimeSlot = null;
              this.fetchTimeSlots();
            }
          })

        }
      });
  }

  saveTimeSlot() {
    if (this.formTimeSlot.invalid) {
      this.toastrService.warning("Please complete the form details before submitting", "Incomplete information");
      return;
    }
    this.coolDialogs.confirm("Are you sure to save this form?")
      .subscribe(confirm => {
        if (confirm) {
          if (this.mode == "EDIT") {
            this.dataService.updateTimeSlot(this.formTimeSlot.value).subscribe(res => {
              if (res.errorFound) {
                this.toastrService.error("Failed to save edited time slot", "Process failed");
              } else {
                this.toastrService.success("This time slot has been successfully edited.", "Success");
                this.formTimeSlot = null;
                this.fetchTimeSlots();
              }

            })
          }
          else if (this.mode == "ADD") {
            this.dataService.createTimeSlot(this.formTimeSlot.value).subscribe(res => {
              if (res.errorFound) {
                this.toastrService.error("Failed to save new time slot", "Process failed");
              } else {
                this.toastrService.success("Successfully add a new time slot.", "Success");
                this.formTimeSlot = null;
                this.fetchTimeSlots();
              }

            }, err => {
              this.toastrService.error(err.error.message, "Process failed");
            });
          }
        }
      })
  }

  openScreening(template: TemplateRef<any>) {
    this.chkNewCenter = false;
    this.formScreeningCentre = null;
    this.selectedScreeningCenter = null;
    this.fetchScreeningCentre();
    this.fetchTimeSlots();
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-lg' });
  }

  modeCenter: any;
  addScreeningCentre(evt) {
    if (evt.target.checked == true) {
      this.modeCenter = "NEW";
      this.formScreeningCentre = this.formBuilder.group({
        sc_id: ['0'],
        sc_name: [null, [Validators.required]],
        sc_address: [null, [Validators.required]],
        sc_postcode: [null, [Validators.required]],
        sc_state: [null, [Validators.required]],
        sc_telephone: [null, [Validators.required]],
        sc_person_in_charge: [null, [Validators.required]],
        sc_picture_path: [null],
        timeslots: this.formBuilder.array([])
      });
    }

  }

  chkNewCenter: any;
  cancelCenter() {
    this.chkNewCenter.value = null;
    this.formScreeningCentre = null;
  }

  loadingTimeslot: any;
  fetchCenterTimeslot() {
    this.loadingTimeslot = true;
    this.dataService.getScreeningCenterTimeslots(this.selectedScreeningCenter).subscribe(data => {
      this.dataCenterTimeslot = data;
      this.loadingTimeslot = false;
    }, error => {
      this.loadingTimeslot = false;
    })
  }

  isSubmitted = false;
  selectedScreeningCenter: any = null;
  dataCenterTimeslot: any;
  editScreeningCentre(evt) {
    this.modeCenter = "EDIT";
    this.dataService.getScreeningCenterById(this.selectedScreeningCenter).subscribe(data => {
      this.formScreeningCentre = this.formBuilder.group(data);
    });
    this.fetchCenterTimeslot();
    this.chkNewCenter = false;

  }

  saveScreeningCentre() {
    if (this.formScreeningCentre.invalid) {
      this.toastrService.warning("Please complete the form details before submitting", "Incomplete information");
      return;
    }
    this.coolDialogs.confirm("Are you sure to save this form?")
      .subscribe(confirm => {
        if (confirm) {
          if (this.modeCenter == "EDIT") {
            this.dataService.updateScreeningCentre(this.formScreeningCentre.value).subscribe(res => {
              this.toastrService.success("The screening center has been successfully updated.", "Success");
              this.formScreeningCentre = null;
              this.selectedScreeningCenter = null;
              this.fetchScreeningCentre();
            })
          }
          else if (this.modeCenter == "NEW") {
            this.dataService.createScreeningCentre(this.formScreeningCentre.value).subscribe(res => {
              this.toastrService.success("The screening center has been successfully added.", "Success");
              this.formScreeningCentre = null;
              this.selectedScreeningCenter = null;
              this.fetchScreeningCentre();
            }, err => {
              this.toastrService.error(err.error.message, "Process failed");
            });
          }
        }
      })
  }

  deleteScreeningCenter() {
    this.coolDialogs.confirm("Are you sure to delete this screening center?")
      .subscribe(confirm => {
        if (confirm) {
          this.dataService.deleteScreeningCentre(this.formScreeningCentre.value).subscribe(res => {
            if (res.errorFound) {
              this.toastrService.error("Failed to delete this screening center.", "Failed");
            } else {
              this.formScreeningCentre = null;
              this.fetchScreeningCentre();
              this.toastrService.success("This screening center has been successfully deleted.", "Success");
              this.modalRef.hide();

            }
          }, error => {
            this.toastrService.error("Failed to delete this screening center.", "Failed");
          })
        }
      })
  }

  photoUrl: any;
  setDefaultPic(event) {
    event.target.src = "assets/images/no_picture.png";
  }

  selectedAddTimeslot: any = null;
  addCenterTimeslot() {
    let centerTimeslot = {
      ts_code: this.selectedAddTimeslot,
      sc_id: this.selectedScreeningCenter
    }
    this.dataService.addCentreTimeSlot(centerTimeslot).subscribe(res => {
      if (!res.errorFound) {
        this.fetchCenterTimeslot();
        this.toastrService.success("The time slot has been successfully added to the list.", "Success");
      } else {
        this.toastrService.error("Could not add the time slot to the list.", "Failed");
      }
    }, error => {
      this.toastrService.error("Could not add the time slot to the list.", "Failed");
    });
    // console.log(centerTimeslot);
  }

  deleteCenterTimeslot(data) {
    this.coolDialogs.confirm("Are you sure to remove this time slot from the list?")
      .subscribe(confirm => {
        if (confirm) {
          this.dataService.deleteCentreTimeSlot(data).subscribe(res => {
            if (!res.errorFound) {
              this.fetchCenterTimeslot();
              this.toastrService.success("The time slot has been successfully removed to the list.", "Success");
            } else {
              this.toastrService.error("Could not remove the time slot to the list.", "Failed");
            }
          }, error => {
            this.toastrService.error("Could not remove the time slot to the list.", "Failed");
          });
        }
      });

  }

  photoFile: any[] = [];
  tmpPhotoFile: any = [];
  uploading: any;
  uploadPhotoNow(event, pPhoto) {
    // console.log("uploadPhotoNow", event);
    this.photoFile = [];
    for (let file of event.files) {
      this.photoFile.push(file);
    }
    // console.log("upload", this.uploadedFiles);
    this.coolDialogs.confirm("Are you sure to upload this photo?")
      .subscribe(res => {
        if (res) {
          this.uploading = true;
          var formData = new FormData();
          formData.append('blank', this.selectedScreeningCenter);
          formData.append('sc_id', this.selectedScreeningCenter);
          formData.append("fileToUpload[]", event.files[0]);
          
          this.dataService.uploadCenterPhoto(formData).subscribe(res => {
            if (res.errorFound) {
              this.tmpPhotoFile = [];
              this.photoFile = [];
              this.toastrService.error("Error in uploading photo", "Failed");
            } else {
              this.tmpPhotoFile = [];
              this.photoFile = [];
              this.toastrService.success("Photo has been successfully uploaded", "Success");
            }
            this.uploading = false;
          }, error => {
            this.toastrService.error("Error in uploading photo", "Failed");
            this.uploading = false;
          });
        }
      });
  }


  dataSearchBooking: any;
  keyword: any;
  isSearching: any;
  selectedBookingStatus: any = "COMPLETED";
  searchBooking() {
    this.isSearching = true;
    this.dataService.searchBookings(this.keyword, this.selectedBookingStatus).subscribe(res => {
      this.dataSearchBooking = res.data;
      this.isSearching = false;
    }, error => {
      this.isSearching = false;
    })
  }

  selectedBooking: any;
  loadingReceipt=false;
  printReceipt(payment) {
    this.selectedBooking = payment;
    this.loadingReceipt = true;
    this.receiptTransNo = payment.trans_no;
    const arrRefNo = payment.ref_no.split("#");
    this.dataService.printReceiptByRefNo(payment.booking_no,arrRefNo[1]).subscribe(res => {
      var file = new Blob([res], { type: 'application/pdf' });
      var fileURL = URL.createObjectURL(file);
      window.open(fileURL);
      this.loadingReceipt = false;
    }, error => {
      console.log(error);
      this.loadingReceipt = false;
    })
  }

 
  receiptTransNo: any;
  loadingTestForm=false;
  printTestForm(booking) {
    this.selectedBooking = booking;
    this.loadingTestForm = true;
    this.dataService.printTestForm(booking.booking_no).subscribe(res => {
      var file = new Blob([res], { type: 'application/pdf' });
      var fileURL = URL.createObjectURL(file);
      window.open(fileURL);
      this.loadingTestForm = false;
    }, error => {
      console.log(error);
      this.loadingTestForm = false;
    })
  }


}
