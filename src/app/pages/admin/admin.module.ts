import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS,HttpClient } from '@angular/common/http';
import {ChartModule} from 'angular2-chartjs';
import {SharedModule} from '../../shared/shared.module';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
 
import { QRCodeSVGModule } from 'ngx-qrcode-svg';
import { TooltipModule } from 'ngx-bootstrap';
import { NgxChartsModule } from '@swimlane/ngx-charts';
 
import { MaterialModule } from '../../shared/material-module';
import { ScreeningPlanComponent } from './screening-plan/screening-plan.component';
import { AdminDashboardComponent } from './dashboard/admin-dashboard.component';
import { MessageBox2Component } from './message-box2/message-box2.component';
import { RegistrationComponent } from './user-registration/registration.component';
import { RegistrationAddComponent } from './user-registration/registration-add/registration-add.component';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { TableModule } from 'primeng-lts/table';
import { FieldsetModule } from 'primeng-lts/fieldset';
import { AdminSettingsomponent } from './admin-settings/admin-settings.component';
import { BookingMgmtComponent } from './booking-mgmt/booking-mgmt.component';
import { TransactionMgmtComponent } from './transaction-mgmt/transaction-mgmt.component';
import { AuditrailComponent } from './auditrail/auditrail.component';
import { ScreeningTestComponent } from './screening-test/screening-test.component';




@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    SharedModule,
    ChartModule,
    NgxChartsModule,
    QRCodeSVGModule,
    TooltipModule.forRoot(),
    MaterialModule,
    AngularEditorModule,
    TableModule,
    FieldsetModule,
    HttpClientModule,
  ],
  declarations: [
    ScreeningPlanComponent,
    AdminDashboardComponent,
    MessageBox2Component,
    AdminComponent,
    RegistrationComponent,
    RegistrationAddComponent,
    AdminSettingsomponent,
    BookingMgmtComponent,
    TransactionMgmtComponent,
    AuditrailComponent,
    ScreeningTestComponent
  ]
})
export class AdminModule { }
