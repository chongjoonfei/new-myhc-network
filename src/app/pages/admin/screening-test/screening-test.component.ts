import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap';

@Component({
  selector: 'app-screening-test',
  templateUrl: './screening-test.component.html',
  styleUrls: ['./screening-test.component.scss']
})
export class ScreeningTestComponent implements OnInit {


  singleGender = [
    {
      "name": "Male",
      "value": 19
    },
    {
      "name": "Female",
      "value": 12
    }
  ];

  singleAge = [
    {
      "name": "<30 yr old",
      "value": 7
    },
    {
      "name": "30 - 55 yr old",
      "value": 11
    },
    {
      "name": "55 -65 yr old",
      "value": 9
    },
    {
      "name": "> 65 yr old",
      "value": 4
    }
  ];

  multi = [
    {
      "name": "No of Individual",
      "series": [
        {
          "name": "Individual 1",
          "value": 30
        },
        {
          "name": "Individual 2",
          "value": 10
        },
        {
          "name": "Individual 3",
          "value": 4
        },
        {
          "name": "Individual 4",
          "value": 7
        },
        {
          "name": "Individual 5",
          "value": 13
        },
        {
          "name": "Individual 6",
          "value": 23
        },
        {
          "name": "Individual 7",
          "value": 33
        },
      ]
    }
  ];

  // options
  view: any[] = [400, ];
  viewLine: any[] = [];
  viewbar: any[] = [ , 650];
  gradient: boolean = false;
  showLegend: boolean = true;
  showLabels: boolean = true;
  isDoughnut: boolean = false;
  legendPosition: string = 'right';
  showXAxis: boolean = true;
  showYAxis: boolean = true;
  showXAxisLabel: boolean = true;
  yAxisLabel: string = 'Country';
  showYAxisLabel: boolean = true;
  xAxisLabel: string = 'Population';
  timeline: boolean = true;
  showRefLines: boolean = true;

  colorScheme = {
    domain: ['#029899', '#8ad056', '#006fc2', '#807d81']
  };

  referenceLines = [
    {"name": "OVERWEIGHT", "value": 25},
    {"name": "HEALTHY", "value": 20},
    {"name": "UNDERWEIGHT", "value": 18}
  ]

  constructor(
    private modalService: BsModalService,
    public translate: TranslateService
  ) { }

  ngOnInit() {
  }

  markerName= 'Body Mass Index';
  NCDModalView:any[] = [];
  showMarker(marker: any) {
    this.markerName = marker;
  }

}
