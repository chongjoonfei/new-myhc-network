import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { DataService } from '../../../service/data.service';
import { ToastrService } from 'ngx-toastr';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { environment } from '../../../../environments/environment';
import * as _ from "lodash";

import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-admin-settings',
  templateUrl: './admin-settings.component.html',
  styleUrls: ['./admin-settings.component.scss']
})
export class AdminSettingsomponent implements OnInit {

  constructor(private dataService: DataService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private toastrService: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    public translate: TranslateService
  ) {}

  loading = true;
  modalRef: BsModalRef;
  dataRegistration: any;
  ngOnInit() {
    this.fetchMainMenu();
    this.fetchAllAdminPerson();
    this.fetchAllGroup();
    this.fetchlookupInfo();


    let auth = JSON.parse(localStorage.getItem("auth"));           
    let dataAut = {
      intProject :1, // 1= MYHC , 2 = MYHC Network
      strModuleNamePathTblName :"Admin Settings >> /administrator/admin-settings >> menu_main",
      intRecordId :0,
      intAction :4, // 1=Login,2=Logout,3=Create,4=Read,5=Update,6=Delete
      strAddedByUsername :auth.account.username,
      strAddedByEmail :auth.account.email     
    };
    this.dataService.createAuditrail(dataAut).subscribe(resp=>{
      console.log(resp);
    },error =>{console.log(error);});




  }

  states: any;
  fetchlookupInfo() {
    this.dataService.getStates().subscribe(res => {
      this.states = res.data;
    });
  }

  keyword: any;
  dataPerson: any;
  isSearching = false;
  searchPerson() {
    this.dataPerson = null;
    this.isSearching = true;
    this.dataService.searchPerson(this.keyword).subscribe(res => {
      this.dataPerson = res.data;
      // console.log("search",res.data);
      this.isSearching = false;
    }, error => {
      this.isSearching = false;
    });
  }

  dataAdminPerson: any;
  fetchAllAdminPerson() {
    this.loading = true;
    this.dataService.getAllAdmin().subscribe(res => {
      this.dataAdminPerson = res.data

      // console.log(this.dataAdminPerson);

      this.loading = false;
    }, error => {
      this.loading = false;
    });
  }

  dataGroup: any;
  fetchAllGroup() {
    this.loading = true;
    this.dataService.getAllGroup().subscribe(res => {
      this.dataGroup = res.data

      this.loading = false;
    }, error => {
      this.loading = false;
    });
  }

  dataMainMenu: any;
  fetchMainMenu() {
    this.loading = true;
    this.dataService.getMainMenuAllItem().subscribe(res => {
      this.dataMainMenu = _.groupBy(res.data, 'owner');

       //console.log(this.dataMainMenu);

      this.loading = false;
    }, error => {
      this.loading = false;
    });
  }

  dataUniqueGroup: any;
  newAccount(template: TemplateRef<any>) {
    this.mode = "NEW";
    
    this.dataService.getDistinctGroup().subscribe(res => {
     
      this.dataUniqueGroup = res.data;
      console.log(res);

    });

   
    this.dataForm = this.formBuilder.group({
      name: [null, Validators.required],
      age: 0,
      ic_no: [null, Validators.required],
      mobile_no: [null, Validators.required],
      email: [null, [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      gender: null,
      patient_type_code: null,
      address: null,
      town: null,
      district: null,
      postcode: null,
      state: null,
      picture_path: null,
      status: null,
      relationship: null,
      reg_no: 'ADMIN',
      user_group: null,
      username: null
    });

    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-xl' });
  }

  mode: any;
  selectedPerson:any;
  editAccount(template: TemplateRef<any>, data) {
    this.selectedPerson = data;
    this.mode = "EDIT";
    this.dataForm = this.formBuilder.group({
      name: [data.person.name, Validators.required],
      age: 0,
      ic_no: [data.person.ic_no, Validators.required],
      mobile_no: [data.person.mobile_no, Validators.required],
      email: [data.person.email, [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      gender: null,
      patient_type_code: [data.person.patient_type_code],
      address: [data.person.address, Validators.required],
      town: [data.person.town, Validators.required],
      district: [data.person.district],
      postcode: [data.person.postcode, Validators.required],
      state: [data.person.state, Validators.required],
      status: [data.person.status],
      relationship: [data.person.relationship],
      reg_no: 'ADMIN',
      user_group: data.menu_owner,
      username: data.username
    });

    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-xl' });
  }

  resetPassword() {
    this.coolDialogs.confirm("Are you sure to reset password for " + this.selectedPerson.person.name + "?")
      .subscribe(confirm => {
        if (confirm) {
          let data = { "ic_no": this.selectedPerson.person.ic_no, "username": this.selectedPerson.username };

          this.dataService.resetPassword(data).subscribe(res => {
            if (res.errorFound) {
              this.toastrService.error("Unable to reset user password", "Failed");
            } else {
              this.toastrService.success("User password has been reset", "Success");
            }
          }, error => {
            this.toastrService.error("Unable to reset user password", "Failed");
          });

        }
      });
  }

  dataForm: FormGroup;
  isSubmitted = false;
  optPackageCode: any;
  saveAccount() {
    this.isSubmitted = true;
    if (this.dataForm.invalid) {
      this.toastrService.warning("Please complete the form details before submitting", "Incomplete information");
      return;
    }
    this.coolDialogs.confirm("Are you sure to save this admin account?")
      .subscribe(res => {
        if (res) {
          // console.log(this.dataForm.value);
          if (this.mode == 'NEW') {
            this.dataService.createAdminAccount(this.dataForm.value).subscribe(result => {
              this.fetchAllAdminPerson();
              this.toastrService.success("The admin account has been successfully created.", "Success");
              this.modalRef.hide();
            }, err => {
              this.toastrService.error(err.error.message, "Process failed");
            });
          } else {
            this.dataService.updateAdminAccount(this.dataForm.value).subscribe(result => {
              this.fetchAllAdminPerson();
              this.toastrService.success("The admin account has been successfully updated.", "Success");
              this.modalRef.hide();
            }, err => {
              this.toastrService.error(err.error.message, "Process failed");
            });
          }

          this.isSubmitted = false;
        }
      });
  }

  //
  //GROUP
  //
  dataGroupForm: FormGroup;
  newGroup(template: TemplateRef<any>) {
    this.mode = "NEW";
    this.dataGroupForm = this.formBuilder.group({
      main_id: [0],
      owner: [null, Validators.required],
      title: [null, Validators.required],
      lang_bm: [null, Validators.required]
    });

    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-xl' });
  }

  selectedGroup: any;
  editGroup(template: TemplateRef<any>, data) {
  
    this.selectedGroup = data;
    this.mode = "EDIT";
    this.dataGroupForm = this.formBuilder.group({
      main_id: [data.main_id, Validators.required],
      owner: [data.owner, Validators.required],
      title: [data.title, Validators.required],
      lang_bm: [data.lang_bm, Validators.required]
    });

    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-xl' });
  }

  saveGroup() {
    this.isSubmitted = true;
    if (this.dataGroupForm.invalid) {
      this.toastrService.warning("Please complete the form details before submitting", "Incomplete information");
      return;
    }
    this.coolDialogs.confirm("Are you sure to save this group name?")
      .subscribe(res => {
        if (res) {
          if (this.mode == 'NEW') {
            this.dataService.createGroup(this.dataGroupForm.value).subscribe(result => {
              this.fetchAllGroup();
              this.toastrService.success("The group has been successfully created.", "Success");
              this.modalRef.hide();
            }, err => {
              this.toastrService.error(err.error.message, "Process failed");
            });
          } else {
            this.dataService.updateGroup(this.dataGroupForm.value).subscribe(result => {
              this.fetchAllGroup();
              this.toastrService.success("The group has been successfully updated.", "Success");
              this.modalRef.hide();
            }, err => {
              this.toastrService.error(err.error.message, "Process failed");
            });
          }

          this.isSubmitted = false;
        }
      });
  }

  menuItems: any;
  selectedMainMenu: any;
  openMenuItem(template: TemplateRef<any>, mainMenu) {
    this.selectedMainMenu = mainMenu;
    this.dataService.getMenuAllItem().subscribe(res => {
      this.menuItems = res.data;
    });

    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-lg' });
  }

  addMenuItem(item) {
    let data = {
      main_id: this.selectedMainMenu.main_id,
      item_id: item.item_id,
      sort_id: this.selectedMainMenu.items.length + 1
    }

    this.coolDialogs.confirm("Are you sure to Add this menu item?")
      .subscribe(confirm => {
        if (confirm) {
          this.dataService.addMenuItem(data).subscribe(res => {
            this.fetchMainMenu();
            this.toastrService.success("The menu item has been successfully added.", "Success");
          }, err => {
            this.toastrService.error(err.error.message, "Process failed");
          });
        }
      });
  }

  deleteMenuItem(item_id, main_id) {
    let data = {
      main_id: main_id,
      item_id: item_id
    }
    this.coolDialogs.confirm("Are you sure to delete this menu item?")
      .subscribe(confirm => {
        if (confirm) {
          this.dataService.deleteMenuItem(data).subscribe(res => {
            this.fetchMainMenu();
            this.toastrService.success("The menu item has been successfully deleted.", "Success");
          }, err => {
            this.toastrService.error(err.error.message, "Process failed");
          });
        }
      });
  }



  // Add Edit Menuitem
  itemId: any;
  mainId: any;
  selectedItem: any;
  editMenuItem(template: TemplateRef<any>, item_id, main_id) {
    //alert(item_id +" <> "+ main_id);
    this.itemId = item_id;
    this.mainId = main_id;
    this.dataService.getMenuItemById(item_id, main_id).subscribe(res => {
      this.selectedItem = res;

      const createResult = res.create == 1 ? true : false;
      const readResult = res.read == 1 ? true : false;
      const updateResult = res.update == 1 ? true : false;
      const deleteResult = res.delete == 1 ? true : false;


      this.dataForm = this.formBuilder.group({
        create: [createResult],
        read: [readResult],
        update: [updateResult],
        delete: [deleteResult],
      });

    }, err => {
      this.toastrService.error(err.error.message, "Process failed");
    });

    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-lg' });
  }

  saveMenuItem() {
    this.isSubmitted = true;
    this.coolDialogs.confirm("Are you sure to save this modify permission?")
      .subscribe(res => {
        if (res) {
          
          this.dataForm.value.item_id = this.itemId;
          this.dataForm.value.main_id = this.mainId;
          this.dataForm.value.create = this.dataForm.value.create ? 1 : 0;
          this.dataForm.value.read = this.dataForm.value.read ? 1 : 0;
          this.dataForm.value.update = this.dataForm.value.update ? 1 : 0;
          this.dataForm.value.delete = this.dataForm.value.delete ? 1 : 0;
          //console.log(this.dataForm.value);
          this.dataService.updateMenuItem(this.dataForm.value).subscribe(result => {

            this.fetchMainMenu();
            this.toastrService.success("The mennu item permission has been successfully updated.", "Success");
            this.modalRef.hide();
          }, err => {
            this.toastrService.error(err.error.message, "Process failed");
          });
          
          this.isSubmitted = false;
        }
      });
  }

}
