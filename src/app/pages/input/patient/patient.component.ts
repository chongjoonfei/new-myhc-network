import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { DataService } from '../../../service/data.service';
import { ToastrService } from 'ngx-toastr';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.scss']
})
export class PatientComponent implements OnInit {

  constructor(private dataService: DataService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private toastrService: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    public translate: TranslateService
  ) {}

  isLoading = true;
  dataPatients: any = [];
  modalRef: BsModalRef;
  ngOnInit() {
    this.fetchData();
  }

  fetchData() {
    this.dataService.getAllPatients().subscribe(data => {
      if (data.records) this.dataPatients = data.records;
      this.isLoading = false;
    },error=>{
      this.isLoading = false;
    });
    this.newForm();
  }

  newForm() {
    this.dataForm = this.formBuilder.group({
      "refno": ['', Validators.required],
      "name": ['', Validators.required],
      "age": ['', Validators.required],
      "ic_no": ['', Validators.required],
      "mobile_no": ['', Validators.required],
      "email": ['', Validators.required]
    });
    this.mode = "NEW";
  }

  isSubmitted = false;
  mode = "NEW";
  public dataForm: FormGroup;
  saveForm() {
    this.isSubmitted = true;
    if (this.dataForm.invalid) {
      this.toastrService.warning("Please complete the form before submitting", "Warning");
      return;
    }

    this.coolDialogs.confirm("Are you sure to save this info?")
      .subscribe(res => {
        if (res) {
          if (this.mode == "NEW") {
            this.dataService.createPatientInfo(this.dataForm.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message, "Warning"); else {
                this.toastrService.success(data.message, "Success");
                this.isSubmitted = false;
                this.fetchData();
                this.modalRef.hide();
              }
            }, error => {
              if (error.status==409)
                this.toastrService.error("Unable to create patient info - data already exist", "Warning");
              else
                this.toastrService.error("Unable to create patient info - service unavailabe", "Warning");  
            });
          } else {
            this.dataService.updatePatientInfo(this.dataForm.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message, "Warning"); else {
                this.toastrService.success(data.message, "Success");
                this.isSubmitted = false;
                this.fetchData();
                this.modalRef.hide();
              }
            }, error => {
              this.toastrService.error("Unable to update patient info", "Warning");
            });
          }
        }
      });
  }

  edit(template: TemplateRef<any>,data){
    this.mode = "EDIT";
    this.dataForm = this.formBuilder.group({
      "refno": [data.refno, Validators.required],
      "name": [data.name, Validators.required],
      "age": [data.age, Validators.required],
      "ic_no": [data.ic_no, Validators.required],
      "mobile_no": [data.mobile_no, Validators.required],
      "email": [data.email, Validators.required]
    });
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-lg' });
  }

  createNew(template: TemplateRef<any>){
    this.newForm(); 
    this.mode = "NEW";
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-lg' });
  }

  delete(refno) {
    this.coolDialogs.confirm("Are you sure to delete info for "+ refno +"?")
      .subscribe(res => {
        if (res) {
          this.dataService.deletePatient(refno).subscribe(data => {
            if (data.errorFound) this.toastrService.error(data.message, "Warning"); else {
              this.toastrService.success(data.message, "Success");
              this.fetchData();
            }
          });
        }
      }, error => {
        this.toastrService.error("Unable to delete patient info", "Warning");
      });
  }

}
