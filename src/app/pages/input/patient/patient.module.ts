import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS,HttpClient } from '@angular/common/http';

import { PatientRoutingModule } from './patient-routing.module';
import { PatientComponent } from './patient.component';
import {SharedModule} from '../../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    PatientRoutingModule,
    SharedModule,
    HttpClientModule,
  ],
  declarations: [PatientComponent]
})
export class PatientModule { }
