import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { ToastrService } from 'ngx-toastr';
import { DataService } from '../../../service/data.service';
import {TranslateService} from '@ngx-translate/core';


@Component({
  selector: 'app-add-on-manage',
  templateUrl: './add-on-manage.component.html',
  styleUrls: [
    './add-on-manage.component.scss'
  ]
})
export class AddOnManageComponent implements OnInit {

  constructor(
    private dataService: DataService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private toastrService: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    public translate: TranslateService
  ) {}

  isLoading = true;
  modalRef: BsModalRef;
  dataAddOn: any;
  mode: any;
  dataNewAddOn: any;
  dataForm: FormGroup;
  dataFormAddOn: FormGroup;

  isSubmitted = false;

  ngOnInit() {
    this.fetchData();
    this.fetchPatientType();
  }

  fetchData() {
    this.dataService.getAllAddOn().subscribe(result => {
      if (result.data) this.dataAddOn = result.data;
    }, error => {
      this.isLoading = false;
    });
  }


  newAddOn(template: TemplateRef<any>) {
    this.mode = "NEW";
    this.dataFormAddOn = this.formBuilder.group({
      "uid": [0],
      "add_on_code": [null],
      "name": [null, Validators.required],
      "price": [null, Validators.required],
      "unit": null,
      "unit_decimal": null,
      "remark": null,
      "status": [null, Validators.required],
      "no_of_patient": "1",
      "patient_type_code": null
        });
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-lg' });
  }

  saveAddOn() {
    this.isSubmitted = true;
    if (this.dataFormAddOn.invalid) {
      this.toastrService.warning("Please complete the form before submitting", "Warning");
      return;
    }

    this.coolDialogs.confirm("Are you sure to save this info?")
      .subscribe(res => {
        if (res) {
          if (this.mode == "NEW") {
            this.dataService.ceateAddOn(this.dataFormAddOn.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message, "Warning"); else {
                this.toastrService.success(data.message, "Success");
                this.isSubmitted = false;
                this.fetchData();
                this.modalRef.hide();
              }
            }, error => {
              if (error.status == 409)
                this.toastrService.error("Unable to create new add on - data already exist", "Warning");
              else
                this.toastrService.error("Unable to create new add on - service unavailabe", "Warning");
            });
          } else {
            this.dataService.updateAddOn(this.dataFormAddOn.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message, "Warning"); else {
                this.toastrService.success(data.message, "Success");
                this.isSubmitted = false;
                this.fetchData();
                this.modalRef.hide();
              }
            }, error => {
              this.toastrService.error("Unable to update add on info", "Warning");
            });
          }
        }
      });
  }

  editAddOn(template: TemplateRef<any>, data) {
    this.mode = "EDIT";
    this.dataFormAddOn = this.formBuilder.group({
      "uid": [data.uid],
      "add_on_code": [data.add_on_code],
      "name": [data.name, Validators.required],
      "price":[data.price, Validators.required],
      "unit": data.unit,
      "unit_decimal": data.unit_decimal,
      "remark": data.remark,
      "status": [data.status, Validators.required],
      "no_of_patient": data.no_of_patient,
      "patient_type_code": data.patient_type_code,
    });
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-lg' });
  }

  deleteRecordAddOn(code) {
    this.coolDialogs.confirm("Are you sure to delete selected Add On info?")
      .subscribe(res => {
        if (res) {
          this.dataService.deleteAddOn(code).subscribe(data => {
            if (data.errorFound) this.toastrService.error(data.message, "Warning"); else {
              this.toastrService.success(data.message, "Success");
              this.fetchData();
            }
          });
        }
      }, error => {
        this.toastrService.error("Unable to delete add on record", "Warning");
      });
  }


  statusTypes = [
    { status: 'Enabled' },
    { status: 'Disabled' }
  ]

  patientTypes: any = [];
  fetchPatientType() {
    this.dataService.getLPatientType().subscribe(result => {
      if (result.data) this.patientTypes = result.data;
    }, error => {
      this.isLoading = false;
    })
  }

}

