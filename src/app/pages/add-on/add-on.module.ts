import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddOnRoutingModule } from './add-on-routing.module';
import {SharedModule} from '../../shared/shared.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ToastrModule } from 'ngx-toastr';
import { AddOnPurchaseComponent } from './purchase/add-on-purchase.component';
import { AddOnManageComponent } from './manage/add-on-manage.component';
import { TooltipModule } from 'ngx-bootstrap';

import {CarouselModule} from 'primeng-lts/carousel';

@NgModule({
  imports: [
    CommonModule,
    AddOnRoutingModule,
    SharedModule,
    NgxChartsModule,
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-right'
    }),
    TooltipModule.forRoot(),
    CarouselModule,
  ],
  declarations: [
    AddOnPurchaseComponent,
    AddOnManageComponent
  ]
})
export class AddOnPurchaseModule { }
