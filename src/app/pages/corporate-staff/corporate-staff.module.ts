import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule, HttpClient} from '@angular/common/http';
 
import {ChartModule} from 'angular2-chartjs';
import {SharedModule} from '../../shared/shared.module';
import { CorporateStaffRoutingModule } from './corporate-staff-routing.module';
import { CorporateStaffComponent } from './corporate-staff.component';
 
import { QRCodeSVGModule } from 'ngx-qrcode-svg';
import { TooltipModule } from 'ngx-bootstrap';
 
import { MaterialModule } from '../../shared/material-module';
import { CorporateStaffDashboardComponent } from './dashboard/corporate-staff-dashboard.component';

@NgModule({
  imports: [
    CommonModule,
    CorporateStaffRoutingModule,
    SharedModule,
    ChartModule,
    QRCodeSVGModule,
    TooltipModule.forRoot(),
    MaterialModule,
    HttpClientModule,
  ],
  declarations: [
    CorporateStaffDashboardComponent,
    CorporateStaffComponent,
  ]
})
export class CorporateStaffModule { }
