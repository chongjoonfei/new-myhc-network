import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyAppointmentComponent } from './my-appointment/my-appointment.component';
// import { AuthGuard } from 'src/app/service/auth/auth.guard';
 
import { MyMedicationComponent } from './my-medication/my-medication.component';
import { MyQrCodeComponent } from './my-qr-code/my-qr-code.component';
 
import { UploadComponent } from './upload/upload.component';
import { BiodataComponent } from './biodata/biodata.component';

const routes: Routes = [
 
      // {
      //   path: '',
      //   component:  MyDashboardComponent,pathMatch: 'full'
      // },
      // {
      //   path: 'dashboard',
      //   component: MyDashboardComponent,pathMatch: 'full'
      // },
      {
        path: 'biodata',
        component: BiodataComponent,pathMatch: 'full'
      },
      {
        path: 'my-appointment',
        component: MyAppointmentComponent,pathMatch: 'full'
      },
      {
        path: 'my-medication',
        component: MyMedicationComponent,pathMatch: 'full'
      },
      // {
      //   path: 'add-on-purchase',
      //   component: AddOnPurchaseComponent,pathMatch: 'full'
      // },
      {
        path: 'upload',
        component: UploadComponent,pathMatch: 'full'
      },
      {
        path: 'my-qr-code',
        component: MyQrCodeComponent,pathMatch: 'full'
      }
 
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientRoutingModule { }
