import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../service/data.service';
import * as _ from "lodash";
import {TranslateService} from '@ngx-translate/core';
@Component({
  selector: 'app-my-qr-code',
  templateUrl: './my-qr-code.component.html',
  styleUrls: [
    './my-qr-code.component.scss' 
  ]
})
export class MyQrCodeComponent implements OnInit {
  
  constructor(private dataService: DataService,public translate: TranslateService) { }

  user:any;
  auth:any;
  dataRegistration:any;
  qrValue:any;
  dataPatients:any=[];
  errorCorrectionLevel='Q';
  ngOnInit() {
    this.auth = JSON.parse(localStorage.getItem("auth"));
    this.dataRegistration = JSON.parse(localStorage.getItem("plan-registration"));


    if (this.dataRegistration.screening_plan.category_code == 'CORPORATE' 
        || this.dataRegistration.screening_plan.category_code == 'INDIVIDUAL') {
      this.dataPatients = _.filter(this.dataRegistration.registration_persons, { 'ic_no': this.auth.account.ic_no });
    } else {
      this.dataPatients = this.dataRegistration.registration_persons;
    }


    this.qrValue = "Name:" + this.dataPatients[0].person.name + "; IC No:" + this.dataPatients[0].person.ic_no 
    + "; Package:" + this.dataRegistration.screening_plan.package_code
    + "; Expired:" + this.dataRegistration.date_expired ;



    // this.dataService.getUserDetails(this.auth.username).subscribe(user=> {
    //   this.user = user;
    //   this.qrValue = "Name:" + this.user.person.name + "; IC No:" + this.user.person.ic_no 
    //   + "; Package:" + this.dataRegistration.screening_plan.package_code
    //   + "; Expired:" + this.dataRegistration.date_expired ;
    // });
  }

  tabClick(evt){
    this.qrValue = "Name:" + this.dataPatients[evt.index].person.name + "; IC No:" + this.dataPatients[evt.index].person.ic_no 
    + "; Package:" + this.dataRegistration.screening_plan.package_code
    + "; Expired:" + this.dataRegistration.date_expired ;
  }

  
}

 