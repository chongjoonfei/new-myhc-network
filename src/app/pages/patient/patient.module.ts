import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS,HttpClient } from '@angular/common/http';
import {ChartModule} from 'angular2-chartjs';
import {SharedModule} from '../../shared/shared.module';
import { PatientRoutingModule } from './patient-routing.module';
import { PatientComponent } from './patient.component';
import { UploadComponent } from './upload/upload.component';
import { MyAppointmentComponent } from './my-appointment/my-appointment.component';
import { MyQrCodeComponent } from './my-qr-code/my-qr-code.component';
import { MyMedicationComponent } from './my-medication/my-medication.component';
import { QRCodeSVGModule } from 'ngx-qrcode-svg';
import { TooltipModule } from 'ngx-bootstrap';
import { MaterialModule } from '../../shared/material-module';
import { BiodataComponent } from './biodata/biodata.component';

@NgModule({
  imports: [
    CommonModule,
    PatientRoutingModule,
    SharedModule,
    ChartModule,
    QRCodeSVGModule,
    TooltipModule.forRoot(),
    MaterialModule,
    HttpClientModule,
  ],
  declarations: [
    PatientComponent,
    UploadComponent,
    MyAppointmentComponent,
    MyQrCodeComponent,
    MyMedicationComponent ,
    BiodataComponent
  ]
})
export class PatientModule { }
