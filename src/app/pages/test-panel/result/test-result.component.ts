import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { DataService } from '../../../service/data.service';
import * as _ from "lodash";
import { environment } from '../../../../environments/environment';
import { ChartConfigService } from '../../../../../src/app/service/chart-config.service';

import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-test-result',
  templateUrl: './test-result.component.html',
  styleUrls: [
    './test-result.component.scss'
  ]
})
export class TestResultComponent implements OnInit {

  constructor(private modalService: BsModalService,
    public chartConfig: ChartConfigService,
    private dataService: DataService,
    public translate: TranslateService
    ) { }


  // chartOption = {
  //   series: [
  //     {
  //       name: "High - 2013",
  //       data: [28, 29, 33, 36, 32, 32, 33]
  //     },
  //     {
  //       name: "Low - 2013",
  //       data: [12, 11, 14, 18, 17, 13, 13]
  //     }
  //   ],
  //   chart: {
  //     height: 350,
  //     type: 'line',
  //     dropShadow: {
  //       enabled: true,
  //       color: '#000',
  //       top: 18,
  //       left: 7,
  //       blur: 10,
  //       opacity: 0.2
  //     },
  //     toolbar: {
  //       show: false
  //     }
  //   },
  //   colors: ['#77B6EA', '#545454'],
  //   dataLabels: {
  //     enabled: true,
  //   },
  //   stroke: {
  //     curve: 'smooth'
  //   },
  //   title: {
  //     text: 'Average High & Low Temperature',
  //     align: 'left'
  //   },
  //   grid: {
  //     borderColor: '#e7e7e7',
  //     row: {
  //       colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
  //       opacity: 0.5
  //     },
  //   },
  //   markers: {
  //     size: 1
  //   },
  //   xaxis: {
  //     categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul'],
  //     title: {
  //       text: 'Month'
  //     }
  //   },
  //   yaxis: {
  //     title: {
  //       text: 'Temperature'
  //     },
  //     min: 5,
  //     max: 40
  //   },
  //   legend: {
  //     position: 'top',
  //     horizontalAlign: 'right',
  //     floating: true,
  //     offsetY: -25,
  //     offsetX: -5
  //   }
  // };

  charMarkers = [
    {
      panel_code: 'vital-sign',
      title: 'Body Mass Index (BMI)',
      unit: 'BMI (kg/m2)',
      dataSeries: [
        {
          dataCode: 'bmi',
          dataName: 'Body Mass Index (BMI)'
        }
      ]
    },
    {
      panel_code: 'vital-sign',
      title: 'Blood Pressure (Systolic / Diastolic)',
      unit: 'mmHG',
      dataSeries: [
        {
          dataCode: 'bp-systolic',
          dataName: 'Blood Pressure: Systolic'
        },
        {
          dataCode: 'bp-diastolic',
          dataName: 'Blood Pressure: Diastolic'
        }
      ]
    },
    {
      panel_code: 'diabetic',
      title: 'Fasting Glucose',
      unit: 'mmol/L',
      dataSeries: [
        {
          dataCode: 'fasting-glucose',
          dataName: 'Fasting Glucose'
        }
      ]
    },
    {
      panel_code: 'diabetic',
      title: 'HbA1c',
      unit: 'x10^12L',
      dataSeries: [
        {
          dataCode: 'hbA1c',
          dataName: 'HbA1c'
        }
      ]
    },
    {
      panel_code: 'liver',
      title: 'Total Protein / Albumin / Globulin',
      unit: 'g/L',
      dataSeries: [
        {
          dataCode: 't-protein',
          dataName: 'Total Protein'
        },
        {
          dataCode: 'albumin',
          dataName: 'Albumin'
        },
        {
          dataCode: 'globulin',
          dataName: 'Globulin'
        }

      ]
    },
    {
      panel_code: 'liver',
      title: 'Total and Direct Bilirubin',
      unit: 'umol/L',
      dataSeries: [
        {
          dataCode: 't-bilirubin',
          dataName: 'Total Bilirubin'
        },
        {
          dataCode: 'd-bilirubin',
          dataName: 'Direct Bilirubin'
        }
      ]
    },

    {
      panel_code: 'lipid',
      title: 'Low Density Lipoprotein (LDL) Cholesterol',
      unit: 'mmol/L',
      dataSeries: [
        {
          dataCode: 'ldl-chol',
          dataName: 'Low Density Lipoprotein (LDL) Cholesterol'
        }
      ]
    },
    {
      panel_code: 'lipid',
      title: 'Triglycerides',
      unit: 'mmol/L',
      dataSeries: [
        {
          dataCode: 'triglycerides',
          dataName: 'Triglycerides'
        }
      ]
    },
    {
      panel_code: 'renal',
      title: 'MicroAlbumin',
      unit: 'mg',
      dataSeries: [
        {
          dataCode: 'm-albumin',
          dataName: 'MicroAlbumin'
        }
      ]
    }
  ]

  refRangeColor = [
    { code: 'Low', colorClass: 'test-element-light-blue', colorCode: '#B2EBF2' },
    { code: 'Normal', colorClass: 'test-element-green', colorCode: '#B9F6CA' },
    { code: 'Borderline', colorClass: 'test-element-yellow', colorCode: '#FFF59D' },
    { code: 'High', colorClass: 'test-element-orange', colorCode: '#FFE0B2' },
    { code: 'Very High', colorClass: 'test-element-red', colorCode: '#EF9A9A' }
  ];


  refRangeGrafColorCode = { 'Low': '#00B0FF', 'Normal': '#43A047', 'Borderline': '#FFFF00', 'High': '#FF9800', 'Very High': '#E53935' };
  refRangeTableColorCode = { 'Low': '#B2EBF2', 'Normal': '#B9F6CA', 'Borderline': '#FFF59D', 'High': '#FFE0B2', 'Very High': '#EF9A9A' };

  modalRef: BsModalRef;
  dataRegistration: any;
  user: any;
  auth: any;
  dataPatients: any;
  staffPatient: any;
  loading = true;
  photoPath: any;
  dataPatient: any;
  dataAllTestPanels: any;
  ngOnInit() {
    this.auth = JSON.parse(localStorage.getItem("auth"));

    this.photoPath = environment.uploadPath;
    this.dataRegistration = JSON.parse(localStorage.getItem("plan-registration"));

    let user = _.filter(this.dataRegistration.registration_persons, { 'ic_no': this.auth.account.ic_no });
    this.user = user[0];

    if (this.dataRegistration.screening_plan.category_code == 'CORPORATE')
      this.staffPatient = 'staff'; else this.staffPatient = 'patient';
    if (this.user.user_account.acc_type_code == 'STAFF') {
      this.dataPatients = _.filter(this.dataRegistration.registration_persons, { 'ic_no': this.user.ic_no });
    } else {
      this.dataPatients = _.filter(this.dataRegistration.registration_persons, { 'person_type_code': 'PATIENT' });
    }


    if (this.dataPatients.length > 0) {
      this.dataPatient = this.dataPatients[0];
      this.getLatestResult();
    }


    this.dataService.getAllTestPanel().subscribe(res => {
      this.dataAllTestPanels = res.data;
    });
  }

  tabClick(evt) {
    console.log(evt.index);
    this.dataPatient = this.dataPatients[evt.index];
    this.getLatestResult();

  }

  dataChartMarkerOptions: any = [];
  dataSpecificPanel: any = [];
  panelSpecificTest = false;
  specificPanel = { name: null, code: null, person: null };
  dataTestMarkers: any = [];
  loadingSpecificResult = false;
  showSpecificTest(person, panel_code, panel_name) {

    this.dataService.getTestMarkerByPanelCode(panel_code).subscribe(res => {
      this.dataTestMarkers = res;
    });

    this.dataChartMarkerOptions = [];
    this.loadingSpecificResult = true;

    this.dataSpecificPanel = [];
    this.dataPatientAllResult.forEach(element => {
      let itemPanel = { test_date: element['test_date'], test_markers: [] };
      Object.entries(element).forEach(([key, value]) => {
        if (key.indexOf('test_panel') != -1) {
          if (value['test_panel_code'].indexOf(panel_code) != -1) {
            itemPanel['test_markers'] = value['test_markers'];
          }
        }

      })
      if (itemPanel['test_markers'].length > 0) this.dataSpecificPanel.push(itemPanel);

    });



    let dateCategories = [];
    this.dataSpecificPanel.forEach(element => {
      dateCategories.push(element.test_date);
    });

    let chartPanels = _.filter(this.charMarkers, { panel_code: panel_code });
    let testMarkerPanel = _.filter(this.dataAllTestPanels, { test_panel_code: panel_code });

    if (chartPanels.length > 0) {

      chartPanels.forEach(marker => {
        let co = new ChartConfigService().lineChartMarker;
        let max_yaxis = "0";
        // console.log("marker", marker);
        co.title.text = marker.title;
        co.yaxis.title.text = marker.unit;
        co.xaxis.title.text = "Date of testing";
        co.series = [];
        co.xaxis.categories = dateCategories;

        marker.dataSeries.forEach(serie => {
          let dataValues = [];

          this.dataSpecificPanel.forEach(dateTest => {
            let data = _.filter(dateTest.test_markers, { test_marker_code: serie.dataCode });
            if (data.length > 0) {
              dataValues.push(data[0].test_value);
              // console.log(data[0].test_value + '>' +  max_yaxis);
              if (parseFloat(data[0].test_value) > parseFloat(max_yaxis)) max_yaxis = data[0].test_value;
            } else
              dataValues.push(0);

          });
          co.yaxis.max = parseFloat(max_yaxis + '') + 3;
          // console.log("max_yaxis",co.yaxis.max,max_yaxis);
          co.series.push({ name: serie.dataName, data: dataValues });
          // max_yaxis = 0;
          if (testMarkerPanel.length > 0 && marker.dataSeries.length == 1) {
            let refRange = _.filter(testMarkerPanel[0].test_markers, { code: serie.dataCode });
            if (refRange.length > 0) {
              refRange[0].test_reference_range.forEach(ref => {
                co.annotations.yaxis.push(
                  {
                    position: 'back',
                    y: ref.min,
                    y2: ref.max,
                    borderColor: '#000',
                    fillColor: this.refRangeGrafColorCode[ref.code],
                  }
                )
              });
            }
          }
        });
        this.dataChartMarkerOptions.push(co);
      });
    }
    // console.log("this.dataChartMarkerOptions", this.dataChartMarkerOptions);

    this.specificPanel.code = panel_code;
    this.specificPanel.name = panel_name;
    this.specificPanel.person = person;
    this.panelSpecificTest = true;
    this.loadingSpecificResult = false;
  }

  // showSpecificTest(person, panel_code, panel_name) {

  //   this.dataService.getTestMarkerByPanelCode(panel_code).subscribe(res => {
  //     this.dataTestMarkers = res;
  //   });

  //   this.dataChartMarkerOptions = [];
  //   this.loadingSpecificResult = true;

  //   this.dataSpecificPanel = [];
  //   this.dataPatientAllResult.forEach(element => {
  //     let itemPanel = { test_date: element['test_date'], test_markers: [] };
  //     Object.entries(element).forEach(([key, value]) => {
  //       if (key.indexOf('test_panel') != -1) {
  //         if (value['test_panel_code'].indexOf(panel_code) != -1) {
  //           itemPanel['test_markers'] = value['test_markers'];
  //         }
  //       }

  //     })
  //     if (itemPanel['test_markers'].length > 0) this.dataSpecificPanel.push(itemPanel);

  //   });


  //   let dateCategories = [];
  //   this.dataSpecificPanel.forEach(element => {
  //     dateCategories.push(element.test_date);
  //   });

  //   let chartPanels = _.filter(this.charMarkers, { panel_code: panel_code });
  //   let testMarkerPanel = _.filter(this.dataAllTestPanels, { test_panel_code: panel_code });

  //   if (chartPanels.length > 0) {

  //     chartPanels.forEach(marker => {
  //       let co = new ChartConfigService().lineChartMarker;
  //       let max_yaxis = "0";
  //       // console.log("marker", marker);
  //       co.title.text = marker.title;
  //       co.yaxis.title.text = marker.unit;
  //       co.xaxis.title.text = "Date of testing";
  //       co.series = [];
  //       co.xaxis.categories = dateCategories;

  //       marker.dataSeries.forEach(serie => {
  //         let dataValues = [];

  //         this.dataSpecificPanel.forEach(dateTest => {
  //           let data = _.filter(dateTest.test_markers, { test_marker_code: serie.dataCode });
  //           if (data.length > 0) {
  //             dataValues.push(data[0].test_value); 
  //             // console.log(data[0].test_value + '>' +  max_yaxis);
  //             if (parseFloat(data[0].test_value) > parseFloat(max_yaxis)) max_yaxis = data[0].test_value;
  //           }else 
  //             dataValues.push(0);

  //         });
  //         co.yaxis.max = parseFloat(max_yaxis+'') + 3;
  //         // console.log("max_yaxis",co.yaxis.max,max_yaxis);
  //         co.series.push({ name: serie.dataName, data: dataValues });
  //         // max_yaxis = 0;
  //         if (testMarkerPanel.length > 0 && marker.dataSeries.length==1) {
  //           let refRange = _.filter(testMarkerPanel[0].test_markers, { code: serie.dataCode });
  //           if (refRange.length > 0) {
  //             refRange[0].test_reference_range.forEach(ref => {
  //               co.annotations.yaxis.push(
  //                 {
  //                   position: 'back',
  //                   y: ref.min,
  //                   y2: ref.max,
  //                   borderColor: '#000',
  //                   fillColor: this.refRangeGrafColorCode[ref.code],
  //                 }
  //               )
  //             });
  //           }
  //         }
  //       });
  //       this.dataChartMarkerOptions.push(co);
  //     });
  //   }
  //   // console.log("this.dataChartMarkerOptions", this.dataChartMarkerOptions);

  //   this.specificPanel.code = panel_code;
  //   this.specificPanel.name = panel_name;
  //   this.specificPanel.person = person;
  //   this.panelSpecificTest = true;
  //   this.loadingSpecificResult = false;
  // }

  setDefaultPic(event) {
    event.target.src = "assets/images/avatar.jpg";
  }

  dataPatientAllResult: any = [];
  dataPatientLatestResult: any;
  // getLatestResult() {
  //   this.loading = true;
  //   this.dataPatientAllResult = [];
  //   this.dataPatientLatestResult = null;
  //   this.dataService.searchTestResultRef(this.dataPatient.ic_no).subscribe(res => {
  //     // this.dataPatientAllResult = res;

  //     Object.entries(res).forEach(([key, value]) => {
  //       let totalElement = Object.entries(value).length;
  //       if (totalElement > 2) {
  //         let r = Object.entries(value)[totalElement - 1]; //take last element - latest record
  //         this.dataPatientLatestResult = r;
  //       }
  //       Object.entries(value).forEach(([key2, value2]) => {
  //         if (key2.indexOf('test_date') != -1) {
  //           this.dataPatientAllResult.push(value2);
  //         }
  //       });
  //     });
  //     this.loading = false;
  //   }, error => {
  //     this.loading = false;
  //   });
  // }

  getLatestResult() {
    this.loading = true;
    this.dataPatientAllResult = [];
    this.dataPatientLatestResult = [];
    this.dataService.searchTestResultRef(this.dataPatient.ic_no).subscribe(res => {
      // this.dataPatientAllResult = res;
      Object.entries(res).forEach(([key, value]) => {
        let totalElement = Object.entries(value).length;
        Object.entries(value).forEach(([key2, value2]) => {
          if (key2.indexOf('test_date') != -1) {
            this.dataPatientAllResult.push(value2);
          }
        });
      });
    }, error => {
    });

    this.dataService.searchDashboardResultRef(this.dataPatient.ic_no).subscribe(res => {
      Object.entries(res).forEach(([key, value]) => {
        Object.entries(value).forEach(([keyPanel, valuePanel]) => {
          if (keyPanel.indexOf('test_panel') != -1) {
            let panelLatestDate = {
              test_panel_code: valuePanel.test_panel_code,
              test_panel_name: valuePanel.test_panel_name, test_date: null, test_markers: []
            };
            // let panelAllDates = {
            //   test_panel_code: valuePanel.test_panel_code,
            //   test_panel_name: valuePanel.test_panel_name
            // };
            Object.entries(valuePanel).forEach(([keyMarker, valueMarker]) => {
              if (keyMarker.indexOf('test_marker_code') != -1) {
                let testDates = valueMarker['test_dates'];
                let len = valueMarker['test_dates'].length - 1;
                panelLatestDate.test_markers.push(testDates[len]);
                panelLatestDate.test_date = testDates[len].test_date;
                // let marker = {
                //   test_marker_code: "",
                //   test_marker_name: "",
                //   decimal_point: "",
                //   test_dates: ""
                // };
                // panelAllDates['test_markers'] = valueMarker;
              }
            });
            this.dataPatientLatestResult.push(panelLatestDate);
            // this.dataPatientAllResult.push(panelAllDates);
          }
        });

      });
      // console.log("this.dataPatientLatestResult", this.dataPatientLatestResult);
      // console.log("this.dataPatientAllResult", this.dataPatientAllResult);
      this.loading = false;
    }, error => {
      this.loading = false;
    });


  }

}

