import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { DataService } from '../../../service/data.service';
import { ToastrService } from 'ngx-toastr';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import * as _ from "lodash";
import { ThrowStmt } from '@angular/compiler';
import {TranslateService} from '@ngx-translate/core';
@Component({
  selector: 'app-test-group',
  templateUrl: './test-group.component.html',
  styleUrls: ['./test-group.component.scss']
})
export class TestGroupComponent implements OnInit {

  constructor(
    private dataService: DataService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private toastrService: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    public translate: TranslateService
  ) {}

  mode: any;
  modalRef: BsModalRef;
  isLoading = true;
  isSubmitted = false;

  ngOnInit() {

    this.fetchData();
    this.fetchTestPanel();
    this.fetchLocation();
    this.fetchPatientType();
    this.fetchPackageCategory();

  }

  fetchData() {
    this.dataService.getAllAddOnPackage().subscribe(result => {
      if (result.data) this.dataAddOnP = result.data;
      this.isLoading = false;
    });

  }

  selectedPackage: any;
  dataForm: FormGroup;
  dataFormTestGroup: FormGroup;
  dataFormAddOnP: FormGroup;
  dataTestGroup: any;
  dataAddOnP: any;

  newAddOnPackage(template: TemplateRef<any>) {
    this.mode = "NEW";
    this.dataFormAddOnP = this.formBuilder.group({
      "test_group_code": ['AOT'],
      "package_category": null,
      "group_name": [null, Validators.required],
      "patient_type": [null, Validators.required],
      "price": [null, Validators.required],
      "enabled": ["YES", Validators.required]

    });
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-lg' });
  }

  saveAddOnPackage() {
    this.isSubmitted = true;
    if (this.dataFormAddOnP.invalid) {
      this.toastrService.warning("Please complete the form before submitting", "Warning");
      return;
    }
    this.coolDialogs.confirm("Are you sure to save this info?")
      .subscribe(res => {
        if (res) {
          if (this.mode == "NEW") {
            this.dataService.createAddOnPackage(this.dataFormAddOnP.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message, "Warning"); else {
                this.toastrService.success(data.message, "Success");
                this.isSubmitted = false;
                
                this.mode = "EDIT";
                this.dataFormAddOnP = this.formBuilder.group({
                  "test_group_code": [data.content.test_group_code, Validators.required],
                  "package_category": data.content.package_category,
                  "group_name": [data.content.group_name, Validators.required],
                  "patient_type": [data.content.patient_type, Validators.required],
                  "price": [data.content.price, Validators.required],
                  "enabled": [data.content.enabled, Validators.required]
                });
                this.fetchData();
              }
            }, error => {
              if (error.status == 409)
                this.toastrService.error("Unable to create new plan package - data already exist", "Warning");
              else
                this.toastrService.error("Unable to create new plan package - service unavailabe", "Warning");
            });
          } else {
            this.dataService.updateAddOnPackage(this.dataFormAddOnP.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message, "Warning"); else {
                this.toastrService.success(data.message, "Success");
                this.isSubmitted = false;
                this.fetchData();
                this.modalRef.hide();
              }
            }, error => {
              this.toastrService.error("Unable to plan package info", "Warning");
            });
          }
        }
      });
  }

  editAddOnPackage(template: TemplateRef<any>, data) {
    this.selectedPackage = data;
    this.mode = "EDIT";
    this.dataFormAddOnP = this.formBuilder.group({
      "test_group_code": [data.test_group_code, Validators.required],
      "package_category": data.package_category,
      "group_name": [data.group_name, Validators.required],
      "patient_type": [data.patient_type, Validators.required],
      "price": [data.price, Validators.required],
      "enabled": [data.enabled, Validators.required]
    });
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-lg' });
  }

  deleteAddOnPackage(code) {
    this.coolDialogs.confirm("Are you sure to delete selected record?")
      .subscribe(res => {
        if (res) {
          this.dataService.deleteAddOnPackage(code).subscribe(data => {
            if (data.errorFound) this.toastrService.error(data.message, "Warning"); else {
              this.toastrService.success(data.message, "Success");
              this.fetchData();
            }
          });
        }
      }, error => {
        this.toastrService.error("Unable to delete screening plan record", "Warning");
      });
  }

  dataFormTestPanel: FormGroup;
  dataTestPanel: any;
  modeTPanel: any;

  editTestPanel(test_group_code) {
    this.modeTPanel = "EDIT";
    this.dataFormTestPanel = this.formBuilder.group(test_group_code);
  }

  newTestPanel(test_group_code) {
    this.modeTPanel = "NEW";
    this.dataFormTestPanel = this.formBuilder.group({
      "test_group_code": [test_group_code, Validators.required],
      "test_panel_code": [null, Validators.required]
    });
  }

  deleteTestPanel(tp) {
    this.coolDialogs.confirm("Are you sure to delete selected record?")
      .subscribe(res => {
        if (res) {
          let deleteTestPanel = {
            "test_group_code": this.selectedPackage.test_group_code,
            "test_panel_code": tp.test_panel_code
          };
          this.dataService.deleteTestPanelInAddOnPackage(deleteTestPanel).subscribe(data => {
            if (data.errorFound) this.toastrService.error(data.message, "Warning"); else {
              this.toastrService.success(data.message, "Success");
              this.dataService.getAddOnPackageOne(this.selectedPackage.test_group_code).subscribe(data => {
                this.selectedPackage = data;
              });
              this.fetchData();              
            }
          });
        }
      }, error => {
        this.toastrService.error("Unable to delete test panel from plan test item record", "Warning");
      });
  }

  addTestPanel() {
    this.isSubmitted = true;
    if (this.dataFormTestPanel.invalid || this.dataFormTestPanel.value.test_panel_code=='null') {
      this.toastrService.warning("Please complete the form before submitting", "Warning");
      return;
    }
    
    this.coolDialogs.confirm("Are you sure to save this info?")
      .subscribe(res => {
        if (res) {
          if (this.modeTPanel == "NEW") {
            this.dataService.createTestPanelInAddOnPackage(this.dataFormTestPanel.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message, "Warning"); else {
                this.toastrService.success(data.message, "Success");
                this.isSubmitted = false;
                this.fetchData();
                this.dataService.getAddOnPackageOne(this.selectedPackage.test_group_code).subscribe(data => {
                  this.selectedPackage = data;
                });
                this.modeTPanel = null;

              }
            }, error => {
              if (error.status == 409)
                this.toastrService.error("Unable to create add test panel - data already exist", "Warning");
              else
                this.toastrService.error("Unable to create add test panel - service unavailabe", "Warning");
            });
          } else {
            this.dataService.updateAddOnPackage(this.dataFormTestPanel.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message, "Warning"); else {
                this.toastrService.success(data.message, "Success");
                this.isSubmitted = false;
                this.dataService.getAddOnPackageOne(this.selectedPackage.test_group_code).subscribe(data => {
                  this.selectedPackage = data;
                });
                // this.fetchData();
                this.modalRef.hide();
                this.modeTPanel = null;
              }
            }, error => {
              this.toastrService.error("Unable to add test panel info", "Warning");
            });
          }
        }
      });
  }


  // Lookups

  locationTypes: any = [];
  fetchLocation() {
    this.dataService.getLLocationType().subscribe(result => {
      if (result.data) this.locationTypes = result.data;
    }, error => {
      this.isLoading = false;
    })
  }

  patientTypes: any = [];
  fetchPatientType() {
    this.dataService.getLPatientType().subscribe(result => {
      if (result.data) this.patientTypes = result.data;
    }, error => {
      this.isLoading = false;
    })
  }

  testPanelTypes: any = [];
  fetchTestPanel() {
    this.dataService.getAllTestPanel().subscribe(result => {
      if (result.data) this.testPanelTypes = result.data;
    }, error => {
      this.isLoading = false;
    })
  }

  packageCategoryTypes: any[];
  fetchPackageCategory() {
    this.dataService.getAllPackageCategory().subscribe(result => {
      if (result.data) this.packageCategoryTypes = result.data;
    }, error => {
      this.isLoading = false;
    })
  }

  cancel() {
    this.modeTPanel = null;
    this.modalRef.hide();
    this.fetchData();
  }



}
