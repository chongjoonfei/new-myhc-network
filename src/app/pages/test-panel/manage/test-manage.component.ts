import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { DataService } from '../../../service/data.service';
import { ToastrService } from 'ngx-toastr';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import * as _ from "lodash";
import {TranslateService} from '@ngx-translate/core';
@Component({
  selector: 'app-test-manage',
  templateUrl: './test-manage.component.html',
  styleUrls: ['./test-manage.component.scss']
})
export class TestManageComponent implements OnInit {

  constructor(private dataService: DataService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private toastrService: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    public translate: TranslateService
  ) {}

  isLoading = true;
  modalRef: BsModalRef;
  dataForm: FormGroup;
  dataFormPanel: FormGroup;
  dataFormMarker: FormGroup;
  dataFormRefRange: FormGroup;

  testMarkerRef = [
    { code: 'Low' },
    { code: 'Normal' },
    { code: 'Borderline' },
    { code: 'High' },
    { code: 'Very High' }
  ]

  dataTestPanel: any;
  dataTestMarker: any;
  dataRefRange: any;

  ngOnInit() {
    this.fetchData();
  }

  fetchData() {
    this.dataService.getAllTestPanel().subscribe(result => {
      if (result.data) this.dataTestPanel = result.data;
      // this.getSingleMarkerRecord();
      if (this.selectedMarkerRefRange) {
        let panel = _.filter(this.dataTestPanel, { test_panel_code: this.selectedMarkerRefRange.test_panel_code });
        // console.log("panel", panel);
        let markerRefRange = _.find(panel[0].test_markers, { code: this.selectedMarkerRefRange.code });
        // console.log("markerRefRange", markerRefRange);
        this.selectedMarkerRefRange.test_reference_range = [...markerRefRange.test_reference_range];
        this.modeRange = null;
      }
      this.isLoading = false;
    }, error => {
      this.isLoading = false;
    });

  }

  isSubmitted = false;
  mode = "NEW";
  editTestPanel(template: TemplateRef<any>, data) {
    this.mode = "EDIT";
    this.dataFormPanel = this.formBuilder.group({
      "panel_id": [data.panel_id, Validators.required],
      "name": [data.name, Validators.required],
      "test_panel_code": [data.test_panel_code, Validators.required],
      "description": [data.description],
      "input_type": [data.input_type, Validators.required]
    });
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-md' });
  }

  savePanel() {
    this.isSubmitted = true;
    let formValue = this.dataFormPanel.value;
    if (this.dataFormPanel.invalid) {
      this.toastrService.warning("Please complete the form before submitting", "Warning");
      return;
    }

    this.coolDialogs.confirm("Are you sure to save this info?")
      .subscribe(res => {
        if (res) {
          this.dataService.updateTestPanel(this.dataFormPanel.value).subscribe(data => {
            if (data.errorFound) this.toastrService.error(data.message, "Warning"); else {
              this.toastrService.success(data.message, "Success");
              this.isSubmitted = false;
              this.fetchData();
              this.modalRef.hide();
            }
          }, error => {
            this.toastrService.error("Unable to update test panel info", "Warning");
          });
        }
      });
  }

  selectedTestPanel:any;
  newTestMarker(template: TemplateRef<any>,testPanel) {
    this.selectedTestPanel = testPanel;
    this.mode = "NEW";
    this.dataFormMarker = this.formBuilder.group({
      "test_panel_code": [testPanel.test_panel_code, Validators.required],
      "name": [null, Validators.required],
      "code": [null, Validators.required],
      "description": [null, Validators.required],
      "unit": [null, Validators.required],
      "data_format": [0, Validators.required]
    });
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-md' });
  }

  // editTestMarker(template: TemplateRef<any>, data) {
  //   this.mode = "EDIT";
  //   this.dataFormMarker = this.formBuilder.group(data);
  // }

  editTestMarker(template: TemplateRef<any>, data,testPanel) {
    this.selectedTestPanel = testPanel;
    this.mode = "EDIT";
    this.dataFormMarker = this.formBuilder.group({
      "test_panel_code": [data.test_panel_code, Validators.required],
      "name": [data.name, Validators.required],
      "code": [data.code, Validators.required],
      "description": [data.description],
      "unit": [data.unit],
      "data_format": [data.data_format, Validators.required]
    });
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-md' });
  }

  saveMarker() {
    this.isSubmitted = true;
    let formValue = this.dataFormMarker.value;
    if (this.dataFormMarker.invalid) {
      this.toastrService.warning("Please complete the form before submitting", "Warning");
      return;
    }

    this.coolDialogs.confirm("Are you sure to save this info?")
      .subscribe(res => {
        if (res) {
          if (this.mode == "NEW") {
            this.dataService.createTestMarker(this.dataFormMarker.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message, "Warning"); else {
                this.toastrService.success(data.message, "Success");
                this.isSubmitted = false;
                this.fetchData();
                this.modalRef.hide();
                // this.mode = "EDIT";
              }
            }, error => {
              if (error.status == 409)
                this.toastrService.error("Unable to create new test marker - data already exist", "Warning");
              else
                this.toastrService.error("Unable to create new test marker - service unavailabe", "Warning");
            });
          } else {
            this.dataService.updateTestMarker(this.dataFormMarker.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message, "Warning"); else {
                this.toastrService.success(data.message, "Success");
                this.isSubmitted = false;
                this.fetchData();
                this.modalRef.hide();
              }
            }, error => {
              this.toastrService.error("Unable to test marker info", "Warning");
            });
          }
        }
      });
  }
  

  selectedMarkerRefRange: any;
  formEditRefRange: FormGroup;
  editRefRange(template: TemplateRef<any>, data) {
    // console.log("selectedMarkerRefRange",data);
    this.selectedMarkerRefRange = data;
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-lg' });
  }

  saveRefRange() {
    this.isSubmitted = true;
    let formValue = this.formEditRefRange.value;
    if (this.formEditRefRange.invalid) {
      this.toastrService.warning("Please complete the form before submitting", "Warning");
      return;
    }

    if (!this.isValidDecimalPlaces(this.formEditRefRange.get('min').value)){
      this.toastrService.warning("Invalid format for Min value", "Warning");
      return;
    }

    if (!this.isValidDecimalPlaces(this.formEditRefRange.get('max').value)){
      this.toastrService.warning("Invalid format for Max value", "Warning");
      return;
    }

    this.coolDialogs.confirm("Are you sure to save this info?")
      .subscribe(res => {
        if (res) {
          this.dataService.updateRefRange(this.formEditRefRange.value).subscribe(data => {
            if (data.errorFound) this.toastrService.error(data.message, "Warning"); else {
              this.toastrService.success(data.message, "Success");
              this.isSubmitted = false;
              this.fetchData();
            }
          }, error => {
            this.toastrService.error("Unable to save reference range", "Warning");
          });
        }
      });
  }

  modeRange: any;
  editRange(range) {
    this.modeRange = "Edit";
    // console.log(range)
    this.formEditRefRange = this.formBuilder.group(range);
    // this.fetchData();
  }

  newRange() {
    // console.log("selectedMarkerRefRange",this.selectedMarkerRefRange);
    this.modeRange = "New";
    this.formEditRefRange = this.formBuilder.group(
      {
        "test_marker_code": this.selectedMarkerRefRange.code,
        "code": null,
        "min": null,
        "max": null,
        "summary": null
      }
    );

  }

  deleteRange(data) {
    this.coolDialogs.confirm("Are you sure to delete selected reference range?")
      .subscribe(res => {
        if (res) {
          this.dataService.deleteRefRange(data).subscribe(data => {
            if (data.errorFound) this.toastrService.error(data.message, "Warning"); else {
              this.toastrService.success(data.message, "Success");
              this.fetchData();
            }
          });
        }
      }, error => {
        this.toastrService.error("Unable to delete test panel record", "Warning");
      });
  }

isValidDecimalPlaces(num) {
    var sep = String(23.32).match(/\D/)[0];
    var b = String(num).split(sep);
    let countDec = b[1]? b[1].length : 0;
    return (this.selectedMarkerRefRange.data_format==countDec);
}

}
