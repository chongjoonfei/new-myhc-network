import { Input, TemplateRef } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { DataService } from '../../../service/data.service';
import { ToastrService } from 'ngx-toastr';
import * as _ from "lodash";
import { environment } from '../../../../environments/environment';
import { ActivatedRoute } from '@angular/router';
import {TranslateService} from '@ngx-translate/core';


@Component({
  selector: 'app-dsh-company-details',
  templateUrl: './company-details.component.html',
  styleUrls: [
    './company-details.component.scss'
  ]
})
export class DashboardCompanyDetailsComponent implements OnInit {



  constructor(
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private coolDialogs: NgxCoolDialogsService,
    private dataService: DataService,
    private toastrService: ToastrService,
    private route: ActivatedRoute,
    public translate: TranslateService
  ) {}

  modalRef: BsModalRef;
  @Input('data-registration') dataRegistration: any;

  user: any;
  auth: any;
  dataPatients: any;
  staffPatient: any;
  loading = true;
  ngOnInit() {
    // console.log(this.dataRegistration);
    this.fetchStateInfo();
    this.auth = JSON.parse(localStorage.getItem("auth"));

    let user = _.filter(this.dataRegistration.registration_persons, { 'ic_no': this.auth.account.ic_no });
    this.user = user[0];
    // this.dataService.getUserDetails(this.auth.username).subscribe(user => {
    //   this.user = user;
    //   this.fetchRegistrationInfo();
    // }, error => {
    this.loading = false;
    // });
  }

  addRegPersonNote: any;
  totalPatient = 0;
  dataMAU: any;
  fetchRegistrationInfo() {
    this.totalPatient = 0;
    this.addRegPersonNote = null;
    this.loading = true;
    if (this.dataRegistration.screening_plan.category_code == 'CORPORATE') this.staffPatient = 'staff'; else this.staffPatient = 'patient';
    this.dataPatients = _.filter(this.dataRegistration.registration_persons, { 'person_type_code': 'PATIENT' });
    this.dataMAU = _.filter(this.dataRegistration.registration_persons, { 'admin_type': 'MAU', 'person_type_code': 'NON-PATIENT' });

    this.dataRegistration.screening_plan.package_patients.forEach(element => {
      this.totalPatient = this.totalPatient + Number(element.total_patient);
      if (this.addRegPersonNote != null)
        this.addRegPersonNote = this.addRegPersonNote + ' and ' + element.total_patient + " " + element.patient_type_code;
      else
        this.addRegPersonNote = element.total_patient + " " + element.patient_type_code;
    });
    this.isSubmitted = false;
    this.loading = false;
  }


  refreshRegistrationInfo() {
    this.dataService.getRegistrationInfo(this.dataRegistration.reg_no).subscribe(res => {
      this.dataRegistration = res;
      this.fetchRegistrationInfo();
    });
  }

  states: any;
  fetchStateInfo() {
    this.dataService.getStates().subscribe(res => {
      this.states = res.data;
    });
  }


  mode: any;
  dataForm: FormGroup;


  docUploadList: any;
  selectedRegPerson: any;
  dataCompanyForm: FormGroup;
  modeCompany: any;
  updateCompanyInfo(template: TemplateRef<any>) {
    if (this.dataRegistration.company != null) {
      this.dataCompanyForm = this.formBuilder.group(this.dataRegistration.company);
      this.dataCompanyForm.get("co_reg_no").setValidators(Validators.required);
      this.dataCompanyForm.get("name").setValidators(Validators.required);
      this.dataCompanyForm.get("address").setValidators(Validators.required);
      this.dataCompanyForm.get("contact_no").setValidators(Validators.required);
      this.dataCompanyForm.get("email").setValidators(Validators.required);
      this.dataCompanyForm.get("postcode").setValidators(Validators.required);
      this.modeCompany = 'EDIT';
    } else {
      this.dataCompanyForm = this.formBuilder.group({
        co_reg_no: [null, Validators.required],
        name: [null, Validators.required],
        address: [null, Validators.required],
        town: null,
        district: null,
        postcode: null,
        state: null,
        geocode: null,
        pic_url: null,
        contact_no: [null, Validators.required],
        email: [null, Validators.required]
      });
      this.modeCompany = 'NEW';
    }
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-l' });
  }

  isSubmittedCo = false;
  saveCompanyInfo() {
    this.isSubmittedCo = true;

    if (this.dataCompanyForm.invalid) {
      this.toastrService.warning("Please complete the Company Info details.", "Data incomplete");
      return;
    }

    this.coolDialogs.confirm("Are you sure to save this company info?")
      .subscribe(res => {
        if (res) {
          // console.log(this.dataCompanyForm.value);
          if (this.modeCompany == 'NEW') {
            this.dataService.saveNewCompanyInfo(this.dataCompanyForm.value).subscribe(res => {
              if (res.errorFound) {
                this.toastrService.error("Unable to save company infomation", "Failed");
              } else {
                let data = { "reg_no": this.dataRegistration.reg_no, "field_name": "company_reg_no", "field_value": this.dataCompanyForm.get('co_reg_no').value }
                this.dataService.updateRegField(data).subscribe(res2 => {
                  this.refreshRegistrationInfo();
                  this.modalRef.hide();
                }, error => {
                  console.log(error);
                  this.toastrService.warning(error.message, error.error);
                });
                this.toastrService.success("Company infomation has been successfully saved", "Success");
              }
            }, err => {
              this.toastrService.warning(err.error.message, err.error.error);
            });
          } else {
            this.dataService.saveUpdateCompanyInfo(this.dataCompanyForm.value).subscribe(res => {
              if (res.errorFound) {
                this.toastrService.error("Unable to save company infomation", "Failed");
              } else {
                let data = { "reg_no": this.dataRegistration.reg_no, "field_name": "company_reg_no", "field_value": this.dataCompanyForm.get('co_reg_no').value }
                this.dataService.updateRegField(data).subscribe(res2 => {
                  this.refreshRegistrationInfo();
                  this.modalRef.hide();
                }, error => {
                  console.log(error);
                  this.toastrService.warning(error.message, error.error);
                });
                this.toastrService.success("Company infomation has been successfully saved", "Success");
              }
            }, err => {
              this.toastrService.warning(err.error.message, err.error.error);
            });
          }

        }
      });
  }

  uploadedFiles: any[] = [];
  onUpload(event) {
    for (let file of event.files) {
      this.uploadedFiles.push(file);
    }
  }

  uploadCompanyDoc(template: TemplateRef<any>) {
    this.fetchCompanyDocData();
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-l' });
  }

  uploading = false;
  uploadNow(event, pfileUpload) {
    this.uploadedFiles = [];
    for (let file of event.files) {
      this.uploadedFiles.push(file);
    }
    // console.log("upload", this.uploadedFiles);
    this.coolDialogs.confirm("Are you sure to upload this file?")
      .subscribe(res => {
        if (res) {
          this.uploading = true;
          var formData = new FormData();
          formData.append('co_id', this.dataRegistration.company.co_id);
          formData.append('co_reg_no', this.dataRegistration.company.co_reg_no);
          formData.append('document_code', 'all');
          // Array.from(this.uploadedFiles).forEach(f => formData.append('fileToUpload', f));
          for (var i = 0; i < this.uploadedFiles.length; i++) {
            formData.append("fileToUpload[]", this.uploadedFiles[i]);
          }
          this.dataService.uploadCompanySupportingDoc(formData).subscribe(res => {
            if (res.errorFound) {
              this.toastrService.error(res.message, "Failed");
            } else {
              this.uploadedFiles = [];
              pfileUpload.clear();
              this.toastrService.success("Document has been successfully uploaded", "Success");
              this.fetchCompanyDocData();
            }
            this.uploading = false;
          }, error => {
            this.uploading = false;
          });
        }
      });
  }

  fetchCompanyDocData() {
    this.dataService.getCompanyDocument(this.dataRegistration.company.co_reg_no).subscribe(res => {
      this.dataRegistration.company.documents = res.data;
    });
  }

  // matchPersonWithUploadDocs(personDocuments) {
  //   this.docUploadList.forEach(doc => {
  //     doc.uploaded = [];
  //     let docUploaded = _.filter(personDocuments, { 'document_code': doc.code });
  //     doc.uploaded = docUploaded;
  //   });
  // }

  removeFile(companyDoc) {
    this.coolDialogs.confirm("Are you sure to remove " + companyDoc.ori_filename + "?")
      .subscribe(res => {
        this.dataService.removeCompanyDocument(companyDoc).subscribe(res => {
          if (res.errorFound) {
            this.toastrService.error("Sorry, there was an error removing your file.", "Failed");
          } else {
            this.fetchCompanyDocData();
            this.toastrService.success("The selected file has been removed", "Success");
          }
        });
      });
  }

  docUrl: any;
  printDoc(url) {
    this.docUrl = environment.uploadPath + url;
    console.log(this.docUrl);
    var proxyIframe = document.createElement('iframe');
    var body = document.getElementsByTagName('body')[0];
    body.appendChild(proxyIframe);
    proxyIframe.style.width = '100%';
    proxyIframe.style.height = '100%';
    proxyIframe.style.display = 'none';

    var contentWindow = proxyIframe.contentWindow;
    contentWindow.document.open();
    contentWindow.document.write('<iframe src="' + this.docUrl + '" onload="print();" width="1000" height="1800" frameborder="0" marginheight="0" marginwidth="0">');
    contentWindow.document.close();
  }

  isSubmitted = false;


  titleCaseWord(word: string) {
    if (!word) return word;
    return word[0].toUpperCase() + word.substr(1).toLowerCase();
  }




}
