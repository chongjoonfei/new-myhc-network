<?php
    include_once '../config/db.php';
    include_once '../objects/v1/booking.php';

    function fetch_data()
    {
        // get database connection
        $database = new Database();
        $db = $database->getConnection();
        
        // prepare object
        $booking = new Booking($db);
        $booking->booking_no = isset($_GET['bNo']) ? $_GET['bNo'] : die();
        $result = $booking->readByBookingNoForTestForm();

        $center = '';
        if ($result['screening_location']!=='HOME') {
            $center=$result['screening_center']['sc_name'];
        }else{
            $center = 'HOME VISIT<br>'.$result['person']['address'] 
                        .'<br>'.$result['person']['town'].'<br>'.$result['person']['postcode'].' '.$result['person']['district']
                        .'<br>'.$result['person']['state'];
        }

        $output ='';

        $sizeOfPatients = sizeof($result['patients']);               
        for($pt = 0; $pt < $sizeOfPatients; $pt++) {
            $gender='Male';
            if ($result['patients'][$pt]['person']['gender']=='F') $gender='Female';
             
            $output .= '
                <link rel="stylesheet" href="bootstrap/bootstrap.min.css" />
                <script src="bootstrap/bootstrap.min.js"></script>
            
                <style>
                    body {
                    font-family: Helvetica, sans-serif;
                    }
                </style>

                <p align="center"><img src="images/header_title.jpg" width="100%" ></p>
                <table style="width:100%">
                    <tr>
                        <td>
                            <div style="border: 1px solid #000000; background-color: #ecf4d3;text-align: center;font-weight:bold">  
                                PATIENT AND BOOKING DETAILS 
                            </div>
                            <!--TABLE UNTUK PATIENT DETAILS -REQUIRED INPUT FROM SYSTEM--->
                            <table style="height: 55px; width: 100%; border-collapse: collapse;" border="1" cellpadding="6">
                                <tbody>
                                <tr style="height: 40px;">
                                    <td style="width: 33%; height: 35px; border-style: solid; border-color: #000000; vertical-align:top;padding:0px 5px 0px 5px">
                                    Name:<br><strong>'.$result['patients'][$pt]['person']['name'].'</strong></td>
                                    <td style="width: 20%; height: 37px; border-style: solid; border-color: #000000; vertical-align:top;padding:0px 5px 0px 5px"
                                    rowspan="2">
                                        Contact No:
                                        <br><strong>+60'.$result['patients'][$pt]['person']['mobile_no'].'</strong>
                                        <br>Date of Birth :
                                        <br><strong>N/A</strong>
                                        <br>Age:
                                        <br><strong>'.$result['patients'][$pt]['person']['age'].'</strong>
                                        <br>Gender :
                                        <br><strong>'.$gender.'</strong>
                                    </td>
                                    <td style="width: 20%; height: 37px; vertical-align:top;padding:0px 5px 0px 5px" rowspan="2">Clinic / <br />Centre
                                        Name:<br><strong>'.$center.'</strong></td>
                                    <td
                                    style="width: 20%; height: 55px; border-style: solid; border-color: #000000; text-align: center; vertical-align: top;"
                                    rowspan="3">Patient QR Code</td>
                                </tr>
                                <tr style="height: 18px;">
                                    <td style="width: 25%; height: 10px;vertical-align:top;padding:0px 5px 0px 5px">
                                        NRIC/Passport No:
                                        <br><strong>'.$result['patients'][$pt]['person']['ic_no'].'</strong>
                                        <br>MyHC ID:
                                        <br><strong>'.$result['userAccount']['username'].' (MAU)</strong>
                                    </td>
                                </tr>
                                <tr style="height: 18px;">
                                    <td style="width: 25%; height: 18px; vertical-align: top; border:1;vertical-align:top;padding:0px 5px 0px 5px">
                                        Doctor Requesting:
                                        <br><strong>&nbsp;</strong>
                                    </td>
                                    <td style="width: 25%; height: 18px; vertical-align:top;padding:0px 5px 0px 5px">
                                        Doctor Code:
                                        <br><strong>&nbsp;</strong>
                                    </td>
                                    <td style="width: 25%; height: 18px; vertical-align:top;padding:0px 5px 0px 5px">
                                        Date of Test :
                                        <br><strong>'.$result['screening_date'].'</strong>
                                        <br>Time Collected :
                                        <br><strong>'.$result['screening_time'].'</strong>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="border: 1px solid #000000; background-color: #ecf4d3;text-align: center;font-weight:bold">  
                                TESTING SPECIMEN DETAILS
                            </div>
                            <table style="height: 51px; width: 100%; border-collapse: collapse;" border="1" cellpadding="6">
                                <tbody>
                                <tr>
                                    <td style="width: 75%;padding:0px 5px 5px 12px;vertical-align: top;">
                                        <b>Test Package / Code:</b>&nbsp;'.$result['products'][0]['name'].'
                                        <table
                                            style="width: 100%; border-collapse: collapse; border-style: none; margin-left: auto; margin-right: auto;"
                                            border="1">
                                            <tbody>
                                            <tr>
                                                <td style="width: 76.3865%; border-style: none;">
                                                <table style="height: 153px; width: 100%; border-collapse: collapse; border-style: none;" border="1">
                                                    <tbody>
                                                    <tr style="height: 17px;">
                                                        <td
                                                        style="width: 50%; height: 17px; background-color: #ecf4d3; border-color: #000000; text-align: center;">
                                                        <b>Panel</b></td>
                                                        <td
                                                        style="width: 50%; height: 17px; background-color: #ecf4d3; border-color: #000000; text-align: center;">
                                                        <b>Panel Code</b></td>
                                                    </tr>';

                $testPanels = $result['products'][0]['testGroup']['test_panels'];
                $sizeOfTestPanel = sizeof($testPanels);
                for($tp = 0; $tp < $sizeOfTestPanel; $tp++) {

                    $output .= '
                                                    <tr style="height: 17px;">
                                                        <td style="width: 50%; height: 17px;padding-left:5px">'.$testPanels[$tp]['name'].'</td>
                                                        <td style="width: 50%; height: 17px;;padding-left:5px"'.$testPanels[$tp]['test_panel_code'].'</td>
                                                    </tr>';
                }

                for ($c=$sizeOfTestPanel; $c <10; $c++){
                    
                    $output .= '
                                                    <tr style="height: 17px;">
                                                        <td style="width: 50%; height: 17px;">&nbsp;</td>
                                                        <td style="width: 50%; height: 17px;">&nbsp;</td>
                                                    </tr>';
                }

                $output .= '                       </tbody>
                                                </table>
                                                </td>
                                                <td style="width: 23.6135%; border-style: none;">
                                                <table
                                                    style="height: 153px; width: 82.5581%; margin-left: auto; margin-right: auto;"
                                                    border="1">
                                                    <tbody>
                                                    <tr style="height: 17px;">
                                                        <td
                                                        style="width: 100%; height: 17px; border-style: solid; background-color: #ecf4d3; border-color: #000000; text-align: center;">
                                                        <b>Check</b></td>
                                                    </tr>';

                for ($c=0; $c <10; $c++){  
                    $output .= '                                  
                                                        <tr style="height: 17px;">
                                                            <td style="width: 100%; height: 17px;">&nbsp;</td>
                                                        </tr>';
                }

                $output .= '                                    
                                                        </tbody>
                                                    </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table> 
                                        </td>
                                        <td style="width: 25.2669%; vertical-align: top;">
                                        <p style="text-align: center;">Test QR Code
                                            <br>&nbsp;
                                        <p style="text-align: center;">&nbsp;
                                        <p style="text-align: center;"><br /><br><br><br><br><br>Test ID:
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table style="width: 100%; border-collapse: collapse;" border="1" cellpadding="6">
                                    <tbody>
                                    <tr>
                                        <td style="width: 75%;padding:5px 5px 5px 12px">
                                        <b>Vital Signs Data Collection<br>
                                            This is to be added using the rapid test kits.</b>
                                            <table
                                                style="width: 100%;   border-style: none; margin-left: auto; margin-right: auto;"
                                                border="1">
                                                <tbody>
                                                <tr>
                                                    <td style="width: 79.1691%; border-style: none;padding:5px">
                                                        <table style="height: 153px; width: 101.411%; border-collapse: collapse; border-style: none;"
                                                            border="1">
                                                            <tbody>
                                                            <tr style="height: 17px;">
                                                                <td
                                                                style="width: 69.6004%; height: 17px; background-color: #ecf4d3; border-color: #000000; text-align: center;">
                                                                <b>Marker</b></td>
                                                                <td
                                                                style="width: 31.7211%; height: 17px; background-color: #ecf4d3; border-color: #000000; text-align: center;"
                                                                colspan="2"><b>Reading</b></td>
                                                            </tr>
                                                            <tr style="height: 17px;">
                                                                <td style="width: 69.6004%; height: 17px;padding-left:5px">Height (cm) &ndash; <em>format 162.0cm</em></td>
                                                                <td style="width: 15.6359%; height: 17px;">&nbsp;</td>
                                                                <td style="width: 16.0852%;">&nbsp;</td>
                                                            </tr>
                                                            <tr style="height: 17px;">
                                                                <td style="width: 69.6004%; height: 17px;padding-left:5px">Weight (kg) &ndash; <em>format 90.0 kg</em></td>
                                                                <td style="width: 15.6359%; height: 17px;">&nbsp;</td>
                                                                <td style="width: 16.0852%;">&nbsp;</td>
                                                            </tr>
                                                            <tr style="height: 17px;">
                                                                <td style="width: 69.6004%; height: 17px;padding-left:5px" rowspan="2">Blood Pressure (mmHG)<br />&ndash;
                                                                <em>format Diastolic/Systolic 123 mmHG</em></td>
                                                                <td style="width: 15.6359%; height: 17px;padding-left:5px">Systolic</td>
                                                                <td style="width: 16.0852%;">&nbsp;</td>
                                                            </tr>
                                                            <tr style="height: 17px;">
                                                                <td style="width: 15.6359%; height: 17px;padding-left:5px">Diastolic</td>
                                                                <td style="width: 16.0852%;">&nbsp;</td>
                                                            </tr>
                                                            <tr style="height: 17px;">
                                                                <td style="width: 69.6004%; height: 17px;padding-left:5px">Blood Sugar (mmol/L) &ndash; <em>format 6.5
                                                                    mmol/L</em></td>
                                                                <td style="width: 15.6359%; height: 17px;">&nbsp;</td>
                                                                <td style="width: 16.0852%;">&nbsp;</td>
                                                            </tr>
                                                            <tr style="height: 17px;">
                                                                <td style="width: 69.6004%; height: 17px;padding-left:5px">Blood Cholesterol (mmol/L) &ndash; <em>format 4.53
                                                                    mmol/L</em></td>
                                                                <td style="width: 15.6359%; height: 17px;">&nbsp;</td>
                                                                <td style="width: 16.0852%;">&nbsp;</td>
                                                            </tr>
                                                            <tr style="height: 17px;">
                                                                <td style="width: 69.6004%; height: 17px;padding-left:5px">Pulse rate (beats/min) &ndash; <em>format 63
                                                                    beats/min</em></td>
                                                                <td style="width: 15.6359%; height: 17px;">&nbsp;</td>
                                                                <td style="width: 16.0852%;">&nbsp;</td>
                                                            </tr>
                                                            <tr style="height: 17px;">
                                                                <td style="width: 69.6004%; height: 17px;padding-left:5px">Oxygen Saturation (%) &ndash; <em>format 98%</em>
                                                                </td>
                                                                <td style="width: 15.6359%; height: 17px;"><em>&nbsp;</em></td>
                                                                <td style="width: 16.0852%;">&nbsp;</td>
                                                            </tr>
                                                            <tr style="height: 17px;">
                                                                <td style="width: 69.6004%; height: 17px;padding-left:5px">Temperature  &ndash; <em>format 36.5<span>&deg;</span>C</em>
                                                                </td>
                                                                <td style="width: 15.6359%; height: 17px;"><em>&nbsp;</em></td>
                                                                <td style="width: 16.0852%;">&nbsp;</td>
                                                            </tr>
                                                            <tr style="height: 17px;">
                                                                <td style="width: 69.6004%; height: 17px;padding-left:5px">Body Mass Index (BMI)</em>
                                                                </td>
                                                                <td style="width: 15.6359%; height: 17px;"><em>&nbsp;</em></td>
                                                                <td style="width: 16.0852%;">&nbsp;</td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                    <td style="width: 20.8309%; border-style: none; vertical-align: top;padding:5px">
                                                        <table
                                                            style="height: 196px; width: 67.5475%; border-collapse: collapse; margin-left: auto; margin-right: auto;"
                                                            border="1">
                                                            <tbody>
                                                            <tr style="height: 17px;">
                                                                <td
                                                                style="width: 100%; height: 17px; border-style: solid; background-color: #ecf4d3; border-color: #000000; text-align: center;">
                                                                <b>Check</b></td>
                                                            </tr>
                                                            <tr style="height: 17px;">
                                                                <td style="width: 100%; height: 17px;">&nbsp;</td>
                                                            </tr>
                                                            <tr style="height: 17px;">
                                                                <td style="width: 100%; height: 17px;">&nbsp;</td>
                                                            </tr>
                                                            <tr style="height: 17px;">
                                                                <td style="width: 100%; height: 17px;">&nbsp;</td>
                                                            </tr>
                                                            <tr style="height: 17px;">
                                                                <td style="width: 100%; height: 17px;">&nbsp;</td>
                                                            </tr>
                                                            <tr style="height: 17px;">
                                                                <td style="width: 100%; height: 17px;">&nbsp;</td>
                                                            </tr>
                                                            <tr style="height: 17px;">
                                                                <td style="width: 100%; height: 17px;">&nbsp;</td>
                                                            </tr>
                                                            <tr style="height: 17px;">
                                                                <td style="width: 100%; height: 17px;">&nbsp;</td>
                                                            </tr>
                                                            <tr style="height: 17px;">
                                                                <td style="width: 100%; height: 17px;">&nbsp;</td>
                                                            </tr>
                                                            <tr style="height: 17px;">
                                                                <td style="width: 100%; height: 17px;">&nbsp;</td>
                                                            </tr>
                                                            <tr style="height: 17px;">
                                                                <td style="width: 100%; height: 17px;">&nbsp;</td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            <!--TABLE UNTUK COVID-19 TEST--->
                            <table style="width: 100%; " border="1" cellpadding="6">
                              <tbody>
                                <tr>
                                  <td style="width: 100%;padding:5px">
                                    <br><b>COVID-19 Rapid Test Kit</b>
                                    <table style="width: 100%; border-collapse: collapse; border-style: solid;" border="1" cellpadding="6">
                                      <tbody>
                                        <tr>
                                          <td style="width: 20.8509%;padding:5px">Rapid Test Result</td>
                                          <td style="width: 13.2857%;padding:5px; background-color: #ecf4d3; text-align: center;">Positive</td>
                                          <td style="width: 13.0683%;padding:5px">&nbsp;</td>
                                          <td style="width: 13.2857%;padding:5px; background-color: #ecf4d3; text-align: center;">Negative</td>
                                          <td style="width: 13.0683%;padding:5px">&nbsp;</td>
                                          <td style="width: 13.2857%;padding:5px; background-color: #ecf4d3; border-style: solid; text-align: center;">Invalid
                                          </td>
                                          <td style="width: 13.0683%;">&nbsp;</td>
                                        </tr>
                                      </tbody>
                                    </table><br><br>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            <!--2ND PAGE NO REQUIRED INPUT FROM SISTEM--->  
                                <div style="page-break-before:always;">
                                    <p align=center><img src="images/testspecimen_2ndpage.png" alt="Test Specimen Booking Form 2nd page" width="100%" height="1020px"></p>
                                </div> 
                            </td>
                        </tr>
                    </table>
            ';
        }
        return $output;
    }

    // Include autoloader 
    require_once 'dompdf/autoload.inc.php'; 
    
    // Reference the Dompdf namespace 
    use Dompdf\Dompdf; 
    
    // Instantiate and use the dompdf class 
    $dompdf = new Dompdf();

    //    $file_name = md5(rand()) . '.pdf';
    $html_code = fetch_data();

    // Load HTML content 
    $dompdf->loadHtml($html_code); 
    
    // (Optional) Setup the paper size and orientation 
    $dompdf->setPaper('A4', 'potrait'); 
    
    // Render the HTML as PDF 
    $dompdf->render(); 

    ob_end_clean();
    
    // Output the generated PDF (1 = download and 0 = preview) 
    $dompdf->stream("codexworld", array("Attachment" => 0));
  
 
 
?>