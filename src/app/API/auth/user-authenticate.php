<?php
/**
* Author : https://www.roytuts.com
* Edited by : Majina
*/

ini_set('display_errors', 1); 
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
mysqli_report(MYSQLI_REPORT_ALL);


require_once '../config/db.php';
require_once 'jwt_utils.php';
require_once '../objects/v1/user-account.php';


header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");


if ($_SERVER['REQUEST_METHOD'] === 'POST') {

	// get posted data
	$data = json_decode(file_get_contents("php://input", true));

	

	$dbService = new Database();
	$conn = $dbService->getConnection();

	$userAccount = new UserAccount($conn);

	 
	$query = "SELECT * FROM user_account  
		WHERE email = :email ";

	$stmt = $conn->prepare($query);
 	$stmt->bindParam(':email', $data->email);
	$encryptPwd = $userAccount->encryptPassword($data->password);

	$stmt->execute();
	$row = $stmt->fetch(PDO::FETCH_ASSOC);

	$dbPassword = $row['password'];
	$dbIcNo = $row['ic_no'];
	$dbUsername = $row['username'];

	if ($dbPassword==null || !$row) {
		http_response_code(401);
		echo json_encode(array('message' => 'Invalid Email','error'=>'Login failed'));
	} else {
		$isAccountBlocked = false;
		if (!($row['acc_type_code'] =='ADMIN' || $row['acc_type_code'] =='EXT-SOURCE')){
			$query = "SELECT * FROM pre_registration r, registration_person p   
			WHERE r.reg_no = p.reg_no and p.ic_no = :ic_no and status<> 'BLOCKED'";
	
			$stmt2 = $conn->prepare($query);
			$stmt2->bindParam(':ic_no', $dbIcNo);
		
			$stmt2->execute();
			$row2 = $stmt2->fetch(PDO::FETCH_ASSOC);

			if  (!isset($row2['status'])) {
				$isAccountBlocked = true;
				http_response_code(401);
				echo json_encode(array('message' => 'Your account has been blocked.' ,'error'=>'Access denied'));
			}	
		}

		if (!$isAccountBlocked) {
			if (hash_equals($dbPassword,$encryptPwd)){
				$expire_at = time() + 60;
				$row = $stmt->fetch(PDO::FETCH_ASSOC);
				$email = $data->email;
				$last_login = date('Y-m-d H:i:s');

	 
				$query = "update user_account set last_login =:last_login
				WHERE email = :email ";
				$stmt = $conn->prepare($query);
				$stmt->bindParam(':email', $data->email);
				$stmt->bindParam(':last_login', $last_login);
				$stmt->execute();

				$userAccount->email = $email;
				$userAccountRec = $userAccount->readByEmail($email);

				$headers = array('alg'=>'HS256','typ'=>'JWT');
				$payload = array('email'=>$email, 'exp'=>$expire_at);
		
				$jwt = generate_jwt($headers, $payload);
		
				http_response_code(200);
				echo json_encode(array('access_token' => $jwt,'username'=>$dbUsername, 'account'=> $userAccountRec, 'expire_at'=>$expire_at));
			}else{
				http_response_code(401);
				echo json_encode(array('message' => 'Invalid Password ' ,'error'=>'Login failed'));
		
			}
		}


	}

}
?>