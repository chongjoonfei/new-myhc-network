<?php

include_once 'test-reference-range.php';

class TestMarker{
  
    // database connection and table name
    private $conn;
    private $table_name = "test_marker";
	private $testReferenceRange;

    // object properties
    public $test_panel_code;
    public $code;
    public $name;
	public $description;
	public $unit;
	public $data_format;
	 
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
		
    }

    // read all records
	function readAll(){
	  
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				ORDER BY
					test_panel_code,code";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	  
		// execute query
		$stmt->execute();
	  
		return $stmt;
	}

	function readByPanelCode($test_panel_code){
		$testReferenceRange = new TestReferenceRange($this->conn);

		// select all query
		$query = "SELECT * FROM " . $this->table_name . "  where test_panel_code=:test_panel_code ORDER BY code";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind code of data to be updated
		$stmt->bindParam(":test_panel_code", $test_panel_code);
	  
		// execute query
		$stmt->execute();

		$arr=array();

		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			extract($row);
			$testMarker_item=array(
				"test_panel_code" => $test_panel_code,
				"code" => $code,
				"name" => $name,
				"description" => $description,
				"unit" => $unit,
				"data_format"=> $data_format,
				"test_reference_range" => $testReferenceRange->readByTestMarkerCode($code)  
			);
			array_push($arr, $testMarker_item);
		}
	  
		return $arr;

	}



	// used when filling up the update record form
	function readOne(){

		$this->code=htmlspecialchars(strip_tags($this->code));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					code = :code
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":code", $this->code);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
		$this->test_panel_code = $row['test_panel_code'];
		$this->code = $row['code'];
		$this->name = $row['name']; 
		$this->description = $row['description'];
		$this->unit = $row['unit'];
		$this->data_format = $row['data_format'];
	}

	// create object
	function create(){
	  
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
				test_panel_code=:test_panel_code, code=:code, name=:name, 
				description=:description, unit=:unit, data_format=:data_format";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->test_panel_code=htmlspecialchars(strip_tags($this->test_panel_code));
		$this->code=htmlspecialchars(strip_tags($this->code));
		$this->name=htmlspecialchars(strip_tags($this->name));
		$this->description=htmlspecialchars(strip_tags($this->description));
		$this->unit=htmlspecialchars(strip_tags($this->unit));
		$this->data_format=htmlspecialchars(strip_tags($this->data_format));
		
		// bind values
		$stmt->bindParam(":test_panel_code", $this->test_panel_code);
		$stmt->bindParam(":code", $this->code);
		$stmt->bindParam(":name", $this->name);
		$stmt->bindParam(":description", $this->description);
		$stmt->bindParam(":unit", $this->unit);
		$stmt->bindParam(":data_format", $this->data_format);
		
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}	
	
	// update the record
	function update(){
	
		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
						name = :name,
						description = :description,
						unit = :unit,
						data_format = :data_format
					WHERE
						code = :code";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
			$this->code=htmlspecialchars(strip_tags($this->code));
			$this->name=htmlspecialchars(strip_tags($this->name));
			$this->description=htmlspecialchars(strip_tags($this->description));
			$this->unit=htmlspecialchars(strip_tags($this->unit));
			$this->data_format=htmlspecialchars(strip_tags($this->data_format));
			
			// bind values
			$stmt->bindParam(":code", $this->code);
			$stmt->bindParam(":name", $this->name);
			$stmt->bindParam(":description", $this->description);
			$stmt->bindParam(":unit", $this->unit);
			$stmt->bindParam(":data_format", $this->data_format);
			
			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}

	function delete(){

		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE code = ?";

		// prepare query
		$stmt = $this->conn->prepare($query);

		// sanitize
		$this->code=htmlspecialchars(strip_tags($this->code));

		// bind id of record to delete
		$stmt->bindParam(1, $this->code);

		// execute query
		if($stmt->execute()){
			return true;
		}
		return false;

	}
	
}

?>