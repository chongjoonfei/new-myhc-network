<?php

include_once 'test-group.php';

class BookingProduct{
  
    // database connection and table name
    private $conn;
    private $table_name = "booking_product";

    // object properties
	public $uid;
	public $booking_id;
    public $patient_type;
	public $code;
	public $name;
	public $price;
	public $quantity;
	public $total_price;
	public $remark;
	public $ref_no;
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
		
    }

	// used when filling up the update record form
	function readOne(){

		$this->booking_id=htmlspecialchars(strip_tags($this->booking_id));
		$this->code=htmlspecialchars(strip_tags($this->code));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					booking_id = :booking_id
					and code = :code
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":booking_id", $this->booking_id);
		$stmt->bindParam(":code", $this->code);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
		$this->booking_id = $row['booking_id'];
		$this->patient_type = $row['patient_type'];
		$this->code = $row['code'];
		$this->name = $row['name'];
		$this->price = $row['price'];
		$this->quantity = $row['quantity'];
		$this->total_price = $row['total_price'];
		$this->remark = $row['remark'];
		$this->ref_no = $row['ref_no'];

	}

 	// used when read record by Booking No
	 function readByBookingId($booking_id){

		$this->booking_id=htmlspecialchars(strip_tags($booking_id));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					booking_id = :booking_id
				order by uid
				 ";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":booking_id", $this->booking_id);
		
		// execute query
		$stmt->execute();
		
	
		$record_item = null;
		$arr=array();

		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			extract($row);
			$record_item=array(
				"uid" => $uid,
				"booking_id" => $booking_id,
				"patient_type" => $patient_type,
				"code" => $code,
				"name" => $name,
				"price" => $price,
				"quantity" => $quantity,
				"total_price" => $total_price,
				"remark" => $remark,
				"ref_no" => $ref_no
			);
			array_push($arr, $record_item);
		}
	  
		return $arr;

	}

	// used when read record by Booking No
	function readByRefNo($ref_no){

		$this->ref_no=htmlspecialchars(strip_tags($ref_no));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					ref_no = :ref_no
				order by uid
					";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":ref_no", $this->ref_no);
		
		// execute query
		$stmt->execute();
		
	
		$record_item = null;
		$arr=array();

		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			extract($row);
			$record_item=array(
				"uid" => $uid,
				"booking_id" => $booking_id,
				"patient_type" => $patient_type,
				"code" => $code,
				"name" => $name,
				"price" => $price,
				"quantity" => $quantity,
				"total_price" => $total_price,
				"remark" => $remark,
				"ref_no" => $ref_no
			);
			array_push($arr, $record_item);
		}
		
		return $arr;

	}

	// used when read record by Booking No
	function readByBookingIdForTestForm($booking_id){

		$testGroup = new TestGroup($this->conn);

		$this->booking_id=htmlspecialchars(strip_tags($booking_id));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . " 
				WHERE
					booking_id = :booking_id
					and remark='AOT'
				order by uid
					";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":booking_id", $this->booking_id);
		
		// execute query
		$stmt->execute();
		
	
		$record_item = null;
		$arr=array();

		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			extract($row);
			$record_item=array(
				"uid" => $uid,
				"booking_id" => $booking_id,
				"patient_type" => $patient_type,
				"code" => $code,
				"name" => $name,
				"price" => $price,
				"quantity" => $quantity,
				"total_price" => $total_price,
				"remark" => $remark,
				"ref_no" => $ref_no,
				"testGroup" => $testGroup->readByTestGroupCode($code)
			);
			array_push($arr, $record_item);
		}
		
		return $arr;

	}

	// create object
	function create(){
	  
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
					booking_id=:booking_id, 
					patient_type=:patient_type, 
					code=:code,
					name=:name, 
					price=:price,
					quantity=:quantity, 
					total_price=:total_price,
					remark=:remark,
					ref_no=:ref_no ";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->booking_id=htmlspecialchars(strip_tags($this->booking_id));
		$this->code=htmlspecialchars(strip_tags($this->code));
		$this->patient_type=htmlspecialchars(strip_tags($this->patient_type));
		$this->price=htmlspecialchars(strip_tags($this->price));
		$this->name=htmlspecialchars(strip_tags($this->name));
		$this->quantity=htmlspecialchars(strip_tags($this->quantity));
		$this->total_price=htmlspecialchars(strip_tags($this->total_price));
		$this->remark=htmlspecialchars(strip_tags($this->remark));
		$this->ref_no=htmlspecialchars(strip_tags($this->ref_no));
		
		// bind values
		$stmt->bindParam(":booking_id", $this->booking_id);
		$stmt->bindParam(":patient_type", $this->patient_type);
		$stmt->bindParam(":code", $this->code);
		$stmt->bindParam(":price", $this->price);
		$stmt->bindParam(":name", $this->name);
		$stmt->bindParam(":quantity", $this->quantity);
		$stmt->bindParam(":total_price", $this->total_price);
		$stmt->bindParam(":remark", $this->remark);
		$stmt->bindParam(":ref_no", $this->ref_no);
		
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}	
	
	// update the record
	function update(){
	
		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
						patient_type=:patient_type,
						price=:price, name=:name,
						quantity=:quantity, total_price=:total_price,
						remark=:remark , ref_no=:ref_no   
					WHERE
						booking_id=:booking_id and code = :code";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
			$this->booking_id=htmlspecialchars(strip_tags($this->booking_id));
			$this->patient_type=htmlspecialchars(strip_tags($this->patient_type));
			$this->code=htmlspecialchars(strip_tags($this->code));
			$this->price=htmlspecialchars(strip_tags($this->price));
			$this->name=htmlspecialchars(strip_tags($this->name));
			$this->quantity=htmlspecialchars(strip_tags($this->quantity));
			$this->total_price=htmlspecialchars(strip_tags($this->total_price));
			$this->remark=htmlspecialchars(strip_tags($this->remark));
			$this->ref_no=htmlspecialchars(strip_tags($this->ref_no));
			
			// bind values
			$stmt->bindParam(":patient_type", $this->patient_type);
			$stmt->bindParam(":booking_id", $this->booking_id);
			$stmt->bindParam(":code", $this->code);
			$stmt->bindParam(":price", $this->price);
			$stmt->bindParam(":name", $this->name);
			$stmt->bindParam(":quantity", $this->quantity);
			$stmt->bindParam(":total_price", $this->total_price);
			$stmt->bindParam(":remark", $this->remark);
			$stmt->bindParam(":ref_no", $this->ref_no);
			
			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}

	function delete(){

		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE booking_id = ? and code = ?";

		// prepare query
		$stmt = $this->conn->prepare($query);

		// sanitize
		$this->booking_id=htmlspecialchars(strip_tags($this->booking_id));
		$this->code=htmlspecialchars(strip_tags($this->code));

		// bind id of record to delete
		$stmt->bindParam(1, $this->booking_id);
		$stmt->bindParam(2, $this->code);

		// execute query
		if($stmt->execute()){
			return true;
		}
		return false;

	}

 
	

}

?>