<?php
 ini_set('display_errors', 1); 
 ini_set('display_startup_errors', 1);
 error_reporting(E_ALL);
 mysqli_report(MYSQLI_REPORT_ALL);

 include_once 'person-document.php';
 include_once 'user-account.php';

 class Person{
  
    // database connection and table name
    private $conn;
    private $table_name = "person";

    // object properties
	public $ic_no;
	public $name;
	public $age;
	public $email;
	public $mobile_no;
	public $gender;
	public $patient_type_code;
	public $address;
	public $town;
	public $district;
	public $postcode;
	public $photo_path;
	public $relationship;

	//for updating registration person -> patient_type_code
	public $reg_no;
 
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
		
    }

    // read all records
	function readAll(){
	  
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				ORDER BY
					ic_no";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	  
		// execute query
		$stmt->execute();
	  
		return $stmt;
	}


	// used when filling up the update record form
	function readOne(){

		$this->ic_no=htmlspecialchars(strip_tags($this->ic_no));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					ic_no = :ic_no
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":ic_no", $this->ic_no);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
		// $this->reg_no = $row['reg_no'];
		$this->name = $row['name'];
		$this->ic_no = $row['ic_no'];
		$this->age = $row['age'];
		$this->email = $row['email'];
		$this->mobile_no = $row['mobile_no'];
		$this->gender = $row['gender'];
		$this->patient_type_code = $row['patient_type_code'];
		$this->address = $row['address'];
		$this->town = $row['town'];
		$this->district = $row['district'];
		$this->postcode = $row['postcode'];
		$this->state = $row['state'];
		$this->photo_path = $row['photo_path'];
		$this->relationship = $row['relationship'];
	}


	function readByIcNo($ic_no){

		$personDocument = new PersonDocument($this->conn);
		

		// select all query
		$query = "SELECT * FROM " . $this->table_name . "  where ic_no=:ic_no";

		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind code of data to be updated
		$stmt->bindParam(":ic_no", $ic_no);
		
		// execute query
		$stmt->execute();

		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		$person_item=array(
			"ic_no" => $row['ic_no'],
			"name" => $row['name'], 
			"mobile_no" => $row['mobile_no'], 
			"email" => $row['email'], 
			"age" => $row['age'], 
			"gender" => $row['gender'], 
			"address" => $row['address'], 
			"town" => $row['town'], 
			"district" => $row['district'], 
			"postcode" => $row['postcode'], 
			"state" => $row['state'] ,
			"photo_path" => $row['photo_path'] ,
			"relationship" => $row['relationship'] ,
			"documents" => $personDocument->readByIcNo($row['ic_no'])
		);
		
		return $person_item;
	}


	function readOneByUsername($username){

		// select all query
		$query = "SELECT p.* FROM person p, user_account u 
		where p.ic_no=u.ic_no and u.username=:username";

		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind code of data to be updated
		$stmt->bindParam(":username", $username);
		
		// execute query
		$stmt->execute();

		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		$this->name = $row['name'];
		$this->ic_no = $row['ic_no'];
		$this->age = $row['age'];
		$this->email = $row['email'];
		$this->mobile_no = $row['mobile_no'];
		$this->gender = $row['gender'];
		$this->patient_type_code = $row['patient_type_code'];
		$this->address = $row['address'];
		$this->town = $row['town'];
		$this->district = $row['district'];
		$this->postcode = $row['postcode'];
		$this->state = $row['state'];
		$this->state = $row['photo_path'];
		$this->relationship = $row['relationship'];
	}

	// create object
	function create(){
	  
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
				ic_no=:ic_no, name=:name,
				age=:age, email=:email, mobile_no=:mobile_no, gender=:gender,
				patient_type_code=:patient_type_code, address=:address, town=:town,
				district=:district, postcode=:postcode, state=:state, relationship=:relationship ";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		// $this->reg_no=htmlspecialchars(strip_tags($this->reg_no));
		$this->ic_no=htmlspecialchars(strip_tags($this->ic_no));
		$this->name=htmlspecialchars(strip_tags($this->name));
		$this->age=htmlspecialchars(strip_tags($this->age));
		$this->email=htmlspecialchars(strip_tags($this->email));
		$this->mobile_no=htmlspecialchars(strip_tags($this->mobile_no));
		$this->gender=htmlspecialchars(strip_tags($this->gender));
		$this->patient_type_code=htmlspecialchars(strip_tags($this->patient_type_code));
		$this->address=htmlspecialchars(strip_tags($this->address));
		$this->town=htmlspecialchars(strip_tags($this->town));
		$this->district=htmlspecialchars(strip_tags($this->district));
		$this->postcode=htmlspecialchars(strip_tags($this->postcode));
		$this->state=htmlspecialchars(strip_tags($this->state));
		$this->relationship=htmlspecialchars(strip_tags($this->relationship));
		
		// bind values
		// $stmt->bindParam(":reg_no", $this->reg_no);
		$stmt->bindParam(":ic_no", $this->ic_no);
		$stmt->bindParam(":name", $this->name);
		$stmt->bindParam(":age", $this->age);
		$stmt->bindParam(":email", $this->email);
		$stmt->bindParam(":mobile_no", $this->mobile_no);
		$stmt->bindParam(":gender", $this->gender);
		$stmt->bindParam(":patient_type_code", $this->patient_type_code);
		$stmt->bindParam(":address", $this->address);
		$stmt->bindParam(":town", $this->town);
		$stmt->bindParam(":district", $this->district);
		$stmt->bindParam(":postcode", $this->postcode);
		$stmt->bindParam(":state", $this->state);
		$stmt->bindParam(":relationship", $this->relationship);
		
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}	
	
	// update the record
	function update(){
	
		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
						name=:name,
						age=:age, email=:email, mobile_no=:mobile_no, gender=:gender,
						patient_type_code=:patient_type_code, address=:address, town=:town,
						district=:district, postcode=:postcode, state=:state , 
						photo_path= :photo_path, relationship= :relationship
					WHERE
						ic_no = :ic_no";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
			$this->ic_no=htmlspecialchars(strip_tags($this->ic_no));
			$this->name=htmlspecialchars(strip_tags($this->name));
			$this->age=htmlspecialchars(strip_tags($this->age));
			$this->email=htmlspecialchars(strip_tags($this->email));
			$this->mobile_no=htmlspecialchars(strip_tags($this->mobile_no));
			$this->gender=htmlspecialchars(strip_tags($this->gender));
			$this->patient_type_code=htmlspecialchars(strip_tags($this->patient_type_code));
			$this->address=htmlspecialchars(strip_tags($this->address));
			$this->town=htmlspecialchars(strip_tags($this->town));
			$this->district=htmlspecialchars(strip_tags($this->district));
			$this->postcode=htmlspecialchars(strip_tags($this->postcode));
			$this->state=htmlspecialchars(strip_tags($this->state));
			$this->photo_path=htmlspecialchars(strip_tags($this->photo_path));
			$this->relationship=htmlspecialchars(strip_tags($this->relationship));
			
			// bind values
			$stmt->bindParam(":ic_no", $this->ic_no);
			$stmt->bindParam(":name", $this->name);
			$stmt->bindParam(":age", $this->age);
			$stmt->bindParam(":email", $this->email);
			$stmt->bindParam(":mobile_no", $this->mobile_no);
			$stmt->bindParam(":gender", $this->gender);
			$stmt->bindParam(":patient_type_code", $this->patient_type_code);
			$stmt->bindParam(":address", $this->address);
			$stmt->bindParam(":town", $this->town);
			$stmt->bindParam(":district", $this->district);
			$stmt->bindParam(":postcode", $this->postcode);
			$stmt->bindParam(":state", $this->state);
			$stmt->bindParam(":photo_path", $this->photo_path);
			$stmt->bindParam(":relationship", $this->relationship);
			
			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}

	// update the record
	function updatePhoto(){

		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
						photo_path=:photo_path 
					WHERE
						ic_no = :ic_no";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
			$this->ic_no=htmlspecialchars(strip_tags($this->ic_no));
			$this->photo_path=htmlspecialchars(strip_tags($this->photo_path));
			
			// bind values
			$stmt->bindParam(":ic_no", $this->ic_no);
			$stmt->bindParam(":photo_path", $this->photo_path);
			
			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}

	function delete(){

		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE ic_no = ?";

		// prepare query
		$stmt = $this->conn->prepare($query);

		// sanitize
		$this->ic_no=htmlspecialchars(strip_tags($this->ic_no));

		// bind id of record to delete
		$stmt->bindParam(1, $this->ic_no);

		// execute query
		if($stmt->execute()){
			return true;
		}
		return false;

	}


	// search records
	function search($keywords){
	
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					ic_no LIKE ? OR name LIKE ?  
				ORDER BY
					name";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$keywords=htmlspecialchars(strip_tags($keywords));
		$keywords = "%{$keywords}%";
		
		// bind
		$stmt->bindParam(1, $keywords);
		$stmt->bindParam(2, $keywords);
	
		
		// execute query
		$stmt->execute();
		
		return $stmt;
	}

	// search records
	function searchByRegistration($keywords){

		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					ic_no LIKE ? OR name LIKE ? OR town LIKE ? OR state LIKE ? 
				ORDER BY
					name";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$keywords=htmlspecialchars(strip_tags($keywords));
		$keywords = "%{$keywords}%";
		
		// bind
		$stmt->bindParam(1, $keywords);
		$stmt->bindParam(2, $keywords);
		$stmt->bindParam(3, $keywords);
		$stmt->bindParam(4, $keywords);
		
		// execute query
		$stmt->execute();
		
		return $stmt;
	}

}

?>