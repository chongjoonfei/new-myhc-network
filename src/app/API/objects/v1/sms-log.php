<?php

class SmsLog{
  
    // database connection and table name
    private $conn;
    private $table_name = "sms_log";

    // object properties
    public $mobile_no;
	public $message;
	public $status;

  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
		
    }
 
	// create object
	function create(){
	  
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
				 mobile_no=:mobile_no,  message=:message, status=:status";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->mobile_no=htmlspecialchars(strip_tags($this->mobile_no));
		$this->message=htmlspecialchars(strip_tags($this->message));
		$this->status=htmlspecialchars(strip_tags($this->status));
		
		// bind values
		$stmt->bindParam(":mobile_no", $this->mobile_no);
		$stmt->bindParam(":message", $this->message);
		$stmt->bindParam(":status", $this->status);
		
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}	
	 
}

?>