 <?php
/**
 * Author: Majina
 * TestResultInput.search()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/test-result-input/search.php
 * JSON input: none
 * Method: GET   
 */

// required headers
// header("Access-Control-Allow-Origin: *");
// header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/test-marker.php';
 
  
// instantiate database and data object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$testMarker = new TestMarker($db);

$test_marker_arr = $testMarker->readByPanelCode("vital-sign");

$test_marker_code="bmi";
$test_value = "35";

foreach ($test_marker_arr as $test_marker_item) {
    $tmcode = $test_marker_item['code'];
    echo $tmcode . " => ";
    if ($test_marker_code==$tmcode){
        $ref_arr = $test_marker_item['test_reference_range'];
 
        // if (sizeof($ref_arr)>0){
            foreach ($ref_arr as $ref_item) {

                if ($test_value>=$ref_item['min'] && $test_value<=$ref_item['max']){
                    echo $ref_item['min'] . " >= " .$test_value . " >=" . $ref_item['max'] . " = ". $ref_item['code'] ."<br>";
                }
                
                // $test_value
            }
        // }
    }
}


?>