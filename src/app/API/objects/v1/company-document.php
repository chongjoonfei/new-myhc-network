<?php


class CompanyDocument{
  
    // database connection and table name
    private $conn;
    private $table_name = "company_document";

    // object properties
    public $co_reg_no;
	public $filename;
	public $ori_filename;
	public $file_path;
	public $document_code;
	public $date_updated;
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
		
    }
 
	// used when read records by IC No
	function readByCoRegNo($co_reg_no){

		$this->co_reg_no=htmlspecialchars(strip_tags($co_reg_no));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
				co_reg_no = :co_reg_no";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":co_reg_no", $this->co_reg_no);
		
		// execute query
		$stmt->execute();
		
		
		$arr=array();

		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			extract($row);
			$record_item=array(
				"co_reg_no" => $co_reg_no,
				"filename" => $filename,
				"ori_filename" => $ori_filename,
				"file_path" => $file_path,
				"document_code" => $document_code 
			);
			array_push($arr, $record_item);
		}
	  
		return $arr;
 
	}
 

	// create object
	function create(){
	  
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
					co_reg_no=:co_reg_no,  filename=:filename, 
					ori_filename=:ori_filename, file_path=:file_path,
					document_code=:document_code 
					";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->co_reg_no=htmlspecialchars(strip_tags($this->co_reg_no));
		$this->filename=htmlspecialchars(strip_tags($this->filename));
		$this->ori_filename=htmlspecialchars(strip_tags($this->ori_filename));
		$this->file_path=htmlspecialchars(strip_tags($this->file_path));
		$this->document_code=htmlspecialchars(strip_tags($this->document_code));
		
		// bind values
		$stmt->bindParam(":co_reg_no", $this->co_reg_no);
		$stmt->bindParam(":filename", $this->filename);
		$stmt->bindParam(":ori_filename", $this->ori_filename);
		$stmt->bindParam(":file_path", $this->file_path);
		$stmt->bindParam(":document_code", $this->document_code);
		
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}	
	 

	function delete(){

		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE co_reg_no = :co_reg_no 
			and filename = :filename";

		// prepare query
		$stmt = $this->conn->prepare($query);

		// sanitize
		$this->co_reg_no=htmlspecialchars(strip_tags($this->co_reg_no));
		$this->filename=htmlspecialchars(strip_tags($this->filename));

		// bind id of record to delete
		$stmt->bindParam(":co_reg_no", $this->co_reg_no);
		$stmt->bindParam(":filename", $this->filename);

		// execute query
		if($stmt->execute()){
			return true;
		}
		return false;

	}

	function deleteDocumentsByIcNo(){

		// $this->co_reg_no=htmlspecialchars(strip_tags($co_reg_no));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					co_reg_no = :co_reg_no";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":co_reg_no", $this->co_reg_no);
		
		// execute query
		$stmt->execute();
		
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			extract($row);
			unlink($file_path);
		}

	}
	
}

?>