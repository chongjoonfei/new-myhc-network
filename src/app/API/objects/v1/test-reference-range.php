<?php

class TestReferenceRange{
  
    // database connection and table name
    private $conn;
    private $table_name = "test_reference_range";

    // object properties
    public $test_marker_code;
    public $code;
    public $min;
	public $max;
	public $range;
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read all records
	function readAll(){
	  
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				ORDER BY
				test_marker_code,code";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	  
		// execute query
		$stmt->execute();
	  
		return $stmt;
	}

	// create object
	function create(){
	  
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
				test_marker_code=:test_marker_code, code=:code, min=:min, max=:max, summary=:summary";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->test_marker_code=htmlspecialchars(strip_tags($this->test_marker_code));
		$this->code=htmlspecialchars(strip_tags($this->code));
		$this->min=htmlspecialchars(strip_tags($this->min));
		$this->max=htmlspecialchars(strip_tags($this->max));
		$this->summary=htmlspecialchars(strip_tags($this->summary));
		
		// bind values
		$stmt->bindParam(":test_marker_code", $this->test_marker_code);
		$stmt->bindParam(":code", $this->code);
		$stmt->bindParam(":min", $this->min);
		$stmt->bindParam(":max", $this->max);
		$stmt->bindParam(":summary", $this->summary);
		
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}
		
		
	// used when filling up the update record form
	function readOne(){

		$this->code=htmlspecialchars(strip_tags($this->code));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					code = :code  
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":code", $this->code);
 
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
		$this->test_marker_code = $row['test_marker_code'];
		$this->code = $row['code'];
		$this->min = $row['min']; 
		$this->max = $row['max'];
		$this->summary = $row['summary'];
	}

	function readByTestMarkerCode($test_marker_code){
		// select all query
		$query = "SELECT * FROM " . $this->table_name . "  where test_marker_code=:test_marker_code ORDER BY code";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind code of data to be updated
		$stmt->bindParam(":test_marker_code", $test_marker_code);
	  
		// execute query
		$stmt->execute();

		$arr=array();

		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			extract($row);
			$testRefRange_item=array(
				"test_marker_code" => $test_marker_code,
				"code" => $code,
				"min" => $min,
				"max" => $max,
				"summary" => $summary  
			);
			array_push($arr, $testRefRange_item);
		}
	  
		return $arr;

	}


	// update the record
	function update(){
		
		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
						summary = :summary,
						min = :min,
						max = :max
					WHERE
						code = :code and test_marker_code=:test_marker_code";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
			$this->summary=htmlspecialchars(strip_tags($this->summary));
			$this->code=htmlspecialchars(strip_tags($this->code));
			$this->min=htmlspecialchars(strip_tags($this->min));
			$this->max=htmlspecialchars(strip_tags($this->max));
			
			// bind values
			$stmt->bindParam(":test_marker_code", $this->test_marker_code);
			$stmt->bindParam(":code", $this->code);
			$stmt->bindParam(":summary", $this->summary);
			$stmt->bindParam(":min", $this->min);
			$stmt->bindParam(":max", $this->max);
			
			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}

  
	function delete(){

		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE code = ?";

		// prepare query
		$stmt = $this->conn->prepare($query);

		// sanitize
		$this->code=htmlspecialchars(strip_tags($this->code));

		// bind id of record to delete
		$stmt->bindParam(1, $this->code);

		// execute query
		if($stmt->execute()){
			return true;
		}
		return false;

	}


}

?>