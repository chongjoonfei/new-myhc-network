<?php


class DocumentUpload{
  
    // database connection and table name
    private $conn;
    private $table_name = "document_upload";

    // object properties
    public $code;
	public $package_category;
	public $name;
	public $note;
	public $doc_path;
	public $sort_id;
	public $enabled;
	 
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
		
    }

    // read all records
	function readByPackageCategory(){
	  
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				where package_category = :package_category
				ORDER BY
					sort_id";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	  
		$stmt->bindParam(":package_category", $this->package_category);

		// execute query
		$stmt->execute();
	  
		return $stmt;
	}

	// read all records
	function readAll(){
	
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				ORDER BY
					sort_id";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		// execute query
		$stmt->execute();
		
		return $stmt;
	}


	// used when filling up the update record form
	function readOne(){

		$this->category_code=htmlspecialchars(strip_tags($this->category_code));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					code = :code
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":code", $this->code);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
		$this->code = $row['code'];
		$this->name = $row['name'];
		$this->doc_path = $row['doc_path'];
		$this->note = $row['note'];
		$this->sort_id = $row['sort_id'];
		$this->enabled = $row['enabled'];
	}

 

	// create object
	function create(){
	  
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
				code=:code,  name=:name, note=:note,
				doc_path=:doc_path, sort_id=:sort_id, enabled=:enabled";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->code=htmlspecialchars(strip_tags($this->code));
		$this->name=htmlspecialchars(strip_tags($this->name));
		$this->note=htmlspecialchars(strip_tags($this->note));
		$this->doc_path=htmlspecialchars(strip_tags($this->doc_path));
		$this->sort_id=htmlspecialchars(strip_tags($this->sort_id));
		$this->enabled=htmlspecialchars(strip_tags($this->enabled));
		
		// bind values
		$stmt->bindParam(":code", $this->code);
		$stmt->bindParam(":name", $this->name);
		$stmt->bindParam(":note", $this->note);
		$stmt->bindParam(":doc_path", $this->doc_path);
		$stmt->bindParam(":sort_id", $this->sort_id);
		$stmt->bindParam(":enabled", $this->enabled);
		
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}	
	
	// update the record
	function update(){
	
		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
						name = :name,
						doc_path = :doc_path,
						note = :note,
						sort_id = :sort_id,
						enabled = :enabled
					WHERE
						code = :code";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
			$this->code=htmlspecialchars(strip_tags($this->code));
			$this->name=htmlspecialchars(strip_tags($this->name));
			$this->note=htmlspecialchars(strip_tags($this->note));
			$this->doc_path=htmlspecialchars(strip_tags($this->doc_path));
			$this->sort_id=htmlspecialchars(strip_tags($this->sort_id));
			$this->enabled=htmlspecialchars(strip_tags($this->enabled));
			
			// bind values
			$stmt->bindParam(":code", $this->code);
			$stmt->bindParam(":name", $this->name);
			$stmt->bindParam(":note", $this->note);
			$stmt->bindParam(":doc_path", $this->doc_path);
			$stmt->bindParam(":sort_id", $this->sort_id);
			$stmt->bindParam(":enabled", $this->enabled);
			
			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}

	function delete(){

		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE code = ?";

		// prepare query
		$stmt = $this->conn->prepare($query);

		// sanitize
		$this->code=htmlspecialchars(strip_tags($this->code));

		// bind id of record to delete
		$stmt->bindParam(1, $this->code);

		// execute query
		if($stmt->execute()){
			return true;
		}
		return false;

	}
	
}

?>