<?php

class iPay88PaymentMethod{
  
    // database connection and table name
    private $conn;
    private $table_name = "ipay88_payment_method";

    // object properties
    
	public $payment_id;
	public $payment_method;

	
	
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read all records
	function readAll(){
	  
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				ORDER BY
				payment_id";
					
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	  
		// execute query
		$stmt->execute();
	  
		return $stmt;
	}

	// used when read record by screening date and timeslot
	function readByPaymentId($payment_id){

		$this->payment_id=htmlspecialchars(strip_tags($payment_id));
		
		// query to read records
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					payment_id = :payment_id
					";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":payment_id", $payment_id);
		
		// execute query
		$stmt->execute();
		
	
		$record_item=null;
	
		if ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			extract($row);
			$record_item=array(
				"payment_id" => $payment_id,
				"payment_method" => $payment_method
			);
		}
		
		return $record_item;

	}	
	
}

?>