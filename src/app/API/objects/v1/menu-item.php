<?php


class MenuItem{
  
    // database connection and table name
    private $conn;
    private $table_name = "menu_item";

    // object properties
    public $item_id;
	public $name;
	public $page;
	public $path;
	public $lang_bm;
	public $enabled;
	public $read;
	public $ficon;
	
	 
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
		
    }

    // read all records
	function readAll(){
	  
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				ORDER BY
					path";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	  
		// execute query
		$stmt->execute();
	  
		return $stmt;
	}


	// used when filling up the update record form
	function readOne(){

		$this->item_id=htmlspecialchars(strip_tags($this->item_id));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					item_id = :item_id
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":item_id", $this->item_id);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
		$this->item_id = $row['item_id'];
		$this->name = $row['name'];
		$this->page = $row['page'];
		$this->path = $row['path'];
		$this->enabled = $row['enabled'];
		$this->ficon = $row['ficon'];
		
	}

	function readByMainIdLang($main_id,$lang){
				// select all query
				$query = "SELECT * FROM menu_item i, menu_main_item m 
				where m.main_id=:main_id and m.item_id = i.item_id  order by sort_id";
			  
				// prepare query statement
				$stmt = $this->conn->prepare($query);
		
				// bind code of data to be updated
				$stmt->bindParam(":main_id", $main_id);
			  
				// execute query
				$stmt->execute();
		
				$arr=array();
				

				while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
					extract($row);
					$fieldname = ($lang=="bm") ? $lang_bm : $name ;
				
					$record=array(
						"item_id" => $item_id,
						"name" => $fieldname,
						"page" => $page,
						"path" => $path,
						"enabled" => $enabled,
						"sort_id" => $sort_id,
						"ficon"   => $ficon,
					);
					array_push($arr, $record);
					}
		 	 
		
				return $arr;
	}

	function readByMainId($main_id){

		// select all query
		$query = "SELECT * FROM menu_item i, menu_main_item m 
		where m.main_id=:main_id and m.item_id = i.item_id order by sort_id";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind code of data to be updated
		$stmt->bindParam(":main_id", $main_id);
	  
		// execute query
		$stmt->execute();

		$arr=array();

		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			extract($row);
			$record=array(
				"item_id" => $item_id,
				"name" => $name,
				"page" => $page,
				"path" => $path,
				"enabled" => $enabled,				
				"sort_id" => $sort_id,
				"ficon"   => $ficon,
				"permission" => array("create"=>$int_create,"read"=>$int_read,"update"=>$int_update,'delete'=>$int_delete)
			);
			array_push($arr, $record);
		}
 

		return $arr;

	}

	// create object
	function create(){
	  
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
					item_id=:item_id,  name=:name, page=:page, path=:path, enabled=:enabled";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->item_id=htmlspecialchars(strip_tags($this->item_id));
		$this->name=htmlspecialchars(strip_tags($this->name));
		$this->page=htmlspecialchars(strip_tags($this->page));
		$this->path=htmlspecialchars(strip_tags($this->path));
		$this->enabled=htmlspecialchars(strip_tags($this->enabled));
		
		// bind values
		$stmt->bindParam(":item_id", $this->item_id);
		$stmt->bindParam(":name", $this->name);
		$stmt->bindParam(":page", $this->page);
		$stmt->bindParam(":path", $this->path);
		$stmt->bindParam(":enabled", $this->enabled);
		
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}	
	
	// update the record
	function update(){
	
		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
						name = :name,
						page = :page,
						path = :path,
						enabled = :enabled
					WHERE
						item_id = :item_id";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
			$this->item_id=htmlspecialchars(strip_tags($this->item_id));
			$this->name=htmlspecialchars(strip_tags($this->name));
			$this->page=htmlspecialchars(strip_tags($this->page));
			$this->path=htmlspecialchars(strip_tags($this->path));
			$this->enabled=htmlspecialchars(strip_tags($this->enabled));
			
			// bind values
			$stmt->bindParam(":item_id", $this->item_id);
			$stmt->bindParam(":name", $this->name);
			$stmt->bindParam(":page", $this->page);
			$stmt->bindParam(":path", $this->path);
			$stmt->bindParam(":enabled", $this->enabled);
			
			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}

	function delete(){

		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE item_id = ?";

		// prepare query
		$stmt = $this->conn->prepare($query);

		// sanitize
		$this->item_id=htmlspecialchars(strip_tags($this->item_id));

		// bind id of record to delete
		$stmt->bindParam(1, $this->item_id);

		// execute query
		if($stmt->execute()){
			return true;
		}
		return false;

	}
	
}

?>