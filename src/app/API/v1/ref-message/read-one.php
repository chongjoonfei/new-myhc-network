<?php
/**
 * Author: Elizha
 * RefMessage.readOne()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/ref-message/read-one.php?c=<message_type_code>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/ref-message.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$refMessage = new RefMessage($db);

// set ID property of record to read
$refMessage->message_type_code = isset($_GET['c']) ? $_GET['c'] : die();
  
// read the details of data to be edited
$refMessage->readOne();
  
if($refMessage->message_type_code!=null){
    // create array
    $refMessage_arr = array(
        "message_type_code"  => $refMessage->message_type_code,
        "message_type_desc"  => $refMessage->message_type_desc 
        
    );
  
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($refMessage_arr);
}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "Reference Message info does not exist for " . $refMessage->message_type_code,"error" => "404 Not found"));
}
?>