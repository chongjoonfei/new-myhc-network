<?php
/**
 * Author: Elizha
 * AddOn.delete()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/add-on/delete.php
 * JSON input: { "add_on_code":"<add_on_code>", "name": "<name>", "price":"<price>", "remark":"<remark>", "status":"<status>", 
 * "unit":"<unit>", "unit_decimal":"<unit_decimal>","no_of_patient":"<no_of_patient>", "patient_type_code":"<patient_type_code>"}
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object file
include_once '../../config/db.php';
include_once '../../objects/v1/add-on.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$addOn = new AddOn($db);
  
// get data input
$data = json_decode(file_get_contents("php://input"));
  
// set data to be deleted
$addOn->add_on_code = $data->add_on_code;

// delete the record
if($addOn->delete()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Add On Services info was deleted.","error"=>"","errorFound"=>false));
}
  
// if unable to delete the record
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to delete Add On Services info.","error"=>"503 service unavailable","errorFound"=>true));
}
?>