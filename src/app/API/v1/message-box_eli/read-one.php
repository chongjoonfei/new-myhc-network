<?php
/**
 * Author: Elizha
 * MessageBox.readOne()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/message-box/read-one.php?c=<message_inbox_code>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/message-box.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$messageBox = new MessageBox($db);

// set ID property of record to read
$messageBox->message_inbox_code = isset($_GET['c']) ? $_GET['c'] : die();
  
// read the details of data to be edited
$messageBox->readOne();
  
if($messageBox->message_inbox_code!=null){
    // create array
    $messageBox_arr = array(
        "message_inbox_code"  => $message_inbox_code,
        "sender"  => $sender ,
        "receiver" => $receiver,
        "subject"  => $subject,
        "message" => $message;
        "headers" => $headers,
        "date_sent" =>$date_sent,
        "message_type_code" =>$message_type_code,
        "ic_no"=>$ic_no,
        "status"=>$status,
        "attachment"=>$attachment
        
    );
  
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($messageBox_arr);
}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "Message info does not exist for " . $messageBox->message_inbox_code,"error" => "404 Not found"));
}
?>