<?php
/**
 * Author: Elizha
 * MessageBox.create()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/message-box/create.php
 * JSON input: { "message_inbox_code":"<message_inbox_code>", "sender": "<sender>", "receiver":"<receiver>", "subject":"<subject>", "message":"<message>", 
 * "headers":"<headers>", "date_sent":"<date_sent>","message_type_code":"<message_type_code>", "ic_no":"<ic_no>",
 * "status":"<status>", "attachment":"<attachment>"}
 * Method: POST   
 */



// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/message-box.php';
  
$database = new Database();
$db = $database->getConnection();

// prepare object
$messageBox = new MessageBox($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));
  
// make sure data is not empty
if (
    !empty($data->message_inbox_code) &&
    !empty($data->message_type_code) 
){

    // set data property values
    $messageBox->message_inbox_code = $data->message_inbox_code;
    $messageBox->sender = $data->sender;
	$messageBox->receiver = $data->receiver;
    $messageBox->subject = $data->subject; 
    $messageBox->message = $data->message;
    $messageBox->headers = $data->headers;
    $messageBox->date_sent = $data->date_sent;
    $messageBox->message_type_code = $data->message_type_code;
    $messageBox->ic_no = $data->ic_no;
	$messageBox->status = $data->status;
    $messageBox->attachment = $data->attachment;

  
    // create the record
    if($messageBox->create()){
  
        // set response code - 201 created
        http_response_code(201);
  
        // tell the user
        echo json_encode(array("message" => "Add On Services info was created.","errorFound"=>false,"error" => "",));
    }
  
    // if unable to create record, tell the user
    else{
		// $messageBox->message_inbox_code = $data->message_inbox_code;
  		// read the details of record to be edited
		// $messageBox->readOne();
		if($messageBox->message_type_code!=null){
			// record already exist
			http_response_code(409);
  
			// tell the user
			echo json_encode(array("message" => "Message info already exist","errorFound"=>true,"error" => "409 conflict"));
		}else{
			// set response code - 503 service unavailable
			http_response_code(503);
  
			// tell the user
			echo json_encode(array("message" => "Unable to create Message info.","errorFound"=>true,"error" => "503 service unavailable"));
		}

    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to create Message. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}
?>