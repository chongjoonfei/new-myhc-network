<?php

ini_set('display_errors', 1); 
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
mysqli_report(MYSQLI_REPORT_ALL);

/**
 * Author: Majina
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/company-document/upload-files.php
 * Method: POST   
 */
     
// required headers
header("Access-Control-Allow-Origin: *");
// header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/company-document.php';
include_once '../../objects/v1/company.php';

$database = new Database();
$db = $database->getConnection();
  
$companyDocument = new CompanyDocument($db);
$company = new Company($db);

$target_dir = $_SERVER['DOCUMENT_ROOT'] . "/" . "upload_files/";
// $fileName = $_FILES['fileToUpload']['name'];
$uploadOk = 1;

$co_id = $_POST["co_id"];
$co_reg_no = $_POST["co_reg_no"];
$document_code = $_POST["document_code"];

// $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
$errorFound = false;

// Check if file already exists
if (!isset($co_reg_no) && !isset($document_code)) {
  $msg = "Data incomplete - " . $co_reg_no .' , '. $document_code;
  $errorFound=true;
}

 

if (!$errorFound){
  $file_names = $_FILES["fileToUpload"]['name'];

  for ($i = 0; $i < sizeof($file_names); $i++) {
    $fileName = $file_names[$i];
    
    $newFilename = $co_id .'_'.str_replace(' ','_',$fileName);
    $target_file = $target_dir . $newFilename;

    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"][$i], $target_file) ) {

      $companyDocument->co_id = $co_id;
      $companyDocument->co_reg_no = $co_reg_no;
      $companyDocument->document_code =$document_code;
      $companyDocument->filename = $newFilename;
      $companyDocument->ori_filename = $fileName;
      $companyDocument->file_path = $target_file;

      if ($companyDocument->create()){

        if ($document_code=='photo'){
          $company->co_reg_no =  $co_reg_no;
          $company->photo_path = $newFilename;
          $company->updatePhoto();
        }
        
        $msg = "The file ". htmlspecialchars( basename($fileName)). " has been uploaded.";
        $errorFound=false;
      }else{
        $msg = "Sorry, there was an error uploading your file. "
        ."co_reg_no->".$companyDocument->co_reg_no.', doccode->' .$companyDocument->document_code
        .',filename->'.$companyDocument->filename.', orifilename->'.$companyDocument->ori_filename.',filepath->'.$companyDocument->file_path;
        $errorFound=true;
      }

    } else {
      $msg = "Sorry, your file is not uploaded. ";
      $errorFound=true;
    }
  }
} 

echo json_encode(array("message" => $msg,"errorFound" => $errorFound));
?>