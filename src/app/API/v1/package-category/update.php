<?php
/**
 * Author: Majina
 * PackageCategory.update()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/package-category/update.php
 * JSON input: { "code":"<code>", "description":"<description>", "picture_path": "<picture_path>"}
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/package-category.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$packageCategory = new PackageCategory($db);
  
// get id of record to be edited
$data = json_decode(file_get_contents("php://input"));
  
// set ID property of record to be edited
$packageCategory->category_code = $data->category_code;
  
// set data property values
$packageCategory->name = $data->name;
$packageCategory->description = $data->description;
$packageCategory->picture_path = $data->picture_path;
  
// update the record
if($packageCategory->update()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Package Category info was updated.","errorFound"=>false,"error"=>"success"));
}
  
// if unable to update the record, tell the user
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to update Package Category info.","errorFound"=>true,"error"=>"503 service unavailable"));
}
?>