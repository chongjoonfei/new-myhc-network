<?php
/**
 * Author: Elizha
 * ContentMgmt.readOne()
 * URL for testing : https://myhealthcard.my/myhc-api/v1/content-mgmt/read-one.php?c=<name>
 * JSON input: none
 * Method: GET   
 */


ini_set('display_errors', 1); 
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
mysqli_report(MYSQLI_REPORT_ALL);

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/content-mgmt.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$contentMgmt = new ContentMgmt($db);

// set ID property of record to read
$contentMgmt->name = isset($_GET['c']) ? $_GET['c'] : die();
  
// read the details of data to be edited
$contentMgmt->readOne();
  
if($contentMgmt->name!=null){
    // create array
    $contentMgmt_arr = array(
        "name"  => $contentMgmt->name,
        "content"  => $contentMgmt->content ,
        "date_updated" => $contentMgmt->date_updated
        
    );
  
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($contentMgmt_arr);
}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "Content Mgmt info does not exist for " . $_GET['c'],"error" => "404 Not found"));
}
?>