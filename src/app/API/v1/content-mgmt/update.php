<?php
/**
 * Author: Elizha
 * ContentMgmt.update()
 * URL for testing : https://myhealthcard.my/myhc-api/v1/content-mgmt/update.php 
 * JSON input: { "id":null, "name": "<name>", "content":"<content>", "date_updated":"<date_updated>"}
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/content-mgmt.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$contentMgmt = new ContentMgmt($db);
  
// get id of record to be edited
$data = json_decode(file_get_contents("php://input"));
  
// set ID property of record to be edited
$contentMgmt->name = $data->name;

// set data property values
$contentMgmt->name = $data->name;
$contentMgmt->content= $data->content;
$contentMgmt->date_updated = $data->date_updated;


  
// update the record
if($contentMgmt->update()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => " Content Mgmt info was updated.","errorFound"=>false,"error"=>"success"));
}
  
// if unable to update the record, tell the user
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to update Content Mgmt info.","errorFound"=>true,"error"=>"503 service unavailable"));
}
?>