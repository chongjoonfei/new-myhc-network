<?php
/**
 * Author: Majina
 * RegistrationPerson.create()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/registration-person/create.php
 * JSON input: { "reg_no":"<reg_no>", "ic_no":"<ic_no>", "person_type_code": "<person_type_code>" }
 * Method: POST   
 */
     
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/registration-person.php';
  
$database = new Database();
$db = $database->getConnection();
  
$registrationPerson = new RegistrationPerson($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));
  
// make sure data is not empty
if (
    !empty($data->reg_no) &&
    !empty($data->ic_no) &&
    !empty($data->person_type_code) 
){
  
    // set data property values
    $registrationPerson->reg_no=$data->reg_no;
    $registrationPerson->ic_no=$data->ic_no;
    $registrationPerson->person_type_code=$data->person_type_code;
 
  
    // create the record
    if($registrationPerson->create()){
  
        // set response code - 201 created
        http_response_code(201);
  
        // tell the user
        echo json_encode(array("message" => "Person registration has been created.","errorFound"=>false,"error" => "",));
    }
  
    // if unable to create record, tell the user
    else{
		$registrationPerson->ic_no = $data->ic_no;
        $registrationPerson->reg_no = $data->reg_no;
  		// read the details of patient to be edited
		$registrationPerson->readOne();
		if(isset($registrationPerson->person_type_code)){
			// record already exist
			http_response_code(409);
  
			// tell the user
			echo json_encode(array("message" => "Person registration already exist","errorFound"=>true,"error" => "409 conflict"));
		}else{
			// set response code - 503 service unavailable
			http_response_code(503);
  
			// tell the user
			echo json_encode(array("message" => "Unable to create Person registration","errorFound"=>true,"error" => "503 service unavailable"));
		}

    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to create Person registration. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}
?>