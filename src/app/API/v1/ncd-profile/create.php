<?php
/**
 * Author: Majina
 * NcdProfile.create()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/ncd-profile/create.php
 * JSON input: { "ic_no":"<ic_no>", "tabacco_use":"<tabacco_use>", "alcohol_consumption":"<alcohol_consumption>, "diet_eat_fruit":"<diet_eat_fruit>", "diet_eat_vege":"<diet_eat_vege>", "physical_activities":"<physical_activities>", "history_hypertension":"<history_hypertension>", "history_diabetes":"<history_diabetes>"}
 * Method: POST   
 */
     
ini_set('display_errors', 1); 
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
mysqli_report(MYSQLI_REPORT_ALL);

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/ncd-profile.php';
  
$database = new Database();
$db = $database->getConnection();
  
$ncdProfile = new NcdProfile($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));

//echo json_encode(array($data));
 

// make sure data is not empty
if (
    !empty($data->ic_no) 
){
  
    // set data property values
    $ncdProfile->ic_no = $data->ic_no;
	$ncdProfile->tabacco_use = $data->tabacco_use;
    $ncdProfile->alcohol_consumption = $data->alcohol_consumption;
    $ncdProfile->diet_eat_fruit = $data->diet_eat_fruit;
    $ncdProfile->diet_eat_vege = $data->diet_eat_vege;
    $ncdProfile->physical_activities = $data->physical_activities;
    $ncdProfile->history_hypertension = $data->history_hypertension;
    $ncdProfile->history_diabetes = $data->history_diabetes;
  
    // create the record
    if($ncdProfile->create()){
        // set response code - 201 created
        http_response_code(201);
  
        // tell the user
        echo json_encode(array("message" => "NCD profile has been created.","errorFound"=>false,"error" => ""));
    }
  
    // if unable to create record, tell the user
    else{
		$ncdProfile->ic_no  = $data->ic_no;
  		// read the details of patient to be edited
		$ncdProfile->readOne();
		if (isset($ncdProfile->ic_no)){
			// record already exist
			http_response_code(409);
  
			// tell the user
			echo json_encode(array("message" => "NCD profile info already exist","errorFound"=>true,"error" => "409 conflict"));
		}else{
			// set response code - 503 service unavailable
			http_response_code(503);
  
			// tell the user
			echo json_encode(array("message" => "Unable to create NCD profile.","errorFound"=>true,"error" => "503 service unavailable"));
		}

    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to create NCD profile. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}

 
?>