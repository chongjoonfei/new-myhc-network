<?php
/**
 * Author: Majina
 * MenuMainItem.create()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/menu-main-item/create.php
 * JSON input: { "main_id":"<main_id>", "item_id":"<item_id>", "sort_id": "<sort_id>"}
 * Method: POST   
 */
     
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/menu-main-item.php';
  
$database = new Database();
$db = $database->getConnection();
  
$menuMainItem = new MenuMainItem($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));


// make sure data is not empty
if (
    !empty($data->main_id) && !empty($data->item_id) 
){
  
    // set data property values
    $menuMainItem->main_id = $data->main_id;
	$menuMainItem->item_id = $data->item_id;
    $menuMainItem->create = $data->create;
    $menuMainItem->read = $data->read;
    $menuMainItem->update = $data->update;
    $menuMainItem->delete = $data->delete;
   
   
    // update the record
    if($menuMainItem->updateCRUD()){
  
        // set response code - 201 updated
        http_response_code(201);
  
        // tell the user
        echo json_encode(array("message" => "CRUD info was updated.","errorFound"=>false,"error" => "",));
    }
  
    // if unable to create record, tell the user
    else{
  		// read the details of patient to be edited
		/*$menuMainItem->readOne();
		if (isset($menuMainItem->item_id)){
			// record already exist
			http_response_code(409);
  
			// tell the user
			echo json_encode(array("message" => "CRUD info already exist","errorFound"=>true,"error" => "409 conflict"));
		}else{*/
			// set response code - 503 service unavailable
			http_response_code(503);
  
			// tell the user
			echo json_encode(array("message" => "Unable to update CRUD info.".$menuMainItem->updateCRUD(),"errorFound"=>true,"error" => "503 service unavailable"));
		//}

    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to save update  info. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}
?>