<?php
/**
 * Author: Majina
 * Booking.delete()
 * URL for testing : https://myhc.my/myhc-api/v1/booking/delete.php
 * JSON input: { "booking_no":"<booking_no>", "reg_no":"<reg_no>", "ic_no":"<ic_no>", 
 * "screening_location":"<screening_location>", "screening_center_id":"<screening_center_id>", "screening_date":"<screening_date>" ,
 * "screening_time":"<screening_time>"  }
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object file
include_once '../../config/db.php';
include_once '../../objects/v1/booking.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$booking = new Booking($db);
  
// get data input
$data = json_decode(file_get_contents("php://input"));
  
// set data to be deleted
$booking->booking_id = $data->booking_id;
  
// delete the record
if($booking->delete()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Booking info was deleted.","error"=>"","errorFound"=>false));
}
  
// if unable to delete the record
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to delete booking info.","error"=>"503 service unavailable","errorFound"=>true));
}
?>