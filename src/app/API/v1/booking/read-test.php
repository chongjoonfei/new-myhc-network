<?php
/**
 * Author: Majina
 * Booking.readOne()
 * URL for testing : https://myhc.my/myhc-api/v1/booking/read-one.php?c=<booking_no>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/booking.php';

// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$booking = new Booking($db);
  
  
// read the details of data to be edited
$booking->booking_no = "B202202-11";
$result = $booking->readByBookingNoForTestForm();
  
if (isset($result['person'])){
 
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($result);
}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "Booking info does not exist for " . $booking->booking_no,"error" => "404 Not found"));
}
?>