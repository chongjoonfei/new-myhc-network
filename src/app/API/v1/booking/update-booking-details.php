<?php
/**
 * Author: Majina
 * Booking.updateBookingDetails()
 * URL for testing : https://myhc.com/myhc-api/v1/booking/update-booking-details.php 
 * JSON input: { "booking_no":"<booking_no>", "reg_no":"<reg_no>", "ic_no":"<ic_no>", 
 * "screening_location":"<screening_location>", "screening_center_id":"<screening_center_id>", "screening_date":"<screening_date>" ,
 * "screening_time":"<screening_time>" }
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/booking.php';
include_once '../../objects/v1/user-account.php';
include_once '../../objects/v1/constant.php';
 include_once '../../objects/v1/email-mgmt.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
$updateBooking = new Booking($db);  
  
// get id of record to be edited
$data = json_decode(file_get_contents("php://input"));
$updateType = isset($_GET['t']) ? $_GET['t'] : die();

if (!isset($updateType)){
        // set response code - 503 service unavailable
        http_response_code(503);

        // tell the user
        echo json_encode(array("message" => "Unable to update booking info.","errorFound"=>true,"error"=>"Update type does not specified"));

}else{

    if ($updateType=='DATE' || $updateType=='TIME' || $updateType=='VENUE' || $updateType=='STATUS' || $updateType=='REMARK'){
   
        $updateBooking = new Booking($db);

        // set ID property of record to be edited, 1 PK
        $updateBooking->booking_id = $data->booking_id;
        $updateBooking->booking_no = $data->booking_no;
 
        $updateBooking->readOne();

        if (isset($updateBooking->reg_no)){
            if ($updateType=='STATUS') {
                $updateBooking->status = $data->status;
                if ($data->status=="CANCELLED")  $updateBooking->remark =  $data->remark;
            }
            if ($updateType=='DATE') $updateBooking->screening_date = $data->screening_date;
            if ($updateType=='TIME') $updateBooking->screening_time = $data->screening_time;
            if ($updateType=='VENUE') $updateBooking->screening_center_id = $data->screening_center_id;
            if ($updateType=='REMARK') $updateBooking->remark = $data->remark;

            // update the record
            if($updateBooking->update()){

                if ($data->status=="PENDING-CANCELLATION"){
                    $displayBooking = new Booking($db);
                    $displayBooking->booking_no = $data->booking_no;
                    $result = $displayBooking->readByBookingNo();

                    $userAccount = new UserAccount($db);
                    $userAccount->readByIcNo($result['ic_no']);
 
                    $emailMgmt = new EmailMgmt($db);
                    $emailMgmt->code = "user-booking-cancelled";
                    $emailMgmt->readOne();
                    $emailMgmt->receivers = CONST_ADMIN_EMAIL;
                    $emailMgmt->paramValues =  array( 
                        array("param" => "?booking_no", "value" =>  $result['booking_no']),
                        array("param" => "?username", "value" =>  $userAccount->username),
                        array("param" => "?name", "value" => $result['person']['name']),
                        array("param" => "?ic_no", "value" => $result['person']['ic_no'])
                   );

                   foreach ($result['products'] as $product) {
                    array_push($emailMgmt->paramValues,array("param" => "?addon_code", "value" =>  $product['code']));
                    array_push($emailMgmt->paramValues,array("param" => "?addon_name", "value" =>  $product['name']));
                    break;
                   }


                    $emailMgmt->sendMail();
                }
            
                // set response code - 200 ok
                http_response_code(200);
            
                // tell the user
                echo json_encode(array("message" => "Booking has been successfully updated.","errorFound"=>false,"error"=>"success"));
            }
            
            // if unable to update the record, tell the user
            else{
            
                // set response code - 503 service unavailable
                http_response_code(503);
            
                // tell the user
                echo json_encode(array("message" => "Unable to update the booking.","errorFound"=>true,"error"=>"Service unavailable"));
            }
        }else{
            // set response code - 503 service unavailable
            http_response_code(503);

            // tell the user
            echo json_encode(array("message" => "Unable to update the booking.","errorFound"=>true,"error"=>"Booking no does not exist"));
        }
    }else{
        // set response code - 503 service unavailable
        http_response_code(503);

        // tell the user
        echo json_encode(array("message" => "Unable to update booking.","errorFound"=>true,"error"=>"Update not allowed"));

    }
 
}
  

?>