<?php
/**
 * Author: Majina
 * Booking.changeBooking()
 * URL for testing : https://myhc.my/myhc-api/v1/booking/create-new-charge.php
 * JSON input: { "booking_no":"<booking_no>", "reg_no":"<reg_no>", "ic_no":"<ic_no>", 
 * "screening_location":"<screening_location>", "screening_center_id":"<screening_center_id>", "screening_date":"<screening_date>" ,
 * "screening_time":"<screening_time>" }
 *  
 * Method: POST   
 */
     
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/booking.php';
include_once '../../objects/v1/booking-product.php';
  
$database = new Database();
$db = $database->getConnection();
  
$booking = new Booking($db);
$BookingProduct = new BookingProduct($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));
  
// make sure data is not empty
if (
    isset($data->booking_id) &&
    (isset($data->screening_location) || isset($data->screening_center_id))
    ){
        $booking->booking_no = $data->booking_no;
        $booking->readOne();

        if (isset($booking->booking_id)){
            $booking->screening_center_id = $data->screening_center_id;
            $booking->screening_location = $data->screening_location;
            $booking->update();

            $products  = $data->products;

            foreach ($products as $product) {
                $bookingProduct = new BookingProduct($db);
                $bookingProduct->booking_id =  $data->booking_id;
                $bookingProduct->code = $product->code;
                $bookingProduct->name = $product->name;
                $bookingProduct->patient_type = $product->patient_type;
                $bookingProduct->price = $product->price;
                $bookingProduct->quantity = $product->quantity;
                $bookingProduct->total_price = $product->total_price;
                $bookingProduct->remark = $product->remark;
                $bookingProduct->ref_no = $booking->booking_no .'#2';
                $bookingProduct->create();
            }
            // set response code - 201 created
            http_response_code(201);
    
            // tell the user
            echo json_encode(array("message" => "Booking info has been updated.","errorFound"=>false,"error" => "","content"=>$booking));

        }else{
            http_response_code(400);
  
            // tell the user
            echo json_encode(array("message" => "Unable to update booking info.","errorFound"=>true,"error" => "400 bad request"));
        
        }
  
 
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to update booking info. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}
?>