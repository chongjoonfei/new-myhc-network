<?php
/**
 * Author: Majina
 * Timeslot.update()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/timeslot/update.php
 * JSON input: { "ts_id":"<ts_id>", "ts_description":"<ts_description>"}
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/timeslot.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$timeslot = new Timeslot($db);
  
// get id of record to be edited
$data = json_decode(file_get_contents("php://input"));
  
// set ID property of record to be edited
$timeslot->ts_code = $data->ts_code;
  
// set data property values
$timeslot->ts_code = $data->ts_code;
$timeslot->ts_description = $data->ts_description;
 
  
// update the record
if($timeslot->update()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Timeslot info was updated.","errorFound"=>false,"error"=>"success"));
}
  
// if unable to update the record, tell the user
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to update timeslot info.","errorFound"=>true,"error"=>"503 service unavailable"));
}
?>