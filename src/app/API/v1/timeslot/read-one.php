<?php
/**
 * Author: Majina
 * Timeslot.readOne()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/timeslot/read-one.php?c=<ts_code>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/timeslot.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$timeslot = new Timeslot($db);
  
// set ID property of record to read
$timeslot->ts_code = isset($_GET['c']) ? $_GET['c'] : die();
  
// read the details of data to be edited
$timeslot->readOne();
  
if (isset($timeslot->ts_code)){
 
    // create array
    $arr = array(
        "ts_code" => $timeslot->ts_code,
		"ts_description" => $timeslot->ts_description 
    );
  
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($arr);
}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "Timeslot info does not exist for " . $screeningCenter->id,"error" => "404 Not found"));
}
?>