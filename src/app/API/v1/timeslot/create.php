<?php
/**
 * Author: Majina
 * Timeslot.create()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/timeslot/create.php
 * JSON input: { "ts_code":"<ts_code>", "ts_code":"<ts_description>"}
 * Method: POST   
 */
     
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/timeslot.php';
  
$database = new Database();
$db = $database->getConnection();
  
$timeslot = new Timeslot($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));
  
// make sure data is not empty
if (
    !empty($data->ts_code) 
){
  
    // set data property values
    $timeslot->ts_code = $data->ts_code;
	$timeslot->ts_description = $data->ts_description;
 
  
    // create the record
    if($timeslot->create()){
  
        // set response code - 201 created
        http_response_code(201);
  
        // tell the user
        echo json_encode(array("message" => "Timeslot info has been created.","errorFound"=>false,"error" => ""));
    }
  
    // if unable to create record, tell the user
    else{
		$timeslot->ts_code = $data->ts_code;
  		// read the details of patient to be edited
		$timeslot->readOne();
		if (isset($timeslot->ts_code)){
			// record already exist
			http_response_code(409);
  
			// tell the user
			echo json_encode(array("message" => "Timeslot info already exist","errorFound"=>true,"error" => "409 conflict"));
		}else{
			// set response code - 503 service unavailable
			http_response_code(503);
  
			// tell the user
			echo json_encode(array("message" => "Unable to create Timeslot info.","errorFound"=>true,"error" => "503 service unavailable"));
		}

    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to create Time slot info. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}
?>