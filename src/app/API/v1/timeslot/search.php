<?php
/**
 * Author: Majina
 * Timeslot.search()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/timeslot/search.php?s=<keyword>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
  
// include database and object files
include_once '../../config/core.php';
include_once '../../config/db.php';
include_once '../../objects/v1/screening-center.php';
  
// instantiate database and object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$timeslot = new Timeslot($db);
  
// get keywords
$keywords=isset($_GET["s"]) ? $_GET["s"] : "";
  
// query data
$stmt = $timeslot->search($keywords);
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $arr=array();
    $arr["data"]=array();
  
    $total_records = 0;
    
    // retrieve our table contents
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
  
        $rec=array(
            "ts_code" => $ts_code,
            "ts_description" => $ts_description 
        );
  
        array_push($arr["data"], $rec);
        $total_records++;
    }
  
    $arr["total_records"]=$total_records;

    // set response code - 200 OK
    http_response_code(200);
  
    // show data
    echo json_encode($arr);
}
  
else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not found
    echo json_encode(
        array("message" => "No timeslot found.","errorFound"=>true,"error"=>"404 Not found")
    );
}
?>