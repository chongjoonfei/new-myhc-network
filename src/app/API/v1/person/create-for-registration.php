<?php
/**
 * Author: Majina
 * Person.createForRegistration()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/person/create.php
 * JSON input: { "person": 
 *                  { "reg_no":"<reg_no>", "ic_no":"<ic_no>", "name": "<name>",
 *                  "gender":"<gender>", "age":"<age>", "email": "<email>",
 *                  "mobile_no":"<mobile_no>", "patient_type_code":"<patient_type_code>", "address": "<address>",
 *                  "town":"<town>", "district":"<district>", "postcode": "<postcode>", "state": "<state>"},
 *               "registration" : 
 *                  { "ic_no": "<ic_no>", "reg_no": "<reg_no>", 
 *                  "person_type_code":"<person_type_code>", "admin_type":"<admin_type>"}
 *             }
 * Method: POST   
 */
     
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/person.php';
include_once '../../objects/v1/registration-person.php';
  
$database = new Database();
$db = $database->getConnection();
  
$person = new Person($db);
$registrationPerson = new RegistrationPerson($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));

$dataPerson = $data->person;
$dataRegistration = $data->registration;
  
// make sure data is not empty
if (
    !empty($dataPerson->ic_no) &&
    !empty($dataPerson->name) &&
    !empty($dataPerson->email) &&
    !empty($dataPerson->mobile_no)
){
  
    //register person
    $registrationPerson->ic_no = $dataRegistration->ic_no;
    $registrationPerson->reg_no = $dataRegistration->reg_no;
    $registrationPerson->readByPatientStatus();

    if (isset($registrationPerson->ic_no)){ //person has already registered to a package plan
        	// record already exist
			http_response_code(409);
  
			// tell the user
			echo json_encode(array("message" => "Person info has already registered as a patient to a package plan","errorFound"=>true,"error" => "409 conflict"));
    }else{
            // person does not registered to any package plan and ready for registration
            // set data property values
            $person->ic_no=$dataPerson->ic_no;
            $person->name=$dataPerson->name;
            $person->age=$dataPerson->age;
            $person->email=$dataPerson->email;
            $person->mobile_no=$dataPerson->mobile_no;
            $person->gender=$dataPerson->gender;
            $person->patient_type_code=$dataPerson->patient_type_code;
            $person->address=$dataPerson->address;
            $person->town=$dataPerson->town;
            $person->district=$dataPerson->district;
            $person->postcode=$dataPerson->postcode;
            $person->state=$dataPerson->state;
            $person->relationship=$dataPerson->relationship;

            $patient_type ="ADULT";
            if ($dataPerson->age < 18) $patient_type= "CHILD";
            if ($dataPerson->age >= 60) $patient_type= "ELDERLY";

            // create the record
            if($person->create()){
                
                $age = 
                //create registration record
                $registrationPerson->ic_no = $dataRegistration->ic_no;
                $registrationPerson->reg_no = $dataRegistration->reg_no;
                $registrationPerson->person_type_code = $dataRegistration->person_type_code;
                $registrationPerson->admin_type = $dataRegistration->admin_type;
                $registrationPerson->patient_type_code = $patient_type;
                $registrationPerson->create();

                // set response code - 201 created
                http_response_code(201);

                // tell the user
                echo json_encode(array("message" => "Person info has been successfully created and registered.","errorFound"=>false,"error" => "",));
            }

            // if unable to create record, tell the user
            else{
                $person->ic_no = $dataPerson->ic_no;
                    // read the details of patient to be edited
                $person->readOne();
                if (isset($person->name)){

                   //create registration record
                    $registrationPerson->ic_no = $dataRegistration->ic_no;
                    $registrationPerson->reg_no = $dataRegistration->reg_no;
                    $registrationPerson->person_type_code = $dataRegistration->person_type_code;
                    $registrationPerson->admin_type = $dataRegistration->admin_type;
                    $registrationPerson->patient_type_code = $patient_type;
                    if ($registrationPerson->create()){
                        // set response code - 201 created
                        http_response_code(201);

                        // tell the user
                        echo json_encode(array("message" => "Person info has been successfully registered.","errorFound"=>false,"error" => "",));

                    }else{
                        // set response code - 503 service unavailable
                        http_response_code(503);

                        // tell the user
                        echo json_encode(array("message" => "Unable to register Person info.","errorFound"=>true,"error" => "Service unavailable"));
 
                    }

                }else{
                    // set response code - 503 service unavailable
                    http_response_code(503);

                    // tell the user
                    echo json_encode(array("message" => "Unable to create Person info.","errorFound"=>true,"error" => "Service unavailable"));
                }

            }
    }
 

  
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to create Person info. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}
?>