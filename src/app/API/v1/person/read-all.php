<?php
/**
 * Author: Majina
 * Person.readAll()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/person/read-all.php
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/person.php';
  
// instantiate database and data object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$person = new Person($db);

// query data
$stmt = $person->readAll();
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $person_arr=array();
    $person_arr["data"]=array();
    
    // retrieve our table contents
    $total_records = 0;
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
  
        $person_item=array(
            "reg_no" => $reg_no,
			"ic_no" => $ic_no,
            "name" => $name, 
            "mobile_no" => $mobile_no, 
            "email" => $mobile_no, 
            "age" => $age, 
            "gender" => $gender, 
            "patient_type_code" => $patient_type_code, 
            "address" => $address, 
            "town" => $town, 
            "district" => $district, 
            "postcode" => $postcode, 
            "state" => $state,
            "relationship" => $relationship
        );
  
        array_push($person_arr["data"], $person_item);
        $total_records++;
    }

    $person_arr["total_records"]=$total_records;
    
    // set response code - 200 OK
    http_response_code(200);
  
    // show data in json format
    echo json_encode($person_arr);
}else{
  
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no record found
    echo json_encode(
        array("message" => "No Person has found.","error" => "404 Not found")
    );
}
?>