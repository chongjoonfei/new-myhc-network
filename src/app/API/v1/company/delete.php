<?php
/**
 * Author: Majina
 * Company.delete()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/company/delete.php
 * JSON input: { "co_reg_no":"<co_reg_no>", "name":"<name>", "address":"<address>", 
 * "town":"<town>", "district":"<district>", "postcode":"<postcode>", "state":"<state>", 
 * "geocode":"<geocode>", "pic_url":"<pic_url>", "contact_no":"<contact_no>", "email":"<email>" }
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object file
include_once '../../config/db.php';
include_once '../../objects/v1/company.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$company = new Company($db);
  
// get data input
$data = json_decode(file_get_contents("php://input"));
  
// set data to be deleted
$company->co_reg_no = $data->co_reg_no;
  
// delete the record
if($company->delete()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Company info was deleted.","error"=>"","errorFound"=>false));
}
  
// if unable to delete the record
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to delete Company info.","error"=>"503 service unavailable","errorFound"=>true));
}
?>