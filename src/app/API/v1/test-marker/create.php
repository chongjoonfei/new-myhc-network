<?php
/**
 * Author: Majina
 * TestMarker.create()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/test-marker/create.php
 * JSON input: { "panel_id":null, "code":"<code>", "name": "<name>", "description":"<description>"}
 * Method: POST   
 */
     
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/test-marker.php';
  
$database = new Database();
$db = $database->getConnection();
  
$testMarker = new TestMarker($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));
  
// make sure data is not empty
if (
    !empty($data->code) &&
    !empty($data->name) 
){
  
    // set data property values
    $testMarker->test_panel_code = $data->test_panel_code;
    $testMarker->code = $data->code;
    $testMarker->name = $data->name;
	$testMarker->description = $data->description;
    $testMarker->unit = $data->unit;
    $testMarker->data_format = $data->data_format;
  
    // create the record
    if($testMarker->create()){
  
        // set response code - 201 created
        http_response_code(201);
  
        // tell the user
        echo json_encode(array("message" => "Test Marker info was created.","errorFound"=>false,"error" => "",));
    }
  
    // if unable to create record, tell the user
    else{
		$testMarker->code = $data->code;
  		// read the details of patient to be edited
		$testMarker->readOne();
		if (isset($testMarker->name)){
			// record already exist
			http_response_code(409);
  
			// tell the user
			echo json_encode(array("message" => "Test Marker info already exist","errorFound"=>true,"error" => "409 conflict"));
		}else{
			// set response code - 503 service unavailable
			http_response_code(503);
  
			// tell the user
			echo json_encode(array("message" => "Unable to create Test Marker info.","errorFound"=>true,"error" => "503 service unavailable"));
		}

    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to create Test Panel info. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}
?>