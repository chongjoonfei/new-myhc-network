<?php
/**
 * Author: Majina
 * TestMarker.readAll()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/test-marker/read-all.php
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/test-marker.php';
  
// instantiate database and data object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$testMarker = new TestMarker($db);

// query data
$stmt = $testMarker->readAll();
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $testMarker_arr=array();
    $testMarker_arr["data"]=array();
    
    // retrieve our table contents
    $total_records = 0;
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
  
        $testMarker_item=array(
            "test_panel_code" => $test_panel_code,
            "code" => $code,
            "name" => $name,
			"description" => $description,
            "unit" => $unit,
            "data_format" => $data_format  
        );
  
        array_push($testMarker_arr["data"], $testMarker_item);
        $total_records++;
    }

    $testMarker_arr["total_records"]=$total_records;
    
    // set response code - 200 OK
    http_response_code(200);
  
    // show data in json format
    echo json_encode($testMarker_arr);
}else{
  
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no record found
    echo json_encode(
        array("message" => "No test marker found.","error" => "404 Not found")
    );
}
?>