<?php
/**
 * Author: Majina
 * TestMarker.delete()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/test-marker/delete.php
 * JSON input: JSON input: { "test_panel_code":<test_panel_code>, "code":"<code>", "name": "<name>", "description":"<description>, "unit":"<unit>"}
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object file
include_once '../../config/db.php';
include_once '../../objects/v1/test-marker.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$testMarker = new TestMarker($db);
  
// get data input
$data = json_decode(file_get_contents("php://input"));
  
// set data to be deleted
$testMarker->code = $data->code;
  
// delete the record
if($testMarker->delete()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Test Marker info was deleted.","error"=>"","errorFound"=>false));
}
  
// if unable to delete the record
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to delete Test Marker info.","error"=>"503 service unavailable","errorFound"=>true));
}
?>