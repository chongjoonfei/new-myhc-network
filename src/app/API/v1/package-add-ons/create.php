<?php
/**
 * Author: Elizha
 * PackageAddOns.create()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/package-add-ons/create.php
 * JSON input: { "package_code":"<package_code>", "add_on_code": "<add_on_code>", "add_on_name":"<add_on_name>", 
 * "test_location_code":"<test_location_code>", "test_location_name":"<test_location_name>", 
 * "total_test_conducted":"<total_test_conducted>", "patient_type_code":"<patient_type_code>"}
 * Method: POST   
 */
     

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/package-add-ons.php';
  
$database = new Database();
$db = $database->getConnection();

// prepare object
$packageAddOns = new PackageAddOns($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));
  
// make sure data is not empty
if (
    !empty($data->add_on_code) &&
    !empty($data->patient_type_code) &&    !empty($data->total_test_conducted)
){
  
    // set data property values
    $packageAddOns->package_code = $data->package_code;
    $packageAddOns->add_on_code = $data->add_on_code;
    $packageAddOns->add_on_name = $data->add_on_name;
    $packageAddOns->test_location_code = $data->test_location_code;
    $packageAddOns->test_location_name = $data->test_location_name;
    $packageAddOns->total_test_conducted = $data->total_test_conducted;
    $packageAddOns->patient_type_code = $data->patient_type_code;


  
    // create the record
    if($packageAddOns->create()){
  
        // set response code - 201 created
        http_response_code(201);
  
        // tell the user
        echo json_encode(array("message" => "Package Add Ons info was created.","errorFound"=>false,"error" => "",));
    }
  
    // if unable to create record, tell the user
    else{
		// $packageAddOns->add_on_code = $data->add_on_code;
  		// read the details of record to be edited
		$packageAddOns->readOne();
		if($packageAddOns->patient_type_code!=null){
			// record already exist
			http_response_code(409);
  
			// tell the user
			echo json_encode(array("message" => "Package Add Ons  info already exist","errorFound"=>true,"error" => "409 conflict"));
		}else{
			// set response code - 503 service unavailable
			http_response_code(503);
  
			// tell the user
			echo json_encode(array("message" => "Unable to create Package Add Ons info.","errorFound"=>true,"error" => "503 service unavailable"));
		}

    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to create Package Add Ons info. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}
?>