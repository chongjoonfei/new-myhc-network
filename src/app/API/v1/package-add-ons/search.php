<?php
/**
 * Author: Elizha
 * PackageAddOns.search()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/package-add-ons/search.php?s=<keyword>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
  
// include database and object files
include_once '../../config/core.php';
include_once '../../config/db.php';
include_once '../../objects/v1/package-add-ons.php';
  
// instantiate database and patient object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$packageAddOns = new PackageAddOns($db);
  
// get keywords
$keywords=isset($_GET["s"]) ? $_GET["s"] : "";
  
// query data
$stmt = $packageAddOns->search($keywords);
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $packageAddOns_arr=array();
    $packageAddOns_arr["data"]=array();
  
    $total_records = 0;
    
    // retrieve our table contents
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
  
        $packageAddOns_rec=array(   
            "package_code"  => $package_code,
            "add_on_code"  => $add_on_code ,
            "add_on_name" => $add_on_name,
            "test_location_code"  => $test_location_code,
            "test_location_name" => $test_location_name,
            "total_test_conducted" =>$total_test_conducted,
            "patient_type_code" =>$patient_type_code
         

        );
  
        array_push($packageAddOns_arr["data"], $packageAddOns_rec);
        $total_records++;
    }
  
    $packageAddOns_arr["total_records"]=$total_records;

    // set response code - 200 OK
    http_response_code(200);
  
    // show data
    echo json_encode($packageAddOns_arr);
}
  
else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not found
    echo json_encode(
        array("message" => "No Package Add Ons found.","errorFound"=>true,"error"=>"404 Not found")
    );
}
?>