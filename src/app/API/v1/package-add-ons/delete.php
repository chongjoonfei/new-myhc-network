<?php
/**
 * Author: Elizha
 * PackageAddOns.delete()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/package-add-ons/delete.php
 * JSON input: { "package_code":"<package_code>", "add_on_code": "<add_on_code>", "add_on_name":"<add_on_name>", 
 * "test_location_code":"<test_location_code>", "test_location_name":"<test_location_name>", 
 * "total_test_conducted":"<total_test_conducted>", "patient_type_code":"<patient_type_code>"}
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object file
include_once '../../config/db.php';
include_once '../../objects/v1/package-add-ons.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$packageAddOns = new PackageAddOns($db);
  
// get data input
$data = json_decode(file_get_contents("php://input"));
  
// set data to be deleted, got 2 composite keys
$packageAddOns->add_on_code = $data->add_on_code;
$packageAddOns->package_code = $data->package_code;

// delete the record
if($packageAddOns->delete()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Package Add Ons info was deleted.","error"=>"","errorFound"=>false));
}
  
// if unable to delete the record
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to delete Package Add Ons info.","error"=>"503 service unavailable","errorFound"=>true));
}
?>