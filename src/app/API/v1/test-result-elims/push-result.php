<?php
/**
 * Author: Majina
 * TestResultElims.pushResult()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/test-result-elims/push-result.php
 * JSON input: [{ "patient_ic_no":<patient_ic_no>, "booking_no":"<booking_no>", "test_marker_code": "<test_marker_code>", "test_date":"<test_date>","test_value":"<test_value>"}]
 * Method: POST   
 */
     
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/test-result-elims.php';
  
$database = new Database();
$db = $database->getConnection();
  
$testResultElims = new TestResultElims($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"),true);

$response_arr=[];

foreach ($data as $item) {
    if (
        !empty($item['patient_ic_no']) &&
        !empty($item['test_marker_code']) &&
        !empty($item['booking_no']) &&
        !empty($item['test_value'])
    ){
        // $response_arr[] = array("message" => "upload in progress.","errorFound"=>false,"error" => "");
        // set data property values
        $testResultElims->test_marker_code = $item['test_marker_code'];
        $testResultElims->patient_ic_no = $item['patient_ic_no'];
        $testResultElims->booking_no = $item['booking_no'];
        $testResultElims->test_date = $item['test_date'];
        $testResultElims->test_value = $item['test_value'];

        // create the record
        if ($testResultElims->create()){
            $response_arr[] = array("message" => $item["booking_no"] ." : ". $item["patient_ic_no"] ." : ".$item['test_marker_code'] ." = " . $item['test_value'] . " -> Test result info was created.","errorFound"=>false,"error" => "");
        }
    
        // if unable to create record, tell the user
        else{
            // read the details of record to be edited
            // $testResultElims->readOne();
            if($testResultElims->patient_ic_no!=null){
                // record already exist and let the record to be updated
                $testResultElims->update();
                $response_arr[] = array("message" =>  $item["booking_no"] ." : ".$item["patient_ic_no"] ." : ".$item['test_marker_code'] ." = " . $item['test_value'] . " ->  Test result info was updated.","errorFound"=>false,"error" => "");
            }else{
                $response_arr[] = array("message" =>  $item["booking_no"] ." : ".$item["patient_ic_no"] ." : ".$item['test_marker_code'] ." = " . $item['test_value'] . " ->  Unable to create Test result info.","errorFound"=>true,"error" => "503 service unavailable");
            }

        }
    }else{
        $response_arr[] = array("message" =>  $item["booking_no"] ." : ".$item["patient_ic_no"] ." : ".$item['test_marker_code'] ." = " . $item['test_value'] . " ->  Unable to create Test result info.","errorFound"=>true,"error" => "503 service unavailable");
    }
}

echo json_encode($response_arr);
   
?>