<?php
/**
 * Author: Majina
 * UserAccount.delete()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/user-account/delete.php
  * JSON input: { "username":"<username>", "password":"<password>", "ic_no": "<ic_no>",
 * 			"acc_type_code":"<acc_type_code>","reg_no":"<reg_no>", "acc_status_code":"<acc_status_code>",
 * 			"date_created":"<date_created>", "date_updated":"<date_updated>"			
 * }
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object file
include_once '../../config/db.php';
include_once '../../objects/v1/user-account.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$userAccount = new UserAccount($db);
  
// get data input
$data = json_decode(file_get_contents("php://input"));
  
// set data to be deleted
$userAccount->username = $data->username;
  
// delete the record
if($userAccount->delete()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Username has been deleted.","error"=>"","errorFound"=>false));
}
  
// if unable to delete the record
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to delete user account.","error"=>"503 service unavailable","errorFound"=>true));
}
?>