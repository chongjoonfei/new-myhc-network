<?php
/**
 * Author: Majina
 * UserAccount.reset-password()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/user-account/reset-password.php
 * JSON input: { "username":"<username>","ic_no":"<ic_no>" }"			
 * }
 * Method: POST   
 */
     
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/user-account.php';
include_once '../../objects/v1/email-mgmt.php';
include_once '../../objects/v1/person.php';
include_once '../../objects/v1/sms-notification.php';
  
$database = new Database();
$db = $database->getConnection();
  
$userAccount = new UserAccount($db);
$emailMgmt = new EmailMgmt($db);
$person = new Person($db);
$smsNotification = new SmsNotification($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));
  
// make sure data is not empty
if (
    !empty($data->ic_no) &&
	!empty($data->username) 
){
  
    // set data property values
    $userAccount->username = $data->username;
	$userAccount->ic_no = $data->ic_no;

	$userAccount->readOne();

	if (isset($userAccount->username)){
		$password = $userAccount->randomPassword(8);
		$userAccount->password = $userAccount->encryptPassword($password);
		// create the record
		if ($userAccount->changePassword()){

			$person->ic_no =  $data->ic_no;
			$person->readOne();
			
			//send sms notification
			$smsNotification->code = "password-reset";
			$smsNotification->readOne();
			$smsNotification->mobile_no = "60". $person->mobile_no;
			$smsNotification->paramValues =  array( 
				array("param" => "?username", "value" => $userAccount->username),
				array("param" => "?password", "value" => $password )
			);
			$smsNotification->sendSms();

			//send email
			$emailMgmt->code = "password-reset";
			$emailMgmt->readOne();
			$emailMgmt->receivers = $person->email;
			$emailMgmt->paramValues = $smsNotification->paramValues;
			$emailMgmt->sendMail();


			// set response code - 200 Ok
			http_response_code(200);

			// tell the user
			echo json_encode(array("message" => "User password has been reset.","errorFound"=>false,"error" => "",));

		 
	
		}else{
			// set response code - 400 bad request
			http_response_code(400);
		
			// tell the user
			echo json_encode(array("message" => "Unable to reset password due to technical error","errorFound"=>true,"error" => "400 bad request"));
		}
	}else{
		// set response code - 503 service unavailable
		http_response_code(503);

		// tell the user
		echo json_encode(array("message" => "User account does not exist.","errorFound"=>true,"error" => "Service unavailable"));

	}
  
 
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to create new user account. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}
?>