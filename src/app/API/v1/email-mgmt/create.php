<?php
/**
 * Author: Majina
 * EmailMgmt.create()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/email-mgmt/create.php
 * JSON input: { "code":"<code>", "title":"<title>", "content": "<content>", "sender": "<sender>"}
 * Method: POST   
 */
     
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/email-mgmt.php';
  
$database = new Database();
$db = $database->getConnection();
  
$emailMgmt = new EmailMgmt($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));
  
// make sure data is not empty
if (
    !empty($data->code) 
){
  
    // set data property values
    $emailMgmt->code = $data->code;
	$emailMgmt->title = $data->title;
    $emailContent->content = $data->content;
    $emailContent->sender = $data->sender;
  
    // create the record
    if($emailMgmt->create()){
  
        // set response code - 201 created
        http_response_code(201);
  
        // tell the user
        echo json_encode(array("message" => "Email Content info was created.","errorFound"=>false,"error" => "",));
    }
  
    // if unable to create record, tell the user
    else{
		$emailMgmt->code = $data->code;
  		// read the details of patient to be edited
		$emailMgmt->readOne();
		if (isset($emailMgmt->title)){
			// record already exist
			http_response_code(409);
  
			// tell the user
			echo json_encode(array("message" => "Email Content info already exist","errorFound"=>true,"error" => "409 conflict"));
		}else{
			// set response code - 503 service unavailable
			http_response_code(503);
  
			// tell the user
			echo json_encode(array("message" => "Unable to create Email Content info.","errorFound"=>true,"error" => "503 service unavailable"));
		}

    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to create Package Category info. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}
?>