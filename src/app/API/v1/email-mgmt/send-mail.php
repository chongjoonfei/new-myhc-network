<?php
/**
 * Author: Majina
 * EmailMgmt.testSendMail()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/email-mgmt/send-mail.php
 * JSON input: { "code":"<code>", "receivers":"<email1>,<email2>", "paramValues":[{"param":"<param>","value":"<value>"}] }
 * Method: GET   
 */

 
ini_set('display_errors',1);
error_reporting(E_ALL);

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/email-mgmt.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$emailMgmt = new EmailMgmt($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));

$emailMgmt->code = $data->code;
$emailMgmt->receivers = $data->receivers;
  
// read the details of data to be edited
$emailMgmt->readOne();
  
if($emailMgmt->code!=null){
 
    $emailMgmt->paramValues = $data->paramValues;

    if ( $emailMgmt->sendMail()){
        // set response code - 200 OK
        http_response_code(200);
    
        // make it json format
        echo json_encode(array("message" => "An email has been sent to your mailbox","error" => "200 Success"));
    }else{
       // set response code - 200 OK
       http_response_code(200);
    
       // make it json format
       echo json_encode(array("message" => "Failed to send email","error" => "200 Failed"));

    }

}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "Email Content info does not exist for " . $data->code,"error" => "404 Not found"));
}
?>