<?php
/**
 * Author: Majina
 * SmsNotification.send()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/sms-notification/send.php
 * JSON input: { "message":"<message>"}
 * Method: POST   
 */
     
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/sms-notification.php';
  
$database = new Database();
$db = $database->getConnection();
  
$smsNotification = new SmsNotification($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));
  
// make sure data is not empty
if (
    !empty($data->message) 
){
 

    $urlSmsGateway = "http://dumei.bulksms2u.com/websmsapi/ISendSMS.aspx";

    //http://dumei.bulksms2u.com/websmsapi/ISendSMS.aspx?username=myhc&password=berkat*2021&message=RM0+myhc+Test+message&mobile=60128571385&Sender=Test&type=1

    //The data you want to send via POST
    $fields = [
        'username' => 'myhc',
        'password' => 'berkat*2021',
        'message' => 'RM0 myhc: '.$data->message,
        'mobile' => $data->mobile,
        'sender' => 'test',
        'type' => '1'
    ];
  
    // send the message
    $response = httpPost($urlSmsGateway, $fields);
    if(strpos($response,'1701')!==false){
  
        // set response code - 200 ok
        http_response_code(200);
  
        // tell the user
        echo json_encode(array("message" => "SMS Notification has been sent to your mobile phone " . $data->mobile,"errorFound"=>false,"error" => ""));
    }
  
    // if unable to create record, tell the user
    else{
 
        // set response code - 503 service unavailable
        http_response_code(503);

        // tell the user
        echo json_encode(array("message" => $response." : Unable to send SMS Notification.","errorFound"=>true,"error" => "503 service unavailable"));

    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to send SMS Notification due to incomplete information.","errorFound"=>true,"error" => "400 bad request"));
}

function httpPost($url, $data)
{
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($curl);
    curl_close($curl);
    return $response;
}
?>