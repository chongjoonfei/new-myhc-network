<?php

ini_set('display_errors', 1); 
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
mysqli_report(MYSQLI_REPORT_ALL);

/**
 * Author: Elizha
 * Timeslot.readAll()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/timeslot/read-all.php
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");

// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/transaction.php';

  
// instantiate database and data object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$transaction = new Transaction($db);


// query data
$stmt = $transaction->readAll();
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $record_arr=array();
    $record_arr["data"]=array();
    
    // retrieve our table contents
    $total_records = 0;
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
         
        $record_item=array(
            "trans_no" => $trans_no,
            "payment_id" => $payment_id, 
            "payment_date" => $payment_date, 
            "ref_no" => $ref_no, 
            "booking_no" => $booking_no, 
            "amount" => $amount, 
            "status" => $status, 
            "remark" => $remark
        );
  
        array_push($record_arr["data"], $record_item);
        $total_records++;
    }

    $record_arr["total_records"]=$total_records;
    
    // set response code - 200 OK
    http_response_code(200);
  
    // show data in json format
    echo json_encode($record_arr);
}else{
  
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no record found
    echo json_encode(
        array("message" => "No Transaction found.","error" => "404 Not found")
    );
}
?>