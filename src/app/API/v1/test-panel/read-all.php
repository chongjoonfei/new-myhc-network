<?php
/**
 * Author: Majina
 * TestPanel.readAll()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/test-panel/read-all.php
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/test-panel.php';
include_once '../../objects/v1/test-marker.php';
  
// instantiate database and data object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$testPanel = new TestPanel($db);
$testMarker = new TestMarker($db);

// query data
$stmt = $testPanel->readAll();
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $testPanel_arr=array();
    $testPanel_arr["data"]=array();
    
    // retrieve our table contents
    $total_records = 0;
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
  
        $testPanel_item=array(
            "panel_id" => $panel_id,
            "test_panel_code" => $test_panel_code,
            "name" => $name,
			"description" => $description ,
            "input_type" => $input_type ,
            "test_markers" => $testMarker->readByPanelCode($test_panel_code)
        );
  
        array_push($testPanel_arr["data"], $testPanel_item);
        $total_records++;
    }

    $testPanel_arr["total_records"]=$total_records;
    
    // set response code - 200 OK
    http_response_code(200);
  
    // show data in json format
    echo json_encode($testPanel_arr);
}else{
  
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no record found
    echo json_encode(
        array("message" => "No test panel found.","error" => "404 Not found")
    );
}
?>