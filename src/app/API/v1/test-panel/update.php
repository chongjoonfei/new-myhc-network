<?php
/**
 * Author: Majina
 * TestPanel.update()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/test-panel/update.php
 * JSON input: { "panel_id":<id>, "test_panel_code":"<test_panel_code>", "name": "<name>", 
 *              "description":"<description>","input_type":"<input_type>"}
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/test-panel.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$testPanel = new TestPanel($db);
  
// get id of record to be edited
$data = json_decode(file_get_contents("php://input"));
  
// set ID property of record to be edited
$testPanel->test_panel_code = $data->test_panel_code;
  
// set data property values
$testPanel->name = $data->name;
$testPanel->description = $data->description;
$testPanel->input_type = $data->input_type;
  
// update the record
if($testPanel->update()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Test Panel info was updated.","errorFound"=>false,"error"=>"success"));
}
  
// if unable to update the record, tell the user
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to update Test Panel info.","errorFound"=>true,"error"=>"503 service unavailable"));
}
?>