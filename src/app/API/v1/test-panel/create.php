<?php
/**
 * Author: Majina
 * TestPanel.create()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/test-panel/create.php
 * JSON input: { "panel_id":null, "test_panel_code":"<test_panel_code>", "name": "<name>", 
 *              "description":"<description>","input_type":"<input_type>"}
 * Method: POST   
 */
     
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/test-panel.php';
  
$database = new Database();
$db = $database->getConnection();
  
$testPanel = new TestPanel($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));
  
// make sure data is not empty
if (
    !empty($data->test_panel_code) &&
    !empty($data->name) 
){
  
    // set data property values
	//$testPanel->panel_id = $data->panel_id;
    $testPanel->test_panel_code = $data->test_panel_code;
    $testPanel->name = $data->name;
	$testPanel->description = $data->description;
    $testPanel->input_type = $data->input_type;
  
    // create the record
    if($testPanel->create()){
  
        // set response code - 201 created
        http_response_code(201);
  
        // tell the user
        echo json_encode(array("message" => "Test panel info was created.","errorFound"=>false,"error" => "",));
    }
  
    // if unable to create record, tell the user
    else{
		$testPanel->test_panel_code = $data->test_panel_code;
  		// read the details of record to be edited
		$testPanel->readOne();
		if (isset($testPanel->name)){
			// record already exist
			http_response_code(409);
  
			// tell the user
			echo json_encode(array("message" => "Test Panel info already exist","errorFound"=>true,"error" => "409 record already exist"));
		}else{
			// set response code - 503 service unavailable
			http_response_code(503);
  
			// tell the user
			echo json_encode(array("message" => "Unable to create Test Panel info.","errorFound"=>true,"error" => "503 service unavailable"));
		}

    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to create Test Panel info. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}
?>