<?php
/**
 * Author: Majina
 * TestPanel.readOne()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/test-panel/read-one.php?c=<code>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/test-panel.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$testPanel = new TestPanel($db);
  
// set ID property of record to read
$testPanel->test_panel_code = isset($_GET['c']) ? $_GET['c'] : die();
  
// read the details of data to be edited
$testPanel->readOne();
  
if (isset($testPanel->panel_id)){
    // create array
    $testPanel_arr = array(
        "panel_id" =>  $testPanel->panel_id,
        "test_panel_code" => $testPanel->test_panel_code,
        "name" => $testPanel->name ,
		"description" => $testPanel->description,
        "input_type" => $testPanel->input_type
    );
  
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($testPanel_arr);
}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "Test Panel info does not exist for " . $testPanel->test_panel_code,"error" => "404 Not found"));
}
?>