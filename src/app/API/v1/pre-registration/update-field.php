<?php

ini_set('display_errors', 1); 
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
mysqli_report(MYSQLI_REPORT_ALL);

/**
 * Author: Majina
 * PreRegistration.updateStatus()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/pre-registration/update-status.php
 * JSON input: { "reg_no":"<reg_no>","status": "<status>" }
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/pre-registration.php';

include_once '../../objects/v1/user-account.php';
include_once '../../objects/v1/sms-notification.php';
include_once '../../objects/v1/email-mgmt.php';
include_once '../../objects/v1/screening-plan.php';
include_once '../../objects/v1/package-category.php';
include_once '../../objects/v1/registration-person.php';

  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$preRegistration = new PreRegistration($db);

$screeningPlan = new ScreeningPlan($db);
$userAccount = new UserAccount($db);
$smsNotification = new SmsNotification($db);
$emailMgmt = new EmailMgmt($db);
$packageCategory = new PackageCategory($db);


// get id of record to be edited
$data = json_decode(file_get_contents("php://input"));
  
// set ID property of record to be edited
$preRegistration->reg_no=$data->reg_no;
$field_name=$data->field_name;
$field_value=$data->field_value;

// update the record
if ($preRegistration->updateFieldByRegNo($field_name,$field_value)){
    // if (true){
    $preRegistration->reg_no=$data->reg_no;
    $preRegistration->readOne();

    //get selected screening package plan
    $screeningPlan->package_code = $preRegistration->package_code;
    $screeningPlan->readOne();

    //get selected package category
    $packageCategory->category_code = $screeningPlan->category_code;
    $packageCategory->readOne();


    if ($screeningPlan->category_code=='CORPORATE'){

        if (strtolower($field_name)=="status" && $field_value=="APPROVED"){
        
            $registrationPerson = new RegistrationPerson($db);
            $stmt = $registrationPerson->readByRegNoAndPerson_Stmt($data->reg_no);		

            $siri=$stmt->rowCount();

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                extract($row);
                $id = sprintf( '%005d', $preRegistration->seq_reg_no );

                if ($admin_type!='MAU'){ //create account for each corporate staff
                    //create user account
                    $prefix = 'P';

                    $userAccount->username = $packageCategory->prefix . $id . '-'.$siri;  //P = prefix utk portal patient, id = id seq reg no,
                    $password = $userAccount->randomPassword(8);
                    $userAccount->password = $userAccount->encryptPassword($password);
                    $userAccount->ic_no = $ic_no;
                    $userAccount->reg_no = $data->reg_no;
                    $userAccount->acc_type_code = "STAFF";
                    $userAccount->acc_status_code = "APPROVED";
                    // $userAccount->menu_owner = $screeningPlan->category_code .'-STAFF';
                    $userAccount->reg_no = $preRegistration->reg_no;
                    $userAccount->date_created = date('Y-m-d H:i:s');

                    if ($userAccount->create()){
                    // if (false){   

                        //send sms notification
                        $smsNotification->code = "acc-staff-approved";
                        $smsNotification->readOne();
                        if (isset($smsNotification->code)){
                            $smsNotification->mobile_no = "60". $mobile_no;
                            $smsNotification->paramValues =  array( 
                                array("param" => "?username", "value" =>$userAccount->username) ,
                                array("param" => "?password", "value" =>$password) 
                            );
                            $smsNotification->sendSms();
                        }

                        //send email
                        $emailMgmt->code = "acc-staff-approved";
                        $emailMgmt->readOne();
                        if (isset($emailMgmt->sender)){
                            $emailMgmt->receivers = $email;
                            $emailMgmt->paramValues = $smsNotification->paramValues;
                            $emailMgmt->sendMail();
                        }

                    }
                    $siri=$siri+1;
                }else{
                    //update MAU status account to approved
                    $userAccount->username = $packageCategory->prefix . $id;
                    $userAccount->acc_status_code = "APPROVED";
                    $userAccount->changeStatus();

                    //send sms notification
                    $smsNotification->code = "acc-mau-approved";
                    $smsNotification->readOne();
                    if (isset($smsNotification->code)){
                        $smsNotification->mobile_no = "60". $mobile_no;
                        $smsNotification->paramValues =  array( 
                            array("param" => "?reg_no", "value" =>$data->reg_no) 
                        );
                        $smsNotification->sendSms();
                    }


                    //send email
                    $emailMgmt->code = "acc-mau-approved";
                    $emailMgmt->readOne();
                    if (isset($emailMgmt->code)){
                        $emailMgmt->receivers = $email;
                        $emailMgmt->paramValues = $smsNotification->paramValues;
                        $emailMgmt->sendMail();
                    }
                }
            }
        }

    } 

    if ($screeningPlan->category_code=='FAMILY'){
        //create another MAU acc for spouse
        if (strtolower($field_name)=="status" && $field_value=="APPROVED"){
            $registrationPerson = new RegistrationPerson($db);
            $stmt = $registrationPerson->readByRegNoAndPerson_Stmt($data->reg_no);		

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                extract($row);
                $id = sprintf( '%005d', $preRegistration->seq_reg_no );

                if ($admin_type!='MAU' && $patient_type_code=='ADULT'){ //create account for spouse
                    //create user account
                    $prefix = 'P';

                    $userAccount->username = $packageCategory->prefix . $id . '-1';  //P = prefix utk portal patient, id = id seq reg no,
                    $password = $userAccount->randomPassword(8);
                    $userAccount->password = $userAccount->encryptPassword($password);
                    $userAccount->ic_no = $ic_no;
                    $userAccount->acc_type_code = "MAU";
                    $userAccount->acc_status_code = "APPROVED";
                    $userAccount->menu_owner = $screeningPlan->category_code;
                    $userAccount->reg_no = $preRegistration->reg_no;
                    $userAccount->date_created = date('Y-m-d H:i:s');

                    if ($userAccount->create()){

                        //send sms notification
                        $smsNotification->code = "acc-mau-approved";
                        $smsNotification->readOne();
                        if (isset($smsNotification->code)){
                            $smsNotification->mobile_no = "60". $mobile_no;
                            $smsNotification->paramValues =  array( 
                                array("param" => "?reg_no", "value" =>$data->reg_no) 
                            );
                            $smsNotification->sendSms();
                        }

                        //send email
                        $emailMgmt->code = "acc-mau-approved";
                        $emailMgmt->readOne();
                        if (isset($emailMgmt->sender)){
                            $emailMgmt->receivers = $email;
                            $emailMgmt->paramValues = $smsNotification->paramValues;
                            $emailMgmt->sendMail();
                        }

                    }

                }else{
                    //update MAU status account to approved
                    $userAccount->username = $packageCategory->prefix . $id;
                    $userAccount->acc_status_code = "APPROVED";
                    $userAccount->changeStatus();

                    //send sms notification
                    $smsNotification->code = "acc-mau-approved";
                    $smsNotification->readOne();
                    if (isset($smsNotification->code)){
                        $smsNotification->mobile_no = "60". $mobile_no;
                        $smsNotification->paramValues =  array( 
                            array("param" => "?reg_no", "value" =>$data->reg_no) 
                        );
                        $smsNotification->sendSms();
                    }


                    //send email
                    $emailMgmt->code = "acc-mau-approved";
                    $emailMgmt->readOne();
                    if (isset($emailMgmt->code)){
                        $emailMgmt->receivers = $email;
                        $emailMgmt->paramValues = $smsNotification->paramValues;
                        $emailMgmt->sendMail();
                    }
                }
            }

        }
    }
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Registration $field_name has been updated.","errorFound"=>false,"error"=>"success"));
}
  
// if unable to update the record, tell the user
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to update registration $field_name.","errorFound"=>true,"error"=>"503 service unavailable"));
}
?>