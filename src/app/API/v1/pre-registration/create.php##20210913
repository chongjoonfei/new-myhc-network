<?php
/**
 * Author: Majina
 * Registration.preRegistration()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/pre-registration/create.php
 * JSON input: { "ic_no":<ic_no>, "mobile_no":"<mobile_no>", "name": "<name>", "email":"<email>", "package_code":"<package_code>"}
 * Method: POST   
 */
     
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/pre-registration.php';
include_once '../../objects/v1/user-account.php';
include_once '../../objects/v1/sms-notification.php';
include_once '../../objects/v1/email-mgmt.php';
include_once '../../objects/v1/screening-plan.php';
include_once '../../objects/v1/package-category.php';
 
  
$database = new Database();
$db = $database->getConnection();
  
$preRegistration = new PreRegistration($db);
$screeningPlan = new ScreeningPlan($db);
$userAccount = new UserAccount($db);
$smsNotification = new SmsNotification($db);
$emailMgmt = new EmailMgmt($db);
$packageCategory = new PackageCategory($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));
  
// make sure data is not empty
if (
    !empty($data->name) &&
    !empty($data->ic_no) &&
    !empty($data->mobile_no) &&
    !empty($data->email) &&
    !empty($data->package_code)  
){
  
    // set data property values
    $preRegistration->ic_no = $data->ic_no;
    $preRegistration->package_code = $data->package_code;
    $preRegistration->readOneByIcNoAndPackageCode();
    if($preRegistration->name!=null){ //record does exist
			// record already exist
			http_response_code(409);
  
			// tell the user
			echo json_encode(array("message" => "Registration info under your name with selected package plan has already exist","errorFound"=>true,"error" => "409 record already exist"));
    }else{
        $preRegistration->ic_no = $data->ic_no;
        // $preRegistration->reg_no = $data->reg_no;
        $preRegistration->name = $data->name;
        $preRegistration->mobile_no = $data->mobile_no;
        $preRegistration->email = $data->email;
        $preRegistration->package_code = $data->package_code;
        $preRegistration->amount_paid = $data->amount_paid;
        $preRegistration->payment_no = $data->payment_no;
        $preRegistration->payment_method = $data->payment_method;
        $preRegistration->payment_date = $data->payment_date;
        $preRegistration->date_expired = $data->date_expired;
        $preRegistration->center_code = $data->center_code;
        $preRegistration->status = "PRE-ACCOUNT";

        // create the record
        if ($preRegistration->create()){

            $preRegistration->ic_no = $data->ic_no;
            $preRegistration->package_code = $data->package_code;
            $preRegistration->readOne();

            //get selected screening package plan
            $screeningPlan->package_code = $preRegistration->package_code;
            $screeningPlan->readOne();

            //get selected package category
            $packageCategory->category_code = $screeningPlan->category_code;
            $packageCategory->readOne();

            //create user account
            $id = sprintf( '%005d', $preRegistration->seq_reg_no );
            $userAccount->username = $packageCategory->prefix . $id;
            $password = $userAccount->randomPassword(8);
            $userAccount->password = $userAccount->encryptPassword($password);
            $userAccount->ic_no = $preRegistration->ic_no;
            if ($screeningPlan->category_code=='FAMILY') $userAccount->patient_type_code = "ADULT";
            $userAccount->acc_type_code = "MAU";
            $userAccount->acc_status_code = "PRE-ACCOUNT";
            $userAccount->menu_owner = $screeningPlan->category_code;
            $userAccount->reg_no = $preRegistration->reg_no;
            $userAccount->date_created = date('Y-m-d H:i:s');

            if ($userAccount->create()){

                //send sms notification
                $smsNotification->code = "reg-pre-acc-created";
                $smsNotification->readOne();
                $smsNotification->mobile_no = "60". $preRegistration->mobile_no;
                $smsNotification->paramValues =  array( 
                    array("param" => "?username", "value" => $userAccount->username),
                    array("param" => "?password", "value" => $password )
                );
                $smsNotification->sendSms();

                //send email
                $emailMgmt->code = "reg-pre-acc-created";
                $emailMgmt->readOne();
                $emailMgmt->receivers = $preRegistration->email;
                $emailMgmt->paramValues = $smsNotification->paramValues;
                $emailMgmt->sendMail();

            }

    
            // set response code - 201 created
            http_response_code(201);
    
            // tell the user
            echo json_encode(array("message" => "Pre-registration successfully completed","errorFound"=>false,"error" => "",));
        }else{
			// set response code - 503 service unavailable
			http_response_code(503);
  
			// tell the user
			echo json_encode(array("message" => "Unable to process pre-registration.","errorFound"=>true,"error" => "503 service unavailable"));
        }
    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to process pre-registration. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}

 
 
?>