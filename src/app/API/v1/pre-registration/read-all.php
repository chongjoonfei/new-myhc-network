<?php
/**
 * Author: Majina
 * PreRegistration.readAll()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/pre-registration/read-all.php
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/pre-registration.php';
include_once '../../objects/v1/registration-person.php';
include_once '../../objects/v1/screening-plan.php';
include_once '../../objects/v1/company.php';
  
// instantiate database and data object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$preRegistration = new PreRegistration($db);
$company = new Company($db);

// query data
$stmt = $preRegistration->readAll();
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){

    $registrationPerson = new RegistrationPerson($db);
    $screeningPlan = new ScreeningPlan($db);
  
    // record array
    $record_arr=array();
    $record_arr["data"]=array();
    
    // retrieve our table contents
    $total_records = 0;
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);

        $record_item = array(
            "seq_reg_no" => $seq_reg_no,
            "reg_no" => $reg_no,
            "ic_no" => $ic_no,
            "name" => $name,
            "mobile_no" => $mobile_no,
            "email" => $email,
            "package_code" => $package_code,
            "amount_paid" => $amount_paid,
            "payment_no" => $payment_no,
            "payment_method" => $payment_method,
            "payment_date" => $payment_date,
            "date_registered" => $date_registered,
            "date_expired" => $date_expired,
            "center_code" => $center_code,
            "status" => $status,
            "company_reg_no" => $company_reg_no,
            "company" => $company->readByCoRegNo($company_reg_no),
            "registration_persons"=> $registrationPerson->readByRegNo($reg_no),
            "screening_plan"=> $screeningPlan->readByPackageCode($package_code)
        );
  
        array_push($record_arr["data"], $record_item);
        $total_records++;
    }

    $record_arr["total_records"]=$total_records;
    
    // set response code - 200 OK
    http_response_code(200);
  
    // show data in json format
    echo json_encode($record_arr);
}else{
  
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no record found
    echo json_encode(
        array("message" => "No registration found.","error" => "Not found")
    );
}
?>