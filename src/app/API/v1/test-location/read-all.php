<?php
/**
 * Author: Elizha
 * TestLocation.readAll()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/test-location/read-all.php
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");

// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/test-location.php';
//include_once '../../objects/v1/test-marker.php';
  
// instantiate database and data object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$testLocation = new TestLocation($db);
//$testMarker = new TestMarker($db);

// query data
$stmt = $testLocation->readAll();
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $testLocation_arr=array();
    $testLocation_arr["data"]=array();
    
    // retrieve our table contents
    $total_records = 0;
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
         
        $testLocation_item=array(
            "code"  => $code,
            "name"  => $name 
           
              //"test_markers" => $testMarker->readByPanelCode($code)
        );
  
        array_push($testLocation_arr["data"], $testLocation_item);
        $total_records++;
    }

    $testLocation_arr["total_records"]=$total_records;
    
    // set response code - 200 OK
    http_response_code(200);
  
    // show data in json format
    echo json_encode($testLocation_arr);
}else{
  
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no record found
    echo json_encode(
        array("message" => "No Test Location found.","error" => "404 Not found")
    );
}
?>