<?php
/**
 * Author: Elizha
 * PackagePatient.create()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/package-patient/create.php
 * JSON input: { "package_code":"<package_code>", "patient_type_code": "<patient_type_code>", "total_patient":"<total_patient>"}
 * Method: POST   
 */

     
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/package-patient.php';
  
$database = new Database();
$db = $database->getConnection();

// prepare object
$packagePatient = new PackagePatient($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));
  
// make sure data is not empty
if (
    !empty($data->package_code) &&
    !empty($data->patient_type_code) 
){
  
    // set data property values
    $packagePatient->package_code = $data->package_code;
    $packagePatient->patient_type_code = $data->patient_type_code;
	$packagePatient->total_patient = $data->total_patient;
    $packagePatient->doc_rquired = $data->doc_rquired;

  
    // create the record
    if($packagePatient->create()){
  
        // set response code - 201 created
        http_response_code(201);
  
        // tell the user
        echo json_encode(array("message" => "Package Patient info was created.","errorFound"=>false,"error" => "",));
    }
  
    // if unable to create record, tell the user
    else{
		// $packagePatient->patient_type_code = $data->patient_type_code;
  		// read the details of record to be edited
		$packagePatient->readOne();
		if (isset($packagePatient->package_code)){
			// record already exist
			http_response_code(409);
  
			// tell the user
			echo json_encode(array("message" => "Package Patient info already exist","errorFound"=>true,"error" => "409 conflict"));
		}else{
			// set response code - 503 service unavailable
			http_response_code(503);
  
			// tell the user
			echo json_encode(array("message" => "Unable to create Package Patient info.","errorFound"=>true,"error" => "503 service unavailable"));
		}

    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to create Add On Services info. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}
?>