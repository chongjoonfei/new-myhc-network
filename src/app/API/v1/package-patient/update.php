<?php
/**
 * Author: Elizha
 * PackagePatient.update()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/package-patient/update.php 
 * JSON input: { "package_code":"<package_code>", "patient_type_code": "<patient_type_code>", "total_patient":"<total_patient>"}
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/package-patient.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$packagePatient = new PackagePatient($db);
  
// get id of record to be edited
$data = json_decode(file_get_contents("php://input"));
  
// set ID property of record to be edited, put 2 PK
$packagePatient->package_code = $data->package_code;
$packagePatient->patient_type_code = $data->patient_type_code;
  
// set data property values
$packagePatient->total_patient = $data->total_patient;
$packagePatient->doc_required = $data->doc_required;
  
// update the record
if($packagePatient->update()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Package Patient info was updated.","errorFound"=>false,"error"=>"success"));
}
  
// if unable to update the record, tell the user
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to update Package Patient info.","errorFound"=>true,"error"=>"503 service unavailable"));
}
?>