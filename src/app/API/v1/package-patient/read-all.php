<?php
/**
 * Author: Elizha
 * PackagePatient.readAll()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/package-patient/read-all.php
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");

// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/package-patient.php';
//include_once '../../objects/v1/test-marker.php';
  
// instantiate database and data object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$packagePatient = new PackagePatient($db);
//$testMarker = new TestMarker($db);

// query data
$stmt = $packagePatient->readAll();
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $packagePatient_arr=array();
    $packagePatient_arr["data"]=array();
    
    // retrieve our table contents
    $total_records = 0;
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
  
        $packagePatient_item=array(
            "package_code"  => $package_code,
            "patient_type_code"  => $patient_type_code ,
            "total_patient" => $total_patient,
            "doc_required" => $doc_required
              //"test_markers" => $testMarker->readByPanelCode($code)
        );

  
        array_push($packagePatient_arr["data"], $packagePatient_item);
        $total_records++;
    }

    $packagePatient_arr["total_records"]=$total_records;
    
    // set response code - 200 OK
    http_response_code(200);
  
    // show data in json format
    echo json_encode($packagePatient_arr);
}else{
  
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no record found
    echo json_encode(
        array("message" => "No Package Patient found.","error" => "404 Not found")
    );
}
?>