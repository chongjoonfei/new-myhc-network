<?php
/**
 * Author: Elizha
 * Payment.delete()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/payment/delete.php
 * JSON input: { "ref_no":"<ref_no>", "amount": "<amount>", "status":"<status>", "remark":"<remark>", "method":"<method>" }
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object file
include_once '../../config/db.php';
include_once '../../objects/v1/payment.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$payment = new Payment($db);
  
// get data input
$data = json_decode(file_get_contents("php://input"));
  
// set data to be deleted
$payment->ref_no = $data->ref_no;


// delete the record
if($payment->delete()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Payment info was deleted.","error"=>"","errorFound"=>false));
}
  
// if unable to delete the record
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to delete Payment info.","error"=>"503 service unavailable","errorFound"=>true));
}
?>