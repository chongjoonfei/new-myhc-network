<?php
/**
 * Author: Elizha
 * Payment.readAll()
 * URL for testing : https://myhc.my/myhc-api/v1/payment/read-all.php
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");

// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/payment.php';

  
// instantiate database and data object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$payment = new Payment($db);


// query data
$stmt = $refMessage->readAll();
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $payment_arr=array();
    $payment_arr["data"]=array();
    
    // retrieve our table contents
    $total_records = 0;
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
         
        $payment_item=array(
            "trans_no"  => $trans_no,
            "payment_id"  => $payment_id,
            "payment_date"  => $payment_date,
            "booking_no"  => $booking_no,
            "ref_no"  => $ref_no,
            "amount"  => $amount,
            "status"  => $status,
            "remark"  => $remark,
 
        );
 
  
        array_push($payment_arr["data"], $payment_item);
        $total_records++;
    }

    $payment_arr["total_records"]=$total_records;
    
    // set response code - 200 OK
    http_response_code(200);
  
    // show data in json format
    echo json_encode($payment_arr);
}else{
  
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no record found
    echo json_encode(
        array("message" => "No Payment found.","error" => "404 Not found")
    );
}
?>