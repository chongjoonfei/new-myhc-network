<?php

ini_set('display_errors', 1); 
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
mysqli_report(MYSQLI_REPORT_ALL);

/**
 * Author: Elizha
 * Relationship.readAll()
 * URL for testing : https://myhc.my/myhc-api/v1/screening-center/read-all.php
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");

// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/screening-center.php';
include_once '../../objects/v1/screening-center-timeslot.php';

  
// instantiate database and data object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$screeningCenter = new ScreeningCenter($db);
$centerTimeslot = new ScreeningCenterTimeslot($db);


// query data
$stmt = $screeningCenter->readAll();
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $record_arr=array();
    $record_arr["data"]=array();
    
    // retrieve our table contents
    $total_records = 0;
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
         
        $record_item=array(
            "sc_id" => $sc_id,
            "sc_name" => $sc_name,
            "sc_address" => $sc_address,
            "sc_postcode" => $sc_postcode,
            "sc_state" => $sc_state,
            "sc_telephone" => $sc_telephone,
            "sc_person_in_charge" => $sc_person_in_charge,
            "sc_picture_path" => $sc_picture_path,
            "timeslots" => $centerTimeslot->readByScreeningCenterId($sc_id)
        );
  
        array_push($record_arr["data"], $record_item);
        $total_records++;
    }

    $record_arr["total_records"]=$total_records;
    
    // set response code - 200 OK
    http_response_code(200);
  
    // show data in json format
    echo json_encode($record_arr);
}else{
  
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no record found
    echo json_encode(
        array("message" => "No screening center found.","error" => "404 Not found")
    );
}
?>