<?php
/**
 * Author: Majina
 * ScreeningCenter.create()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/screening-center/create.php
 * JSON input: { "sc_id":"<sc_id>", "sc_name":"<sc_name>", "sc_address":"<sc_address>, "sc_postcode":"<sc_postcode>", "sc_state":"<sc_state>", "sc_telephone":"<sc_telephone>", "sc_person_in_charge":"<sc_person_in_charge>", "sc_picture_path":"<sc_picture-path>"}
 * Method: POST   
 */
     
ini_set('display_errors', 1); 
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
mysqli_report(MYSQLI_REPORT_ALL);

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/screening-center.php';
include_once '../../objects/v1/screening-center-timeslot.php';
  
$database = new Database();
$db = $database->getConnection();
  
$screeningCenter = new ScreeningCenter($db);
 
  
// get posted data
$data = json_decode(file_get_contents("php://input"));

//echo json_encode(array($data));
 

// make sure data is not empty
if (
    !empty($data->sc_name) 
){
  
    // set data property values
    $screeningCenter->sc_id = $data->sc_id;
	$screeningCenter->sc_name = $data->sc_name;
    $screeningCenter->sc_address = $data->sc_address;
    $screeningCenter->sc_postcode = $data->sc_postcode;
    $screeningCenter->sc_state = $data->sc_state;
    $screeningCenter->sc_telephone = $data->sc_telephone;
    $screeningCenter->sc_person_in_charge = $data->sc_person_in_charge;
    $screeningCenter->sc_picture_path = $data->sc_picture_path;
   
    // create the record
    if($screeningCenter->create()){
        // set response code - 201 created
        http_response_code(201);

        $timeslots  = $data->timeslots;
        foreach ($timeslots as $timeslot) {
            $sctimeslot = new ScreeningCenterTimeslot($db);
            $sctimeslot->sc_id =  $timeslot->sc_id;
            $sctimeslot->ts_code =  $timeslot->ts_code;
            $sctimeslot->create();
        }
  
        // tell the user
        echo json_encode(array("message" => "Screening center info has been created.","errorFound"=>false,"error" => ""));
    }
  
    // if unable to create record, tell the user
    else{
		$screeningCenter->sc_id = $data->sc_id;
  		// read the details of patient to be edited
		$screeningCenter->readOne();
		if (isset($screeningCenter->name)){
			// record already exist
			http_response_code(409);
  
			// tell the user
			echo json_encode(array("message" => "Screening center info already exist","errorFound"=>true,"error" => "409 conflict"));
		}else{
			// set response code - 503 service unavailable
			http_response_code(503);
  
			// tell the user
			echo json_encode(array("message" => "Unable to create Screening center info.","errorFound"=>true,"error" => "503 service unavailable"));
		}

    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to create Screening center info. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}

 
?>