<?php
/**
 * Author: Majina
 * TestResultInput.readAll()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/test-result-input/read-all.php
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/test-result-input.php';
include_once '../../objects/v1/person.php';
  
// instantiate database and data object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$testResultInput = new TestResultInput($db);

// query data
$stmt = $testResultInput->readAll();
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){

    $person = new Person($db);
  
    // record array
    $record_arr=array();
    $record_arr["data"]=array();
    
    // retrieve our table contents
    $total_records = 0;
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);

        $record_item = array(
            "patient_ic_no" => $patient_ic_no,
            "booking_no" => $booking_no,
            "reg_no" => $reg_no,
            "test_marker_code" => $test_marker_code,
            "test_date" => $test_date,
            "test_value" => $test_value,
            "source" => $source,
            "test_panel_code" => $test_panel_code,
            "person"=> $person->readByIcNo($patient_ic_no)
            // "screening_plan"=> $screeningPlan->readByPackageCode($package_code)
        );
  
        array_push($record_arr["data"], $record_item);
        $total_records++;
    }

    $record_arr["total_records"]=$total_records;
    
    // set response code - 200 OK
    http_response_code(200);
  
    // show data in json format
    echo json_encode($record_arr);
}else{
  
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no record found
    echo json_encode(
        array("message" => "No test result found.","error" => "Not found")
    );
}
?>