<?php
/**
 * Author: Majina
 * State.readAll()
 * URL for testing : https://myhc.my/myhc-api/v1/home-screening-charging/read-all.php
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");

// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/home-screening-charging.php';

  
// instantiate database and data object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$homeScreeningCharging = new HomeScreeningCharging($db);


// query data
$stmt = $homeScreeningCharging->readAll();
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $state_arr=array();
    $state_arr["data"]=array();
    
    // retrieve our table contents
    $total_records = 0;
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
         
        $state_item=array(
            "id"  => $id,
            "code"  => $code,
            "description"  => $description,
            "price"  => $price,
            "quantity"  => $quantity,
            "enabled"  => $enabled,
        );
  
        array_push($state_arr["data"], $state_item);
        $total_records++;
    }

    $state_arr["total_records"]=$total_records;
    
    // set response code - 200 OK
    http_response_code(200);
  
    // show data in json format
    echo json_encode($state_arr);
}else{
  
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no record found
    echo json_encode(
        array("message" => "No Home Screening Charging found.","error" => "404 Not found")
    );
}
?>