<?php
/**
 * Author: Majina
 * BookingPatient.create()
 * URL for testing : https://myhc.my/myhc-api/v1/booking-patient/create.php
 * JSON input: { "booking_id":"<booking_id>", "ic_no":"<ic_no>"}
 *  
 * Method: POST   
 */
     
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/booking-patient.php';
  
$database = new Database();
$db = $database->getConnection();
  
$bookingPatient = new BookingPatient($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));
  
// make sure data is not empty
if (
    !empty($data->booking_id) &&
    !empty($data->ic_no) 
    ){
  
	$bookingPatient->booking_id = $data->booking_id;
    $bookingPatient->ic_no = $data->ic_no;
 
  
    // create the record
    if($bookingPatient->create()){
  
        // set response code - 201 created
        http_response_code(201);
  
        // tell the user
        echo json_encode(array("message" => "Booking's patient info has been created.","errorFound"=>false,"error" => "",));
    }
  
    // if unable to create record, tell the user
    else{
  		// read the details of patient to be edited
		$bookingPatient->readOne();
		if($bookingPatient->booking_id!=null){
			// record already exist
			http_response_code(409);
  
			// tell the user
			echo json_encode(array("message" => "Booking's patient info already exist","errorFound"=>true,"error" => "Conflict"));
		}else{
			// set response code - 503 service unavailable
			http_response_code(503);
  
			// tell the user
			echo json_encode(array("message" => "Unable to create Booking's patient info.","errorFound"=>true,"error" => "Service unavailable"));
		}

    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to create Booking's patient info. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}
?>