<?PHP


ini_set('display_errors', 1); 
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


function Requery($MerchantCode, $RefNo, $Amount){
    $query = "https://payment.ipay88.com.my/epayment/enquiry.asp?MerchantCode=" . $MerchantCode . "&RefNo=" . $RefNo . "&Amount=" . $Amount;
    $url = parse_url($query);
    $host = $url["host"];
    $sslhost = "ssl://".$host;
    $path = $url["path"] . "?" . $url["query"];
    $timeout = 5;
    $fp = fsockopen ($sslhost, 443, $errno, $errstr, $timeout);
    $buf="";
    if ($fp) {
        fputs ($fp, "GET $path HTTP/1.0\nHost: " . $host . "\n\n");
        while (!feof($fp)) {
            $buf .= fgets($fp, 128);
        }
        $lines = preg_split("/\n/", $buf);
        $Result = $lines[count($lines)-1];
        fclose($fp);
    } else {
        # enter error handing code here
        echo "Error occured - " . $query ;
    }
    return $Result;
}

if (isset($_POST['Submit'])){
    echo "<br>PASSING PARAMS: ". $_POST['MerchantCode'] .' , '. $_POST['RefNo'] . ' , '. $_POST['Amount'];
    echo "<br>RESULT: ".Requery($_POST['MerchantCode'], $_POST['RefNo'], $_POST['Amount']);
}

?>

<HTML>
    <BODY>
        <br><br>
        <FORM method="post" name="ePaymentRequery">
            MerchantCode:
            <INPUT type="text" name="MerchantCode" value="">
            <br><br>
            RefNo:
            <INPUT type="text" name="RefNo" value="">
            <br><br>
            Amount:
            <INPUT type="text" name="Amount" value="">
            <br><br>
            <INPUT type="submit" value="Proceed with checking" name="Submit"> 
        </FORM>
    </BODY>
</HTML>