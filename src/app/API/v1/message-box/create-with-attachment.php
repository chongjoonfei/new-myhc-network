<?php

ini_set('display_errors', 1); 
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
mysqli_report(MYSQLI_REPORT_ALL);

/**
 * Author: Elizha
 * MessageBox.createWithAttachment()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/message-box/create-with-attachment.php
 * JSON input: { "message_id":"<message_id>", "sender": "<sender>", "receiver":"<receiver>", "subject":"<subject>", "content":"<content>", 
 * "headers":"<headers>", "date_sent":"<date_sent>","message_type_code":"<message_type_code>", ,
 * "status":"<status>", "attachment":"[<attachment>]", "message_root_id":"<message_root_id>"}
 * Method: POST   
 */



// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/message-box.php';
  
$database = new Database();
$db = $database->getConnection();

// prepare object
$messageBox = new MessageBox($db);
  
// get posted data
// $data = json_decode(file_get_contents("php://input"));

$message_id = $_POST["receiver"];
$sender = $_POST["sender"];
$receiver = $_POST["receiver"];
$sender_name = $_POST["sender_name"];
$receiver_name = $_POST["receiver_name"];
$subject = $_POST["subject"];
$content = $_POST["content"];
$headers = $_POST["headers"];
$message_type_code = $_POST["message_type_code"];
$status = $_POST["status"];
$message_root_id = $_POST["message_root_id"];
  
// make sure data is not empty
if (
    !empty($sender) &&
    !empty($receiver) &&
    !empty($content) &&
    !empty($message_type_code) 
){
    
    // set data property values
    $messageBox->message_id = $messageBox->getNextId();
    $messageBox->sender = $sender;
    $messageBox->receiver = $receiver;
    $messageBox->sender_name = $sender_name;
    $messageBox->receiver_name = $receiver_name;
    $messageBox->subject = $subject; 
    $messageBox->content = $content;
    $messageBox->headers = $headers;
    $messageBox->date_sent = date('Y-m-d H:i:s');
    $messageBox->message_type_code = $message_type_code;
    $messageBox->status = $status;
    $messageBox->message_root_id = $message_root_id;

    $upload = false;
    $errorFound=false;
    
    if (isset($_FILES['attachment'])){
        $upload = true;
        $target_dir = $_SERVER['DOCUMENT_ROOT'] . "/" . "upload_files/message/";
        $fileName = $_FILES['attachment']['name'];
        $messageBox->attachment = $messageBox->message_id.'_' . $fileName;
        $target_file = $target_dir .  $messageBox->message_id. '_' .$fileName;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        $errorFound = false;
    
        // Check if file already exists
        if (file_exists($target_file) && !$errorFound) {
            $msg = "File already exists. Please remove the existing uploaded file before upload";
            $errorFound=true;
        }
    
        // Check file size
        if ($_FILES["attachment"]["size"] > 3000000  && !$errorFound) {
            $msg = "Your file is too large exceeding maximum size of 3MB.";
            $errorFound=true;
        }
    
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" && $imageFileType != "pdf"  && !$errorFound) {
            $msg = "Only JPG, JPEG, PNG, GIF & PDF files are allowed.";
            $errorFound=true;
        }
    }else{
        $messageBox->attachment = null;
    }



    if (!$errorFound){

        // create the record
        if($messageBox->create()){
            if ($upload){
                if (move_uploaded_file($_FILES["attachment"]["tmp_name"], $target_file) ) {
                    // set response code - 201 created
                    http_response_code(201);
    
                    // tell the user
                    echo json_encode(array("message" => "Message has been created.","errorFound"=>false,"error" => "",));
                }else{
                    // set response code - 503 service unavailable
                    http_response_code(503);
    
                    // tell the user
                    echo json_encode(array("message" => "Unable to create Message info.","errorFound"=>true,"error" => "503 service unavailable"));
                }
            }else{
                // set response code - 201 created
                http_response_code(201);

                // tell the user
                echo json_encode(array("message" => "Message has been created.","errorFound"=>false,"error" => "",));
            }

        }
        // if unable to create record, tell the user
        else{
            // set response code - 503 service unavailable
            http_response_code(503);

            // tell the user
            echo json_encode(array("message" => "Unable to create Message info.","errorFound"=>true,"error" => "503 service unavailable"));
        }
    }else{
        // set response code - 503 service unavailable
        http_response_code(503);

        // tell the user
        echo json_encode(array("message" => "Unable to create Message info.","errorFound"=>true,"error" => "503 service unavailable"));
    }
    
// tell the user data is incomplete
}else{

    // set response code - 400 bad request
    http_response_code(400);

    // tell the user
    echo json_encode(array("message" => "Unable to create Message. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}
?>