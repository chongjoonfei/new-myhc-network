<?php
/**
 * Author: Elizha
 * MessageBox.create()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/message-box/create.php
 * JSON input: { "message_id":"<message_id>", "sender": "<sender>", "receiver":"<receiver>", "subject":"<subject>", "content":"<content>", 
 * "headers":"<headers>", "date_sent":"<date_sent>","message_type_code":"<message_type_code>", ,
 * "status":"<status>", "attachment":"<attachment>", "message_root_id":"<message_root_id>"}
 * Method: POST   
 */



// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/message-box.php';
  
$database = new Database();
$db = $database->getConnection();

// prepare object
$messageBox = new MessageBox($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));
  
// make sure data is not empty
if (
    !empty($data->sender) &&
    !empty($data->receiver) &&
    !empty($data->content) &&
    !empty($data->message_type_code) 
){

    // set data property values
    $messageBox->message_id = $data->message_id;
    $messageBox->sender = $data->sender;
	$messageBox->receiver = $data->receiver;
    $messageBox->sender_name = $data->sender_name;
	$messageBox->receiver_name = $data->receiver_name;
    $messageBox->subject = $data->subject; 
    $messageBox->content = $data->content;
    $messageBox->headers = $data->headers;
    $messageBox->date_sent = date('Y-m-d H:i:s');
    $messageBox->message_type_code = $data->message_type_code;
	$messageBox->status = $data->status;
    $messageBox->attachment = $data->attachment;
    $messageBox->message_root_id = $data->message_root_id;
  
    // create the record
    if($messageBox->create()){
  
        // set response code - 201 created
        http_response_code(201);
  
        // tell the user
        echo json_encode(array("message" => "Message has been created.","errorFound"=>false,"error" => "",));
    }
  
    // if unable to create record, tell the user
    else{
		$messageBox->message_id = $data->message_id;
  		// read the details of record to be edited
		$messageBox->readOne();
		if (isset($messageBox->message_type_code)){
			// record already exist
			http_response_code(409);
  
			// tell the user
			echo json_encode(array("message" => "Message info already exist","errorFound"=>true,"error" => "409 conflict"));
		}else{
			// set response code - 503 service unavailable
			http_response_code(503);
  
			// tell the user
			echo json_encode(array("message" => "Unable to create Message info.","errorFound"=>true,"error" => "503 service unavailable"));
		}

    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to create Message. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}
?>