<?php
/**
 * Author: Elizha
 * PackageTestGroups.delete()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/package-test-groups/delete.php
 * JSON input: { "package_code":"<package_code>", "test_group_code":"<test_group_code>",
 * "test_location":"<test_location>",  "total_test_conducted":"<total_test_conducted>", "remark":"<remark>"}
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object file
include_once '../../config/db.php';
include_once '../../objects/v1/package-test-groups.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$packageTestGroups = new PackageTestGroups($db);
  
// get data input
$data = json_decode(file_get_contents("php://input"));
  
// set data to be deleted
$packageTestGroups->package_code = $data->package_code;
$packageTestGroups->test_group_code = $data->test_group_code;
  
// delete the record
if($packageTestGroups->delete()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Package Test Groups  info was deleted.","error"=>"","errorFound"=>false));
}
  
// if unable to delete the record
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to delete Package Test Groups  info.","error"=>"503 service unavailable","errorFound"=>true));
}
?>