<?php
/**
 * Author: Majina
 * MenuItem.delete()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/menu-item/delete.php
 * JSON input: { "item_id":"<item_id>", "name":"<name>", "path": "<path>", "page": "<page>", "enabled": "<enabled>"}
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object file
include_once '../../config/db.php';
include_once '../../objects/v1/menu-item.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$menuItem = new MenuItem($db);
  
// get data input
$data = json_decode(file_get_contents("php://input"));
  
// set data to be deleted
$menuItem->item_id = $data->item_id;
  
// delete the record
if($menuItem->delete()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Menu item info was deleted.","error"=>"","errorFound"=>false));
}
  
// if unable to delete the record
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to deleteMenu item  info.","error"=>"503 service unavailable","errorFound"=>true));
}
?>